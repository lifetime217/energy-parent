queryPage
===
select
@pageTag(){
*
@}
from bt_data_pcs
where 1=1
@if(isNotEmpty(project_id) && strutil.length(project_id) > 0){
and project_id = #project_id#
@}
@if(isNotEmpty(pcs_sign) && strutil.length(pcs_sign) > 0){
and pcs_sign like #'%'+pcs_sign+'%'#
@}
