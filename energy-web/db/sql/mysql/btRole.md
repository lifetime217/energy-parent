queryPage
===
select
@pageTag(){
id,
name,
company_id,
create_time,
update_time,
description
@}
from bt_role
where 1=1
@if(isNotEmpty(name) && strutil.length(name) != 0){
and name like #'%'+name+'%'#
@}
@if(isNotEmpty(description)&&strutil.length(description) != 0){
and description like #'%'+description+'%'#
@}
@if(!isSystemUser){
and company_id = #company_id#
@}

getRoleByName
===
select id,name,company_id,create_time,update_time,description from bt_role where name = #name#

getAllRoleByCompanyId
===
select id,name,company_id,create_time,update_time,description from bt_role where  company_id = #companyId#