userLogin
===
* 账号密码登录SQL

	select id,login_no,nick,login_pwd,company_code,company_name,company_id,user_type from bt_user 
	where login_no = #loginNo# and login_pwd = #loginPwd#
	
modifyPwd
===
*修改账号密码
	update  bt_user set login_pwd = #loginPwd# where login_no = #loginNo#

getInfoByNumber
===
* 根据账号查找用户信息
	select id,login_no,login_pwd from bt_user
	where login_no = #loginNo#

getInfo
===
* 根据id查找用户信息
	select id,login_no,login_pwd,nick,company_id from bt_user
	where id = #id#

	
queryPage
===
select
@pageTag(){
id,
login_no,
email,
nick,
role,
create_time,
update_time,
last_login_time,
company_id,
company_name,
status,
user_type
@}
from bt_user
where status=1
@if(isNotEmpty(login_no) && strutil.length(login_no) != 0){
and login_no like #'%'+login_no+'%'#
@}
@if(isNotEmpty(nick)&&strutil.length(nick) != 0){
and nick like #'%'+nick+'%'#
@}
@if(!isSystemUser){
and company_id = #company_id#
@}

selectUserByLoginNo
===
select
id,
login_no,
email,
nick,
role
from bt_user
where login_no = #loginNo#

selectUserByEmail
===
select
id,
login_no,
email,
nick,
role
from bt_user
where email = #email#


