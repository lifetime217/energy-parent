queryPage
===
select
@pageTag(){
*
@}
from bt_data_alert
where 1=1
@if(isNotEmpty(project_id) && strutil.length(project_id) > 0 && project_id != "全部"){
and project_id = #project_id#
@}
@if(isNotEmpty(alert_source_type) && strutil.length(alert_source_type) > 0 && alert_source_type != "全部"){
and alert_source_type = #alert_source_type#
@}
@if(isNotEmpty(alert_level) && strutil.length(alert_level) > 0 && alert_level != "全部"){
and alert_level = #alert_level#
@}
@if(isNotEmpty(alert_device) && strutil.length(alert_device) > 0 && alert_device != "全部"){
and alert_device = #alert_device#
@}
@if(isNotEmpty(alert_time) && strutil.length(alert_time) > 0){
and alert_time >= #strutil.subStringTo(alert_time,0,19)#
@}
@if(isNotEmpty(alert_time) && strutil.length(alert_time) > 0){
and alert_time <= #strutil.subStringTo(alert_time,20,39)#
@}

getBugByProjectId
===
select alert_device,alert_content,alert_level,alert_time from bt_data_alert where project_id = #projectId# order  by alert_time desc