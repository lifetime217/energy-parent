getGroup
===
* 查询项目下的设备组
select id,name,project_id from bt_project_devicegroup where is_delete = 0 and project_id = #projectId#


deleteByProject
===
* 逻辑删除一个项目的所有设备组
update bt_project_devicegroup set is_delete = 1 where project_id = #projectId#