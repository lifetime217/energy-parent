queryPage
===
select
@pageTag(){
id,
company_id,
report_name,
report_url,
report_type,
description,
create_time,
update_time
@}
from bt_report_type
where 1=1
@if(isNotEmpty(report_name) && strutil.length(report_name) != 0){
and report_name like #'%'+report_name+'%'#
@}
@if(isNotEmpty(report_type)&&strutil.length(report_type) != 0){
and report_type like #'%'+report_type+'%'#
@}
@if(isNotEmpty(description)&&strutil.length(description) != 0){
and description like #'%'+description+'%'#
@}
@if(!isSystemUser){
and company_id = #company_id#
@}


getReportTypeByName
===
select id,report_name,report_url,report_type,company_id,create_time,update_time,description from bt_report_type where report_name = #reportName#

getAllReportTypeByCompanyId
===
select id,report_name,report_url,report_type,company_id,create_time,update_time,description from bt_report_type where company_id = #companyId#
