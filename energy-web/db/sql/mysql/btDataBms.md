queryPage
===
select
@pageTag(){
*
@}
from bt_data_bms
where 1=1
@if(isNotEmpty(project_id) && strutil.length(project_id) > 0){
and project_id = #project_id#
@}
@if(isNotEmpty(bms_sign) && strutil.length(bms_sign) > 0){
and bms_sign like #'%'+bms_sign+'%'#
@}
