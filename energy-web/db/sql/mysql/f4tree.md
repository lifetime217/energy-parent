test
===
select region_code as id,parent_reg_code as pId,region_name as `name`,
id AS treeId
from bt_region

getStartPlace
===
SELECT position_name AS `name`,position_code AS id ,parent_reg_position_code AS pId,'start_place' AS treeId,
`type` FROM bt_reg_position WHERE state=0

getEditStartPlace
===
SELECT position_name AS `name`,position_code AS id ,parent_reg_position_code AS pId,'edit_start_place' AS treeId,`type` FROM bt_reg_position WHERE state=0

getDistination
===
SELECT position_name AS `name`,position_code AS id ,parent_reg_position_code AS pId,'distination' AS treeId,
`type` FROM bt_reg_position WHERE state=0

getEditDistination
===
SELECT position_name AS `name`,position_code AS id ,parent_reg_position_code AS pId,'edit_distination' AS treeId,`type` FROM bt_reg_position WHERE state=0