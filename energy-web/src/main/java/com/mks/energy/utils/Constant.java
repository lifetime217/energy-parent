package com.mks.energy.utils;

import java.text.SimpleDateFormat;

/**
 * @author lifetime
 *
 */
public final class Constant {
	/**
	 * 用户登录的用户名key
	 */
	public static final String UserNumber = "bartalks_user_number";
	/**
	 * 用户访问网站的密钥key
	 */
	public static final String AccessToken = "bartalks_user_accessToken";
	/**
	 * 日期格式化1
	 */
	public static final SimpleDateFormat DateFormat1 = new SimpleDateFormat("yyyyMMdd");
	
}
