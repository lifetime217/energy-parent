package com.mks.energy.utils;

import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;

public class ExcelUtil {
	
	
	/**
	 * @author ghl
	 * @Date 2019年2月1日下午7:09:56
	 * @TODO 获取Excel文件单元格的值
	 * @param cell 单元格对象
	 * @return
	 */
	public static String getCellValue(XSSFCell cell) {
		if (cell != null) {
			if (CellType.STRING.compareTo(cell.getCellType()) == 0) {
				XSSFRichTextString cellStr = cell.getRichStringCellValue();
				if (cellStr != null) {
					return cellStr.getString();
				}
			} else if (CellType.NUMERIC.compareTo(cell.getCellType()) == 0) {
				return String.valueOf(cell.getNumericCellValue());
			} else if (CellType.FORMULA.compareTo(cell.getCellType()) == 0) {
				return cell.getCellFormula();
			}
		}
		return null;
	}

	public static Double getCellDoubleValue(XSSFCell cell) {
		if (cell != null) {
			if (CellType.STRING.compareTo(cell.getCellType()) == 0) {
				XSSFRichTextString cellStr = cell.getRichStringCellValue();
				if (cellStr != null) {
					return Double.parseDouble(cellStr.getString());
				}
			} else if (CellType.NUMERIC.compareTo(cell.getCellType()) == 0) {
				return cell.getNumericCellValue();
			}
		}
		return null;
	}
	
	public static Integer getCellIntValue(XSSFCell cell) {
		Double d = getCellDoubleValue(cell);
		if(d != null){
			return d.intValue();
		}
		return null;
	}
}
