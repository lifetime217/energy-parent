package com.mks.energy.utils;

import com.mks.energy.core.UserContext;

/**
 * 判断是否是平台方管理员
 * @author ghl 
 * @since JDK 1.8
 * ClassName: SysUtil
 * date: 2018年12月22日 上午3:07:15
 *
 */
public class SysUtil {
	
	public static boolean isSystemUser() {
		return UserContext.get().getNumber().equals("admin");
	}
}
