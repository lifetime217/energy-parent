package com.mks.energy.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.beetl.sql.core.engine.PageQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.alibaba.fastjson.JSONObject;
import com.mks.energy.core.UserContext;
import com.mks.energy.core.UserMenuVO;
import com.mks.energy.core.ext.AbstractBaseService;
import com.mks.energy.core.ext.BaseDao;
import com.mks.energy.dao.IBtRoleDao;
import com.mks.energy.dao.IBtRoleMenuDao;
import com.mks.energy.dao.IBtRoleReportTypeDao;
import com.mks.energy.dao.IBtUserMenuDao;
import com.mks.energy.dao.IBtUserReportTypeDao;
import com.mks.energy.dao.IBtUserRoleDao;
import com.mks.energy.dao.IMenuDao;
import com.mks.energy.dao.IUserDao;
import com.mks.energy.entity.biz.BtRole;
import com.mks.energy.entity.biz.BtRoleMenu;
import com.mks.energy.entity.biz.BtRoleReportType;
import com.mks.energy.entity.biz.BtUserMenu;
import com.mks.energy.entity.biz.BtUserReportType;
import com.mks.energy.entity.biz.BtUserRole;
import com.mks.energy.entity.biz.MenuInfo;
import com.mks.energy.entity.biz.UserInfo;
import com.mks.energy.service.IUserService;
import com.mks.energy.utils.MD5;
import com.mks.energy.utils.SysUtil;

@Service
public class UserService extends AbstractBaseService<UserInfo> implements IUserService {
	
	private static final Logger logger = LoggerFactory.getLogger(UserService.class);

	@Autowired
	private IUserDao userDao;

	@Autowired
	private IMenuDao menuDao;
	
	@Autowired
	private IBtRoleDao roleDao;
	
	@Autowired
	private IBtRoleMenuDao roleMenuDao;
	
	@Autowired
	private IBtRoleReportTypeDao roleReportTypeDao;

	@Autowired
	private IBtUserRoleDao userRoleDao;
	
	@Autowired
	private IBtUserMenuDao userMenuDao;
	
	@Autowired
	private IBtUserReportTypeDao userReportTypeDao;
	
	public UserInfo getUser(String number, String pwd) {
		return userDao.userLogin(number, pwd);
	}

	/**
	 * 是否是有效的原始密码
	 * 
	 * @param number
	 *            账号
	 * @param pwd
	 *            未加密过的密码
	 * @return
	 */
	public boolean pwdSuccess(String number, String pwd) {
		UserInfo info = userDao.getInfoByNumber(number);
		if (info != null && !StringUtils.isEmpty(info.getLoginPwd())) {
			return info.getLoginPwd().equalsIgnoreCase(MD5.get(pwd));
		}
		return false;
	}

	public void modifyPwd(String number, String newPwd) {
		userDao.modifyPwd(number, MD5.get(newPwd));
	}

	public UserInfo getInfoByNumber(String number) {
		return userDao.getInfoByNumber(number);
	}

	/**
	 * 获取用户菜单
	 * @param uinfo
	 * @return
	 * @author ghl 2018年12月22日上午2:30:37
	 * getUserMenus:
	 */
	public List<UserMenuVO> getUserMenus(UserInfo uinfo) {
		//如果当前是超管(平台方管理员),获取系统所有菜单
		if (uinfo.getUserType() == 99) {
			List<MenuInfo> list = menuDao.all();
			List<UserMenuVO> res = new ArrayList<UserMenuVO>();
			if (list.size() > 0) {
				for (MenuInfo menuInfo : list) {
					if (StringUtils.isEmpty(menuInfo.getParentId())&& menuInfo.getMenuStatus() == UserMenuVO.Enable_Status) {
						UserMenuVO menuVO = new UserMenuVO();
						menuVO.setName(menuInfo.getMenuName());
						menuVO.setNumber(menuInfo.getMenuNumber());
						menuVO.setUrl(menuInfo.getMenuUrl());
						menuVO.setIconClass(menuInfo.getMenuIcon());
						menuVO.getChilds().addAll(findChilds(menuInfo.getId(), list));
						res.add(menuVO);
					}
				}
			}
			// 按菜单编码的自然顺序进行排序
			Collections.sort(res, new Comparator<UserMenuVO>() {
				public int compare(UserMenuVO o1, UserMenuVO o2) {
					return o1.getNumber().compareTo(o2.getNumber());
				}
			});
			return res;
		} else {
			return getUserMenus(uinfo.getId());
		}
	}

	/** 
	 * @param userId
	 * @return
	 * @author ghl 2018年12月22日上午2:36:30
	 * getUserMenus:获取用户所有菜单(非超管)
	 */  
	@Override
	public List<UserMenuVO> getUserMenus(String userId) {
		List<MenuInfo> list = menuDao.getUserMenus(userId);
		List<UserMenuVO> res = new ArrayList<UserMenuVO>();
		if (list.size() > 0) {
			for (MenuInfo menuInfo : list) {
				if (StringUtils.isEmpty(menuInfo.getParentId())&& menuInfo.getMenuStatus() == UserMenuVO.Enable_Status) {
					UserMenuVO menuVO = new UserMenuVO();
					menuVO.setName(menuInfo.getMenuName());
					menuVO.setNumber(menuInfo.getMenuNumber());
					menuVO.setUrl(menuInfo.getMenuUrl());
					menuVO.setIconClass(menuInfo.getMenuIcon());
					menuVO.getChilds().addAll(findChilds(menuInfo.getId(), list));
					res.add(menuVO);
				}
			}
		}
		// 按菜单编码的自然顺序进行排序
		Collections.sort(res, new Comparator<UserMenuVO>() {
			public int compare(UserMenuVO o1, UserMenuVO o2) {
				return o1.getNumber().compareTo(o2.getNumber());
			}
		});
		return res;
	}

	/**
	 * 寻找菜单的子菜单集合
	 * 
	 * @param id
	 * @param list
	 * @return
	 */
	List<UserMenuVO> findChilds(String id, List<MenuInfo> list) {
		List<UserMenuVO> res = new ArrayList<UserMenuVO>();
		for (MenuInfo menuInfo : list) {
			if (id.equals(menuInfo.getParentId()) && menuInfo.getMenuStatus() == UserMenuVO.Enable_Status) {
				UserMenuVO menuVO = new UserMenuVO();
				menuVO.setName(menuInfo.getMenuName());
				menuVO.setNumber(menuInfo.getMenuNumber());
				menuVO.setUrl(menuInfo.getMenuUrl());
				menuVO.setIconClass(menuInfo.getMenuIcon());
				res.add(menuVO);
			}
		}
		// 按菜单编码的自然顺序进行排序
		Collections.sort(res, new Comparator<UserMenuVO>() {
			public int compare(UserMenuVO o1, UserMenuVO o2) {
				return o1.getNumber().compareTo(o2.getNumber());
			}
		});
		return res;
	}

	public UserInfo getUser(String id) {
		return userDao.getInfo(id);
	}

	// 分页
	@Override
	public PageQuery<UserInfo> getQueryList(PageQuery<UserInfo> pageQuery) {
		Map<String, Object> map = (Map<String, Object>) pageQuery.getParas();
		map.put("company_id", UserContext.get().getCompanyId());// 添加查询参数（公司ID）
		map.put("isSystemUser", SysUtil.isSystemUser());
		if (StringUtils.isEmpty(pageQuery.getOrderBy())) {
			pageQuery.setOrderBy("create_time DESC");
		}
		userDao.queryPage(pageQuery);
		return pageQuery;
	}

	@Override
	public List<MenuInfo> getMenuTree() {
		return menuDao.all();
	}

	@Override
	public UserInfo checkUserName(String loginNo) {
		return userDao.selectUserByLoginNo(loginNo);
	}

	@Override
	public BaseDao<UserInfo> getDao() {
		return userDao;
	}

	/** 
	 * @param id
	 * @param companyId
	 * @return
	 * @author ghl 2018年12月20日上午1:12:18
	 * getAllPermissionByUserId://获取当前用户所有的权限(角色)
	 */  
	@Override
	public JSONObject getAllPermissionByUserId(String id,String companyId) {
		JSONObject res = new JSONObject();
		List<JSONObject> list = new ArrayList<>();
		//1获取当前用户所属公司的所有角色
		List<BtRole> roleList = roleDao.getAllRoleByCompanyId(companyId);
		if(roleList!=null&&roleList.size()>0) {
			for (int i = 0; i < roleList.size(); i++) {
				JSONObject obj =new JSONObject();
				obj.put("roleId", roleList.get(i).getId());
				obj.put("roleName", roleList.get(i).getName());
				obj.put("parentId", 0);
				list.add(obj);
			}
		}
		//2获取当前用户的所有角色
		List<BtUserRole> userRoleList =userRoleDao.getAllMyRoleById(id);
		
		if(list!=null&&list.size()>0&&userRoleList!=null&&userRoleList.size()>0) {
			for (int i = 0; i < list.size(); i++) {
				for (int j = 0; j < userRoleList.size(); j++) {
					//3如果角色ID等于用户所拥有的角色ID.添加选中
					if(list.get(i).get("roleId").equals(userRoleList.get(j).getRoleId())) {
						list.get(i).put("checked", true);
					}
				}
			}
		}
		res.put("role", list);
		return res;
	}

	/** 
	 * @param id
	 * @param roleIds
	 * @author ghl 2018年12月21日上午12:18:20
	 * saveAllMyPermission:根据用户ID,角色ID数组.更新保存权限
	 */  
	@Transactional
	@Override
	public void saveAllMyPermission(String id,String[] roleIds) {
		HashSet<String> menuIds = new HashSet<>(); 
		HashSet<String> reportTypeIds = new HashSet<>();
		String menuId = null;
		String reportTypeId = null;
		try {
			//1.根据用户ID删除用户原有角色
			userRoleDao.deleteRoleByUserId(id);
			//2.同时还需要删除当前用户所拥有的菜单.
			userMenuDao.deleteAllMenuByUserId(id);
			//3.同时还需要删除用户所拥有的查看报告的权限
			userReportTypeDao.deleteUserReportTypeByUserId(id);
			//3.插入所有的新角色
			for (int i = 0; i < roleIds.length; i++) {
				//获取新角色对应的菜单集合
				List<BtRoleMenu> roleMenuList = roleMenuDao.getMenuByRoleId(roleIds[i]);
				//获取新角色对应的查看报告的权限
				List<BtRoleReportType> roleReportTypeList = roleReportTypeDao.getReportTypeByRoleId(roleIds[i]);
				//遍历菜单集合,封装菜单ID,去重复
				for (int j = 0; j < roleMenuList.size(); j++) {
					menuId = roleMenuList.get(j).getMenuId();
					menuIds.add(menuId);
				}
				//遍历查看报告权限,封装菜单ID,去重复
				for (int j = 0; j < roleReportTypeList.size(); j++) {
					reportTypeId = roleReportTypeList.get(j).getReportTypeId();
					reportTypeIds.add(reportTypeId);
				}
				BtUserRole userRole = new BtUserRole();
				userRole.setUserId(id);
				userRole.setRoleId(roleIds[i]);
				userRole.setCreateTime(new Date());
				userRole.setUpdateTime(new Date());
				userRoleDao.insertTemplate(userRole);
			}
			//4.插入所有的新角色对应的菜单
			for (String menuid : menuIds) {
				BtUserMenu userMenu = new BtUserMenu();
				userMenu.setUserId(id);
				userMenu.setMenuId(menuid);
				userMenuDao.insertTemplate(userMenu);
			}
			//5.插入所有的新角色对应的查看报告的权限
			for (String reportTypeid : reportTypeIds) {
				BtUserReportType userReportType = new BtUserReportType();
				userReportType.setUserId(id);
				userReportType.setReportTypeId(reportTypeid);
				userReportType.setCreateTime(new Date());
				userReportType.setUpdateTime(new Date());
				userReportTypeDao.insertTemplate(userReportType);
			}
			
			
			UserInfo user = new UserInfo();
			user.setId(id);
			user.setUpdateTime(new Date());
			userDao.updateTemplateById(user);
		} catch (Exception e) {
			logger.error(e.getMessage(),e.fillInStackTrace());
			throw new RuntimeException("服务器异常，请联系管理员");
		}
		
	}

}
