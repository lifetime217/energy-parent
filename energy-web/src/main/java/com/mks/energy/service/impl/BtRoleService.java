package com.mks.energy.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.beetl.sql.core.engine.PageQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.alibaba.fastjson.JSONObject;
import com.mks.energy.core.UserContext;
import com.mks.energy.core.ext.AbstractBaseService;
import com.mks.energy.core.ext.BaseDao;
import com.mks.energy.dao.IBtReportTypeDao;
import com.mks.energy.dao.IBtRoleDao;
import com.mks.energy.dao.IBtRoleMenuDao;
import com.mks.energy.dao.IBtRoleReportTypeDao;
import com.mks.energy.dao.IMenuDao;
import com.mks.energy.entity.biz.BtReportType;
import com.mks.energy.entity.biz.BtRole;
import com.mks.energy.entity.biz.BtRoleMenu;
import com.mks.energy.entity.biz.BtRoleReportType;
import com.mks.energy.entity.biz.MenuInfo;
import com.mks.energy.service.IBtRoleService;
import com.mks.energy.utils.SysUtil;


/**
 * @author lifetime
 *
 */
@Service
public class BtRoleService extends AbstractBaseService<BtRole> implements IBtRoleService{
	
	private static final Logger logger = LoggerFactory.getLogger(BtRoleService.class);
	@Autowired
	private IBtRoleDao roleDao;
	
	@Autowired
	private IMenuDao menuDao;
	
	@Autowired
	private IBtRoleMenuDao roleMenuDao;
	
	@Autowired
	private IBtReportTypeDao reportTypeDao;
	
	@Autowired
	private IBtRoleReportTypeDao roleReportTypeDao;

	@Override
	public BaseDao<BtRole> getDao() {
		return roleDao;
	}
	
	@Override
	public String add(BtRole t) {
		return super.add(t);
	}

	
	/** 
	 * @param pageQuery
	 * @return
	 * @author ghl 2018年12月22日上午3:14:31
	 * getQueryList:
	 */  
	@Override
	public PageQuery<BtRole> getQueryList(PageQuery<BtRole> pageQuery) {
		Map<String, Object> map = (Map<String, Object>) pageQuery.getParas();
		map.put("company_id", UserContext.get().getCompanyId());// 添加查询参数（公司ID）
		map.put("isSystemUser", SysUtil.isSystemUser());
		if (StringUtils.isEmpty(pageQuery.getOrderBy())) {
			pageQuery.setOrderBy("create_time DESC");
		}
		roleDao.queryPage(pageQuery);
		return pageQuery;
	}

	
	/** 
	 * @see com.mks.energy.service.IBtRoleService#checkNameUnique(java.lang.String) 
	 * @param name
	 * @return
	 * @author ghl 2018年12月17日下午5:34:46
	 * checkNameUnique:
	 */  
	@Override
	public BtRole checkNameUnique(String name) {
		BtRole role = roleDao.getRoleByName(name);
		return role;
	}

	/** 
	 * @param roleId
	 * @param menuIds
	 * @param rptTypeIds
	 * @return
	 * @author ghl 2018年12月19日上午12:37:35
	 * saveRolePermission:保存角色权限的逻辑是先删除该角色原有的菜单和查看报告的权限,再新增这一次选中的菜单和查看报告的权限
	 */  
	@Transactional
	@Override
	public void saveRolePermission(String roleId, String[] menuIds, String[] rptTypeIds) {
		try {
			//1.删除角色原有的菜单.
			roleMenuDao.deleteRoleMenuByRoleId(roleId);
			//2.新增选中的菜单
			for (int i = 0; i < menuIds.length; i++) {
				BtRoleMenu roleMenu = new BtRoleMenu();
				roleMenu.setRoleId(roleId);
				roleMenu.setMenuId(menuIds[i]);
				roleMenu.setCreateTime(new Date());
				roleMenu.setUpdateTime(new Date());
				roleMenuDao.insertTemplate(roleMenu);
			}
			//3.删除角色原有的查看报告权限
			roleReportTypeDao.deleteRoleReportTypeByRoleId(roleId);
			//4.新增选中的查看报告的权限
			for (int i = 0; i < rptTypeIds.length; i++) {
				BtRoleReportType roleReportType = new BtRoleReportType();
				roleReportType.setRoleId(roleId);
				roleReportType.setReportTypeId(rptTypeIds[i]);
				roleReportType.setCreateTime(new Date());
				roleReportType.setUpdateTime(new Date());
				roleReportTypeDao.insertTemplate(roleReportType);
			}
			//5更新角色信息
			BtRole role = new BtRole();
			role.setId(roleId);
			role.setUpdateTime(new Date());
			roleDao.updateTemplateById(role);
		} catch (Exception e) {
			logger.error(e.getMessage(),e.fillInStackTrace());
			throw new RuntimeException("服务器异常，请联系管理员");
		}
	}

	/** 
	 * @param id
	 * @param companyId
	 * @return
	 * @author ghl 2018年12月22日下午5:28:58
	 * getPermission:获取当前角色的权限(菜单+查看报告)
	 */  
	@Override
	public JSONObject getPermission(String id,String companyId) {
		JSONObject res = new JSONObject();
		//1.获取当前系统所有的菜单
		List<MenuInfo> allMenus = menuDao.all();
		// 保存树信息
		List<JSONObject> menuTreeData = new ArrayList<>();
		for (int i = 0; i < allMenus.size(); i++) {
			JSONObject obj = new JSONObject();
			obj.put("id", allMenus.get(i).getId());
			obj.put("menuName", allMenus.get(i).getMenuName());
			obj.put("parentId", allMenus.get(i).getParentId());
			menuTreeData.add(obj);
		}
		//2.获取当前角色的菜单
		List<BtRoleMenu> menus = roleMenuDao.getMenuByRoleId(id);
		if(menus.size()>0) {
			//比较两个集合中相同的菜单ID
			for (int i = 0; i < menuTreeData.size(); i++) {
				for (int j = 0; j < menus.size(); j++) {
					if(menuTreeData.get(i).get("id").equals(menus.get(j).getMenuId())) {
						//如果拥有该菜单ID,则需要给JSON对象添加一个属性代表默认选中
						menuTreeData.get(i).put("checked", true);
					}
				}
			}
		}
		res.put("menuTreeData", menuTreeData);
		//3.获取系统所有的报告类型
		List<BtReportType> reportTypeList = reportTypeDao.getAllReportTypeByCompanyId(companyId);
		List<JSONObject> reportTreeData = new ArrayList<>();
		if(reportTypeList!=null&&reportTypeList.size()>0) {
			for (int i = 0; i < reportTypeList.size(); i++) {
				JSONObject obj =new JSONObject();
				obj.put("id", reportTypeList.get(i).getId());
				obj.put("reportName", reportTypeList.get(i).getReportName());
				obj.put("parentId", 0);
				reportTreeData.add(obj);
			}
		}
		//4.获取当前角色的所有的可查看的报告类型
		List<BtRoleReportType> roleReportTypeList =roleReportTypeDao.getReportTypeByRoleId(id);
		if(roleReportTypeList!=null && roleReportTypeList.size()>0) {
			for (int i = 0; i < reportTreeData.size(); i++) {
				for (int j = 0; j < roleReportTypeList.size(); j++) {
					if(reportTreeData.get(i).get("id").equals(roleReportTypeList.get(j).getReportTypeId())) {
						reportTreeData.get(i).put("checked", true);
					}
				}
			}
		}
		res.put("reportTreeData", reportTreeData);
		
		return res;
	}

}