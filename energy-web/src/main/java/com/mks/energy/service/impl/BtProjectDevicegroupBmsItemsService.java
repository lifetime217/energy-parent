package com.mks.energy.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mks.energy.core.ext.AbstractBaseService;
import com.mks.energy.core.ext.BaseDao;
import com.mks.energy.dao.IBtProjectDevicegroupBmsItemsDao;
import com.mks.energy.entity.biz.BtProjectDevicegroupBmsItems;
import com.mks.energy.service.IBtProjectDevicegroupBmsItemsService;


/**
 * @author lifetime
 *
 */
@Service
public class BtProjectDevicegroupBmsItemsService extends AbstractBaseService<BtProjectDevicegroupBmsItems> implements IBtProjectDevicegroupBmsItemsService{
	@Autowired
	private IBtProjectDevicegroupBmsItemsDao dao;

	@Override
	public BaseDao<BtProjectDevicegroupBmsItems> getDao() {
		return dao;
	}
	
	@Override
	public String add(BtProjectDevicegroupBmsItems t) {
		return super.add(t);
	}

}