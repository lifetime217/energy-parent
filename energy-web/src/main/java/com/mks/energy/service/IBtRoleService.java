package com.mks.energy.service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.mks.energy.core.ext.IBaseService;
import com.mks.energy.entity.biz.BtRole;

/**
 * @author lifetime
 *
 */
public interface IBtRoleService extends IBaseService<BtRole>{

	/** 
	 * @author ghl  by 2018年12月17日下午5:34:13
	 * @param name
	 * @return 
	 * @since JDK 1.8
	 * checkNameUnique:
	 */ 
	BtRole checkNameUnique(String name);

	/** 
	 * @param roleId
	 * @param menuIds
	 * @param rptTypeIds 
	 * @return 
	 * @since JDK 1.8
	 * @author ghl  by 2018年12月19日上午12:36:43
	 * saveRoleMenu:保存当前角色的权限(菜单+查看报告)
	 */ 
	void saveRolePermission(String roleId, String[] menuIds, String[] rptTypeIds);

	/** 
	 * @param id
	 * @param companyId 
	 * @return 
	 * @since JDK 1.8
	 * @author ghl  by 2018年12月22日下午5:28:25
	 * getPermission:获取当前角色的权限(菜单+查看报告)
	 */ 
	JSONObject getPermission(String id, String companyId);
	
}
