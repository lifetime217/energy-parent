package com.mks.energy.service;

import com.mks.energy.core.ext.IBaseService;
import com.mks.energy.entity.biz.BtDataBmsCellv;

/**
 * @author lifetime
 *
 */
public interface IBtDataBmsCellvService extends IBaseService<BtDataBmsCellv>{
	
}
