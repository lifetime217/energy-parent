package com.mks.energy.service.impl;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mks.energy.core.ext.AbstractBaseService;
import com.mks.energy.core.ext.BaseDao;
import com.mks.energy.dao.IBtProjectRuntimeDao;
import com.mks.energy.entity.biz.BtProjectRuntime;
import com.mks.energy.service.IBtProjectRuntimeService;


/**
 * @author lifetime
 *
 */
@Service
public class BtProjectRuntimeService extends AbstractBaseService<BtProjectRuntime> implements IBtProjectRuntimeService{
	@Autowired
	private IBtProjectRuntimeDao projectRuntimeDao;

	@Override
	public BaseDao<BtProjectRuntime> getDao() {
		return projectRuntimeDao;
	}
	
	@Override
	public String add(BtProjectRuntime t) {
		return super.add(t);
	}

	/* (non-Javadoc)
	 * @see com.mks.energy.service.IBtProjectRuntimeService#getProjectRuntimeData(java.lang.String, java.lang.Integer)
	 */
	@Override
	public BtProjectRuntime getProjectRuntimeData(String projectId, Integer date) {
		BtProjectRuntime projectRuntime = projectRuntimeDao.getProjectRuntimeDataByProjectIdAndDate(projectId,date);
		if(projectRuntime!=null) {
			return projectRuntime;
		}else {
			//如果没有查到数据,可能是凌晨时刻,数据并未传输到储能平台.这里需要手动初始化,
			BtProjectRuntime btProjectRuntime = new BtProjectRuntime();
			btProjectRuntime.setProjectId(projectId);
			btProjectRuntime.setDateVal(date);
			btProjectRuntime.setCapKw(new BigDecimal(0));
			btProjectRuntime.setCapA(new BigDecimal(0));
			btProjectRuntime.setCapV(new BigDecimal(0));
			btProjectRuntime.setFill(new BigDecimal(0));
			btProjectRuntime.setFillTotal(new BigDecimal(0));
			btProjectRuntime.setOut(new BigDecimal(0));
			btProjectRuntime.setOutTotal(new BigDecimal(0));
			btProjectRuntime.setSafeDay(23);
			btProjectRuntime.setSysEffic(new BigDecimal(0));
			btProjectRuntime.setMoney(new BigDecimal(0));
			btProjectRuntime.setMoneyTotal(new BigDecimal(0));
			projectRuntimeDao.insertTemplate(btProjectRuntime);
			return btProjectRuntime;
		}
	}

}