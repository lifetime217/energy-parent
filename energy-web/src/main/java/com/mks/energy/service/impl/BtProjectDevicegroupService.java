package com.mks.energy.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mks.energy.core.ext.AbstractBaseService;
import com.mks.energy.core.ext.BaseDao;
import com.mks.energy.dao.IBtProjectDevicegroupDao;
import com.mks.energy.entity.biz.BtProjectDevicegroup;
import com.mks.energy.service.IBtProjectDevicegroupService;


/**
 * @author lifetime
 *
 */
@Service
public class BtProjectDevicegroupService extends AbstractBaseService<BtProjectDevicegroup> implements IBtProjectDevicegroupService{
	@Autowired
	private IBtProjectDevicegroupDao dao;

	@Override
	public BaseDao<BtProjectDevicegroup> getDao() {
		return dao;
	}
	
	@Override
	public String add(BtProjectDevicegroup t) {
		return super.add(t);
	}

}