package com.mks.energy.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mks.energy.core.ext.AbstractBaseService;
import com.mks.energy.core.ext.BaseDao;
import com.mks.energy.dao.IBtProjectDevicegroupBmsDao;
import com.mks.energy.entity.biz.BtProjectDevicegroupBms;
import com.mks.energy.service.IBtProjectDevicegroupBmsService;


/**
 * @author lifetime
 *
 */
@Service
public class BtProjectDevicegroupBmsService extends AbstractBaseService<BtProjectDevicegroupBms> implements IBtProjectDevicegroupBmsService{
	@Autowired
	private IBtProjectDevicegroupBmsDao dao;

	@Override
	public BaseDao<BtProjectDevicegroupBms> getDao() {
		return dao;
	}
	
	@Override
	public String add(BtProjectDevicegroupBms t) {
		return super.add(t);
	}

}