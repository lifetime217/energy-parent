package com.mks.energy.service;

import com.mks.energy.core.ext.IBaseService;
import com.mks.energy.entity.biz.BtDataBmsCellt;

/**
 * @author lifetime
 *
 */
public interface IBtDataBmsCelltService extends IBaseService<BtDataBmsCellt>{
	
}
