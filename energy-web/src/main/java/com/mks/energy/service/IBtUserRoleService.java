package com.mks.energy.service;

import com.mks.energy.core.ext.IBaseService;
import com.mks.energy.entity.biz.BtUserRole;

/**
 * @author lifetime
 *
 */
public interface IBtUserRoleService extends IBaseService<BtUserRole>{
	
}
