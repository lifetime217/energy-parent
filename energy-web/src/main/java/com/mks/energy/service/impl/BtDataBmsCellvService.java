package com.mks.energy.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mks.energy.core.ext.AbstractBaseService;
import com.mks.energy.core.ext.BaseDao;
import com.mks.energy.dao.IBtDataBmsCellvDao;
import com.mks.energy.entity.biz.BtDataBmsCellv;
import com.mks.energy.service.IBtDataBmsCellvService;


/**
 * @author lifetime
 *
 */
@Service
public class BtDataBmsCellvService extends AbstractBaseService<BtDataBmsCellv> implements IBtDataBmsCellvService{
	@Autowired
	private IBtDataBmsCellvDao dao;

	@Override
	public BaseDao<BtDataBmsCellv> getDao() {
		return dao;
	}
	
	@Override
	public String add(BtDataBmsCellv t) {
		return super.add(t);
	}

}