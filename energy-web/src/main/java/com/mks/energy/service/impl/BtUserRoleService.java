package com.mks.energy.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mks.energy.core.ext.AbstractBaseService;
import com.mks.energy.core.ext.BaseDao;
import com.mks.energy.dao.IBtUserRoleDao;
import com.mks.energy.entity.biz.BtUserRole;
import com.mks.energy.service.IBtUserRoleService;


/**
 * @author lifetime
 *
 */
@Service
public class BtUserRoleService extends AbstractBaseService<BtUserRole> implements IBtUserRoleService{
	@Autowired
	private IBtUserRoleDao dao;

	@Override
	public BaseDao<BtUserRole> getDao() {
		return dao;
	}
	
	@Override
	public String add(BtUserRole t) {
		return super.add(t);
	}

}