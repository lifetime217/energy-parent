package com.mks.energy.service;

import com.mks.energy.core.ext.IBaseService;
import com.mks.energy.entity.biz.BtProjectRuntime;

/**
 * @author lifetime
 *
 */
public interface IBtProjectRuntimeService extends IBaseService<BtProjectRuntime>{

	/**
	 * @author ghl
	 * @Date 2019年1月30日下午6:46:05
	 * @param projectId
	 * @param date
	 * @return
	 */
	BtProjectRuntime getProjectRuntimeData(String projectId, Integer date);
	
}
