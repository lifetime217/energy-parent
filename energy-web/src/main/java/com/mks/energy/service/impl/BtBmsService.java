package com.mks.energy.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mks.energy.core.ext.AbstractBaseService;
import com.mks.energy.core.ext.BaseDao;
import com.mks.energy.dao.IBtBmsDao;
import com.mks.energy.entity.biz.BtBms;
import com.mks.energy.service.IBtBmsService;

/**
 * @author lifetime
 *
 */
@Service
public class BtBmsService extends AbstractBaseService<BtBms> implements IBtBmsService {
	@Autowired
	private IBtBmsDao dao;

	@Override
	public BaseDao<BtBms> getDao() {
		return dao;
	}

	public String existsAndSave(BtBms bms) {
		BtBms exisInfo = new BtBms();
		exisInfo.setBmsFactory(bms.getBmsFactory());
		exisInfo.setBmsSoftVersion(bms.getBmsSoftVersion());
		exisInfo.setBmsHardVersion(bms.getBmsHardVersion());
		exisInfo.setBmsBalancedType(bms.getBmsBalancedType());
		exisInfo.setBatteryType(bms.getBatteryType());
		exisInfo = dao.templateOne(exisInfo);
		if (exisInfo == null) {
			dao.insert(bms);
			return bms.getId();
		} else {
			return exisInfo.getId();
		}
	}

	@Override
	public String add(BtBms t) {
		return super.add(t);
	}

}