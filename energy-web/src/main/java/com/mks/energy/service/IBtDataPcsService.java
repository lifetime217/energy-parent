package com.mks.energy.service;

import com.mks.energy.core.ext.IBaseService;
import com.mks.energy.entity.biz.BtDataPcs;

/**
 * @author lifetime
 *
 */
public interface IBtDataPcsService extends IBaseService<BtDataPcs>{
	
}
