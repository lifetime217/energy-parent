package com.mks.energy.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mks.energy.core.ext.AbstractBaseService;
import com.mks.energy.core.ext.BaseDao;
import com.mks.energy.dao.IBtMenuDao;
import com.mks.energy.entity.biz.BtMenu;
import com.mks.energy.service.IBtMenuService;


/**
 * @author lifetime
 *
 */
@Service
public class BtMenuService extends AbstractBaseService<BtMenu> implements IBtMenuService{
	@Autowired
	private IBtMenuDao dao;

	@Override
	public BaseDao<BtMenu> getDao() {
		return dao;
	}
	
	@Override
	public String add(BtMenu t) {
		return super.add(t);
	}

}