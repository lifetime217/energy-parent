package com.mks.energy.service.impl;

import java.util.Map;

import org.beetl.sql.core.SQLManager;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.mks.energy.entity.F4PageQueryInfo;
import com.mks.energy.service.IF4Service;

/**
 * @author lifetime
 *
 */
@Service
public class F4Service implements IF4Service{
	@Autowired
	private SQLManager sqlManager;

	@Override
	public PageQuery<Map<String, Object>> f4Query(String sign, F4PageQueryInfo f4PageQueryInfo) {
		PageQuery<Map<String, Object>> pq = new PageQuery<>();
		if(!StringUtils.isEmpty(f4PageQueryInfo.getOrderBy())){
			pq.setOrderBy(f4PageQueryInfo.getOrderBy());
		}
		pq.setPageNumber(f4PageQueryInfo.getPageNumber());
		pq.setPageSize(f4PageQueryInfo.getPageSize());
		pq.setParas(f4PageQueryInfo.getQueryParams());
		sqlManager.pageQuery("f4query." + sign, Map.class, pq,null);
		return pq;
	}

}
