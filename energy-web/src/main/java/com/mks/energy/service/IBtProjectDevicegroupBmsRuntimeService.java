package com.mks.energy.service;

import com.mks.energy.core.ext.IBaseService;
import com.mks.energy.entity.biz.BtProjectDevicegroupBmsRuntime;

/**
 * @author lifetime
 *
 */
public interface IBtProjectDevicegroupBmsRuntimeService extends IBaseService<BtProjectDevicegroupBmsRuntime>{
	
}
