package com.mks.energy.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mks.energy.core.ext.AbstractBaseService;
import com.mks.energy.core.ext.BaseDao;
import com.mks.energy.dao.IBtUserReportTypeDao;
import com.mks.energy.entity.biz.BtUserReportType;
import com.mks.energy.service.IBtUserReportTypeService;


/**
 * @author lifetime
 *
 */
@Service
public class BtUserReportTypeService extends AbstractBaseService<BtUserReportType> implements IBtUserReportTypeService{
	@Autowired
	private IBtUserReportTypeDao dao;

	@Override
	public BaseDao<BtUserReportType> getDao() {
		return dao;
	}
	
	@Override
	public String add(BtUserReportType t) {
		return super.add(t);
	}

}