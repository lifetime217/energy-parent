package com.mks.energy.service.impl;

import java.util.Map;

import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.mks.energy.core.UserContext;
import com.mks.energy.core.ext.AbstractBaseService;
import com.mks.energy.core.ext.BaseDao;
import com.mks.energy.dao.IBtReportTypeDao;
import com.mks.energy.entity.biz.BtReportType;
import com.mks.energy.service.IBtReportTypeService;
import com.mks.energy.utils.SysUtil;


/**
 * @author lifetime
 *
 */
@Service
public class BtReportTypeService extends AbstractBaseService<BtReportType> implements IBtReportTypeService{
	@Autowired
	private IBtReportTypeDao reportTypeDao;

	@Override
	public BaseDao<BtReportType> getDao() {
		return reportTypeDao;
	}
	
	@Override
	public String add(BtReportType t) {
		return super.add(t);
	}
	
	/** 
	 * @param pageQuery
	 * @return
	 * @author ghl 2018年12月22日上午2:59:06
	 * getQueryList:
	 */  
	@Override
	public PageQuery<BtReportType> getQueryList(PageQuery<BtReportType> pageQuery) {
		Map<String, Object> map = (Map<String, Object>) pageQuery.getParas();
		map.put("company_id", UserContext.get().getCompanyId());// 添加查询参数（公司ID）
		map.put("isSystemUser", SysUtil.isSystemUser());
		if (StringUtils.isEmpty(pageQuery.getOrderBy())) {
			pageQuery.setOrderBy("create_time DESC");
		}
		reportTypeDao.queryPage(pageQuery);
		return pageQuery;
	}

	/** 
	 * @param reportName
	 * @return
	 * @author ghl 2018年12月22日上午3:44:18
	 * checkNameUnique:
	 */  
	@Override
	public BtReportType checkNameUnique(String reportName) {
		
		return reportTypeDao.getReportTypeByName(reportName);
	}

}