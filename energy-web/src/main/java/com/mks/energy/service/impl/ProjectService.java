package com.mks.energy.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;

import org.beetl.sql.core.db.KeyHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSONArray;
import com.mks.energy.core.UserContext;
import com.mks.energy.core.ext.AbstractBaseService;
import com.mks.energy.core.ext.BaseDao;
import com.mks.energy.dao.IBtDataBmsCelltDao;
import com.mks.energy.dao.IBtDataBmsCellvDao;
import com.mks.energy.dao.IBtDataBmsDao;
import com.mks.energy.dao.IBtDataPcsDao;
import com.mks.energy.dao.IBtProjectDao;
import com.mks.energy.dao.IBtProjectDevicegroupBmsDao;
import com.mks.energy.dao.IBtProjectDevicegroupBmsItemsDao;
import com.mks.energy.dao.IBtProjectDevicegroupDao;
import com.mks.energy.dao.IBtProjectDevicegroupPcsDao;
import com.mks.energy.entity.biz.BtBms;
import com.mks.energy.entity.biz.BtDataBms;
import com.mks.energy.entity.biz.BtDataBmsCellt;
import com.mks.energy.entity.biz.BtDataBmsCellv;
import com.mks.energy.entity.biz.BtDataPcs;
import com.mks.energy.entity.biz.BtPcs;
import com.mks.energy.entity.biz.BtProject;
import com.mks.energy.entity.biz.BtProjectDevicegroup;
import com.mks.energy.entity.biz.BtProjectDevicegroupBms;
import com.mks.energy.entity.biz.BtProjectDevicegroupBmsItems;
import com.mks.energy.entity.biz.BtProjectDevicegroupPcs;
import com.mks.energy.service.IBtBmsService;
import com.mks.energy.service.IBtPcsService;
import com.mks.energy.service.IProjectService;
import com.mks.energy.utils.ShortUuid;
import com.mks.energy.utils.ZTreeInfo;

/**
 * @author lifetime
 *
 */
@Service
public class ProjectService extends AbstractBaseService<BtProject> implements IProjectService {
	private static final Logger log = LoggerFactory.getLogger(ProjectService.class);

	@Autowired
	private IBtProjectDao projectDao;

	@Autowired
	private IBtProjectDevicegroupDao devicegroupDao;

	@Autowired
	private IBtProjectDevicegroupBmsDao devicegroupBmsDao;

	@Autowired
	private IBtProjectDevicegroupPcsDao devicegroupPcsDao;

	@Autowired
	private IBtProjectDevicegroupBmsItemsDao devicegroupBmsItemsDao;

	@Autowired
	private IBtDataBmsDao dataBmsdao;
	
	@Autowired
	private IBtDataPcsDao dataPcsdao;

	@Autowired
	private IBtDataBmsCelltDao bmsCelltDao;

	@Autowired
	private IBtDataBmsCellvDao bmsCellvDao;

	@Autowired
	private IBtPcsService pcsService;

	@Autowired
	private IBtBmsService bmsService;

	@Override
	public BaseDao<BtProject> getDao() {
		return projectDao;
	}

	@Override
	public String add(BtProject t) {
		t.setProjectSign(ShortUuid.generateShortUuid());
		return super.add(t);
	}
	
	@Transactional
	public Integer saveBmsData(BtDataBms bmsData, List<BtDataBmsCellt> itemts, List<BtDataBmsCellv> itemvs) {
		bmsData.setAddTime(Long.valueOf(System.currentTimeMillis() / 1000).intValue());
		KeyHolder holder = dataBmsdao.insertReturnKey(bmsData);
		int id = holder.getInt();
		for (BtDataBmsCellt item : itemts) {
			item.setBmsDataId(id);
		}
		bmsCelltDao.insertBatch(itemts);

		for (BtDataBmsCellv item : itemvs) {
			item.setBmsDataId(id);
		}
		bmsCellvDao.insertBatch(itemvs);
		return id;
	}
	
	
	public Integer savePcsData(BtDataPcs pcsData){
		pcsData.setAddTime(Long.valueOf(System.currentTimeMillis() / 1000).intValue());
		KeyHolder holder = dataPcsdao.insertReturnKey(pcsData);
		return holder.getInt();
	}

	public String getTreeJsonString(String projectId) {
		JSONArray array = new JSONArray();
		ZTreeInfo root = new ZTreeInfo();
		// 1.一级：项目
		BtProject pinfo = projectDao.unique(projectId);
		root.id = 0;
		root.uuid = pinfo.getId();
		root.pId = -1;
		root.name = pinfo.getName();
		root.open = true;
		root.icon = UserContext.getCtxPath() + "/imgs/商场_16.png";
		array.add(root);
		// 2.二级：设备组
		List<BtProjectDevicegroup> devicegroups = devicegroupDao.getGroup(projectId);
		for (int i = 0; i < devicegroups.size(); i++) {
			BtProjectDevicegroup group = devicegroups.get(i);
			ZTreeInfo two = new ZTreeInfo();
			two.id = Integer.valueOf("" + 20 + i);
			two.uuid = group.getId();
			two.name = group.getName();
			two.pId = root.id;
			two.open = true;
			array.add(two);

			// 3.三级：PCS
			BtProjectDevicegroupPcs pcs = devicegroupPcsDao.getPcs(group.getId());
			if (pcs != null) {
				ZTreeInfo three = new ZTreeInfo();
				three.id = Integer.valueOf("" + 3000 + i);
				three.uuid = pcs.getId();
				three.name = "PCS";
				three.pId = two.id;
				three.icon = UserContext.getCtxPath() + "/imgs/pcs_16.png";
				array.add(three);
			}

			// 3.三级：BMS
			List<BtProjectDevicegroupBms> bms = devicegroupBmsDao.getBms(group.getId());
			if (bms != null && !bms.isEmpty()) {
				for (int j = 0; j < bms.size(); j++) {
					BtProjectDevicegroupBms bmsInfo = bms.get(j);
					ZTreeInfo three = new ZTreeInfo();
					three.id = Integer.valueOf("" + 3200 + i + j);
					three.uuid = bmsInfo.getId();
					three.pId = two.id;
					three.name = "BMS(" + (j + 1) + ")";
					three.icon = UserContext.getCtxPath() + "/imgs/BMS_16.png";
					three.open = true;
					array.add(three);

					List<BtProjectDevicegroupBmsItems> bmsItems = devicegroupBmsItemsDao.getBmsItems(bmsInfo.getId());
					TreeMap<String, List<BtProjectDevicegroupBmsItems>> itemGroup = getItemGroupNames(bmsItems);
					// 4.四级：电池模组
					int groupIndex = 100000;
					for (Iterator<String> it = itemGroup.keySet().iterator(); it.hasNext();) {
						String bgroup = it.next();
						ZTreeInfo four = new ZTreeInfo();
						four.id = Integer.valueOf("" + 40000 + i + j) + groupIndex++;
						four.pId = three.id;
						four.name = bgroup;
						four.icon = UserContext.getCtxPath() + "/imgs/电池模组_16.png";
						four.open = true;
						array.add(four);

						// 5.五级：电池单体
						List<BtProjectDevicegroupBmsItems> groupItems = itemGroup.get(bgroup);
						for (int k = 0; k < groupItems.size(); k++) {
							BtProjectDevicegroupBmsItems groupItem = groupItems.get(k);

							ZTreeInfo five = new ZTreeInfo();
							five.id = Integer.valueOf("" + 50000 + i + j + k);
							five.pId = four.id;
							five.name = groupItem.getName();
							five.icon = UserContext.getCtxPath() + "/imgs/电池_16.png";
							array.add(five);
						}
					}
				}
			}

		}

		return array.toJSONString();
	}

	TreeMap<String, List<BtProjectDevicegroupBmsItems>> getItemGroupNames(List<BtProjectDevicegroupBmsItems> bmsItems) {
		TreeMap<String, List<BtProjectDevicegroupBmsItems>> set = new TreeMap<>();
		for (int k = 0; k < bmsItems.size(); k++) {
			BtProjectDevicegroupBmsItems bmsItem = bmsItems.get(k);
			if (set.containsKey(bmsItem.getBgroupName())) {
				set.get(bmsItem.getBgroupName()).add(bmsItem);
			} else {
				List<BtProjectDevicegroupBmsItems> newList = new ArrayList<>();
				newList.add(bmsItem);
				set.put(bmsItem.getBgroupName(), newList);
			}
		}
		return set;
	}

	@Override
	@Transactional
	public String saveDeviceGroupInfo(String projectId, String groupName, BtPcs pcs, String pcsSign, BtBms[] bmsList,
			String[] bmsSigns, BtProjectDevicegroupBmsItems[] items) {
		// 1. 先形成项目的设备组
		BtProjectDevicegroup devicegroup = new BtProjectDevicegroup();
		devicegroup.setProjectId(projectId);
		devicegroup.setName(groupName);
		devicegroup.setPcsNum(1);
		devicegroup.setAddTime(Long.valueOf(System.currentTimeMillis()/1000).intValue());
		devicegroup.setBmsNum(bmsList != null ? bmsList.length : 0);
		devicegroup.setBatteryNum(items != null ? items.length : 0);
		devicegroup.setIsDelete(0);
		devicegroupDao.insert(devicegroup);
		String projectGroupId = devicegroup.getId();
		// 2. 将pcs加入设备组。
		String pcsId = pcsService.existsAndSave(pcs);
		BtProjectDevicegroupPcs groupPcs = new BtProjectDevicegroupPcs();
		groupPcs.setProjectGroupId(projectGroupId);
		groupPcs.setProjectId(projectId);
		groupPcs.setPcsId(pcsId);
		groupPcs.setPcsSign(pcsSign);
		devicegroupPcsDao.insert(groupPcs);
		// 3. 将多个bms加入设备组。
		if (bmsList != null && bmsSigns != null) {
			if (bmsList.length != bmsSigns.length) {
				log.error("请注意：BMS数量与电池单体里涉及的BMS数据不一致");
			}
			for (int i = 0; i < bmsList.length; i++) {
				String bmsId = bmsService.existsAndSave(bmsList[i]);
				BtProjectDevicegroupBms devicegroupBms = new BtProjectDevicegroupBms();
				devicegroupBms.setDevicegroupId(projectGroupId);
				devicegroupBms.setBmsId(bmsId);
				devicegroupBms.setBmsSign(bmsSigns[i]);
				devicegroupBms.setAddTime(((Long) (System.currentTimeMillis() / 1000)).intValue());
				devicegroupBmsDao.insert(devicegroupBms);
				String devicegroupBmsId = devicegroupBms.getId();
				// 4. 保存bms里的电池单体数据
				List<BtProjectDevicegroupBmsItems> bmsItems = getBmsItems(bmsSigns[i], items);
				for (int j = 0; j < bmsItems.size(); j++) {
					BtProjectDevicegroupBmsItems item = bmsItems.get(j);
					item.setDevicegroupBmsId(devicegroupBmsId);
					devicegroupBmsItemsDao.insert(item);
				}
			}
		}
		return devicegroup.getId();
	}

	/**
	 * 获取bms下的所有单体
	 * 
	 * @param bmsSign
	 * @param items
	 */
	protected List<BtProjectDevicegroupBmsItems> getBmsItems(String bmsSign, BtProjectDevicegroupBmsItems[] items) {
		List<BtProjectDevicegroupBmsItems> list = new ArrayList<BtProjectDevicegroupBmsItems>();
		for (int i = 0; i < items.length; i++) {
			if (bmsSign.equals(items[i].getString("bms_sign"))) {
				list.add(items[i]);
			}
		}
		return list;
	}

	@Override
	public void deleteDeviceGroupInfo(String projectId) {
		devicegroupDao.deleteByProject(projectId);
	}

}
