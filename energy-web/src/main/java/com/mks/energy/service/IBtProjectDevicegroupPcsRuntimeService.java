package com.mks.energy.service;

import com.mks.energy.core.ext.IBaseService;
import com.mks.energy.entity.biz.BtProjectDevicegroupPcsRuntime;

/**
 * @author lifetime
 *
 */
public interface IBtProjectDevicegroupPcsRuntimeService extends IBaseService<BtProjectDevicegroupPcsRuntime>{
	
}
