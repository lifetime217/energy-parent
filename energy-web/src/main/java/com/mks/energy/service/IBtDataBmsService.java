package com.mks.energy.service;

import com.mks.energy.core.ext.IBaseService;
import com.mks.energy.entity.biz.BtDataBms;

/**
 * @author lifetime
 *
 */
public interface IBtDataBmsService extends IBaseService<BtDataBms>{
	
}
