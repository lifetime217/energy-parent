package com.mks.energy.service;

import com.mks.energy.core.ext.IBaseService;
import com.mks.energy.entity.biz.BtRoleReportType;

/**
 * @author lifetime
 *
 */
public interface IBtRoleReportTypeService extends IBaseService<BtRoleReportType>{
	
}
