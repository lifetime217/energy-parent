package com.mks.energy.service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.mks.energy.core.UserMenuVO;
import com.mks.energy.core.ext.IBaseService;
import com.mks.energy.entity.biz.MenuInfo;
import com.mks.energy.entity.biz.UserInfo;

public interface IUserService extends IBaseService<UserInfo> {

	public UserInfo getUser(String number, String pwd);

	/**
	 * 是否是有效的原始密码
	 * 
	 * @param number
	 * @param pwd
	 * @return
	 */
	public boolean pwdSuccess(String number, String pwd);

	/**
	 * 修改用户密码
	 * 
	 * @param number
	 * @param newPwd
	 */
	public void modifyPwd(String number, String newPwd);

	public UserInfo getInfoByNumber(String number);

	// 获取用户菜单
	public List<UserMenuVO> getUserMenus(String userId);
	
	/**
	 * 获取用户菜单
	 * @param uinfo
	 * @return
	 */
	public List<UserMenuVO> getUserMenus(UserInfo uinfo);

	// 获取菜单树
	public List<MenuInfo> getMenuTree();

	// 校验用户名是否被占用
	public UserInfo checkUserName(String loginNo);

	/** 
	 * @param id
	 * @param companyId 
	 * @return 
	 * @since JDK 1.8
	 * @author ghl  by 2018年12月20日上午1:11:51
	 * getAllPermissionByUserId:/获取当前用户所有的权限
	 */ 
	public JSONObject getAllPermissionByUserId(String id, String companyId);

	/** 
	 * @param id 
	 * @param roleIds 
	 * @since JDK 1.8
	 * @author ghl  by 2018年12月21日上午12:18:06
	 * saveAllMyPermission:根据用户ID,角色ID数组更新保存权限
	 */ 
	public void saveAllMyPermission(String id, String[] roleIds);

}
