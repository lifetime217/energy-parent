package com.mks.energy.service;

import com.mks.energy.core.ext.IBaseService;
import com.mks.energy.entity.biz.BtProjectDevicegroupModuleRuntime;

/**
 * @author lifetime
 *
 */
public interface IBtProjectDevicegroupModuleRuntimeService extends IBaseService<BtProjectDevicegroupModuleRuntime>{
	
}
