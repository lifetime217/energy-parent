package com.mks.energy.service;

import com.mks.energy.core.ext.IBaseService;
import com.mks.energy.entity.biz.BtReportType;

/**
 * @author lifetime
 *
 */
public interface IBtReportTypeService extends IBaseService<BtReportType>{

	/** 
	 * @param reportName
	 * @return 
	 * @since JDK 1.8
	 * @author ghl  by 2018年12月22日上午3:43:48
	 * checkNameUnique:
	 */ 
	BtReportType checkNameUnique(String reportName);
	
}
