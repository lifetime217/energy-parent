package com.mks.energy.service;

import java.util.List;

import com.mks.energy.core.ext.IBaseService;
import com.mks.energy.entity.biz.BtBms;
import com.mks.energy.entity.biz.BtDataBms;
import com.mks.energy.entity.biz.BtDataBmsCellt;
import com.mks.energy.entity.biz.BtDataBmsCellv;
import com.mks.energy.entity.biz.BtDataPcs;
import com.mks.energy.entity.biz.BtPcs;
import com.mks.energy.entity.biz.BtProject;
import com.mks.energy.entity.biz.BtProjectDevicegroupBmsItems;

public interface IProjectService extends IBaseService<BtProject> {

	/**
	 * 保存pcs的数据
	 * 
	 * @param pcsData
	 * @return
	 */
	public Integer savePcsData(BtDataPcs pcsData);

	/**
	 * 保存bms的数据以及bms下面所有单体温度和电压的数据
	 * 
	 * @param bmsData
	 *            一条bms的数据包
	 * @param itemts
	 *            所有单体温度数据
	 * @param itemvs
	 *            所有单体电压数据
	 * @return
	 */
	public Integer saveBmsData(BtDataBms bmsData, List<BtDataBmsCellt> itemts, List<BtDataBmsCellv> itemvs);

	/**
	 * 一次性保存好项目的设备关系数据
	 * 
	 * @param projectId
	 *            项目id
	 * @param groupName
	 *            设备组名称
	 * @param pcs
	 *            设备组里的pcs
	 * @param pcsSign
	 *            pcs的唯一标识符
	 * @param bmsList
	 *            设备组里的bms，允许存在多个
	 * @param bmsSigns
	 *            bms的唯一标识符
	 * @param items
	 *            bms下面的电池单体数据
	 * @return
	 */
	public String saveDeviceGroupInfo(String projectId, String groupName, BtPcs pcs, String pcsSign, BtBms[] bmsList,
			String[] bmsSigns, BtProjectDevicegroupBmsItems[] items);

	/**
	 * 逻辑删除项目的设备组
	 * 
	 * @param projectId
	 */
	public void deleteDeviceGroupInfo(String projectId);

	/**
	 * 获取项目的结构树
	 * 
	 * @param projectId
	 * @return
	 */
	public String getTreeJsonString(String projectId);

}
