package com.mks.energy.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mks.energy.core.ext.AbstractBaseService;
import com.mks.energy.core.ext.BaseDao;
import com.mks.energy.dao.IBtDataBmsDao;
import com.mks.energy.entity.biz.BtDataBms;
import com.mks.energy.service.IBtDataBmsService;


/**
 * @author lifetime
 *
 */
@Service
public class BtDataBmsService extends AbstractBaseService<BtDataBms> implements IBtDataBmsService{
	@Autowired
	private IBtDataBmsDao dao;

	@Override
	public BaseDao<BtDataBms> getDao() {
		return dao;
	}
	
	@Override
	public String add(BtDataBms t) {
		return super.add(t);
	}

}