package com.mks.energy.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mks.energy.core.ext.AbstractBaseService;
import com.mks.energy.core.ext.BaseDao;
import com.mks.energy.dao.IBtProjectDevicegroupModuleRuntimeDao;
import com.mks.energy.entity.biz.BtProjectDevicegroupModuleRuntime;
import com.mks.energy.service.IBtProjectDevicegroupModuleRuntimeService;


/**
 * @author lifetime
 *
 */
@Service
public class BtProjectDevicegroupModuleRuntimeService extends AbstractBaseService<BtProjectDevicegroupModuleRuntime> implements IBtProjectDevicegroupModuleRuntimeService{
	@Autowired
	private IBtProjectDevicegroupModuleRuntimeDao dao;

	@Override
	public BaseDao<BtProjectDevicegroupModuleRuntime> getDao() {
		return dao;
	}
	
	@Override
	public String add(BtProjectDevicegroupModuleRuntime t) {
		return super.add(t);
	}

}