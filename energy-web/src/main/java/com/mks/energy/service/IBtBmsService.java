package com.mks.energy.service;

import com.mks.energy.core.ext.IBaseService;
import com.mks.energy.entity.biz.BtBms;

/**
 * @author lifetime
 *
 */
public interface IBtBmsService extends IBaseService<BtBms> {
	/**
	 * 根据bms的组合字段信息一起检查bms是否存在，存在则返回数据库记录的对象，不存在则新增一个bms信息后返回id
	 * 
	 * @param bms
	 * @return
	 */
	public String existsAndSave(BtBms bms);
}
