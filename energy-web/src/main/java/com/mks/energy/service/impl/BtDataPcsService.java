package com.mks.energy.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mks.energy.core.ext.AbstractBaseService;
import com.mks.energy.core.ext.BaseDao;
import com.mks.energy.dao.IBtDataPcsDao;
import com.mks.energy.entity.biz.BtDataPcs;
import com.mks.energy.service.IBtDataPcsService;


/**
 * @author lifetime
 *
 */
@Service
public class BtDataPcsService extends AbstractBaseService<BtDataPcs> implements IBtDataPcsService{
	@Autowired
	private IBtDataPcsDao dao;

	@Override
	public BaseDao<BtDataPcs> getDao() {
		return dao;
	}
	
	@Override
	public String add(BtDataPcs t) {
		return super.add(t);
	}

}