package com.mks.energy.service;

import com.mks.energy.core.ext.IBaseService;
import com.mks.energy.entity.biz.BtPcs;

/**
 * @author lifetime
 *
 */
public interface IBtPcsService extends IBaseService<BtPcs> {
	/**
	 * 根据pcs的组合字段信息一起检查pcs是否存在，存在则返回数据库记录的对象，不存在则新增一个pcs信息后返回id
	 * 
	 * @param pcs
	 * @return
	 */
	public String existsAndSave(BtPcs pcs);
}
