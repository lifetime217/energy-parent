package com.mks.energy.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mks.energy.core.ext.AbstractBaseService;
import com.mks.energy.core.ext.BaseDao;
import com.mks.energy.dao.IBtProjectDevicegroupPcsDao;
import com.mks.energy.entity.biz.BtProjectDevicegroupPcs;
import com.mks.energy.service.IBtProjectDevicegroupPcsService;


/**
 * @author lifetime
 *
 */
@Service
public class BtProjectDevicegroupPcsService extends AbstractBaseService<BtProjectDevicegroupPcs> implements IBtProjectDevicegroupPcsService{
	@Autowired
	private IBtProjectDevicegroupPcsDao dao;

	@Override
	public BaseDao<BtProjectDevicegroupPcs> getDao() {
		return dao;
	}
	
	@Override
	public String add(BtProjectDevicegroupPcs t) {
		return super.add(t);
	}

}