package com.mks.energy.service;

import com.mks.energy.core.ext.IBaseService;
import com.mks.energy.entity.biz.BtMenu;

/**
 * @author lifetime
 *
 */
public interface IBtMenuService extends IBaseService<BtMenu>{
	
}
