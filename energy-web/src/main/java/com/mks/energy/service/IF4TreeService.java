package com.mks.energy.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface IF4TreeService {
	/**
	 * 请至少保持Map包含{"id","name","pId"}三个key
	 * 
	 * @param sign
	 *            标识
	 * @param params
	 *            查询参数
	 * @param fields
	 *            可查询出的属性字段(除id,name,pid以外的字段)
	 * @param openLevel
	 *            树打开的级别
	 * @return
	 */
	public List<HashMap> tree(String sign, Map<String, Object> params, String[] fields, Integer openLevel);

	/**
	 * 请至少保持Map包含{"id","name","pId"}三个key
	 * 
	 * @param sign
	 *            标识
	 * @param fields
	 *            可查询出的属性字段(除id,name,pid以外的字段)
	 * @param openLevel
	 *            树打开的级别
	 * @return
	 */
	public List<HashMap> tree(String sign, String[] fields, Integer openLevel);

	/**
	 * 请至少保持Map包含{"id","name","pId"}三个key
	 * 
	 * @param sign
	 *            标识
	 * @param openLevel
	 *            树打开的级别
	 * @return
	 */
	public List<HashMap> tree(String sign, Integer openLevel);

	/**
	 * 请至少保持Map包含{"id","name","pId"}三个key
	 * 
	 * @param sign
	 *            标识
	 * @return
	 */
	public List<HashMap> tree(String sign);
}
