package com.mks.energy.service;

import com.mks.energy.core.ext.IBaseService;
import com.mks.energy.entity.biz.BtProjectDevicegroup;

/**
 * @author lifetime
 *
 */
public interface IBtProjectDevicegroupService extends IBaseService<BtProjectDevicegroup>{
	
}
