package com.mks.energy.service;

import java.util.List;

import com.mks.energy.core.ext.IBaseService;
import com.mks.energy.entity.biz.BtRoleMenu;

/**
 * @author lifetime
 *
 */
public interface IBtRoleMenuService extends IBaseService<BtRoleMenu>{

	/** 
	 * @param id
	 * @return 
	 * @since JDK 1.8
	 * @author ghl  by 2018年12月18日下午11:06:50
	 * getMenuByRoleId:
	 */ 
	List<BtRoleMenu> getMenuByRoleId(String id);
	
}
