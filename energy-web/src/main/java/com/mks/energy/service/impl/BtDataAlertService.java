package com.mks.energy.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mks.energy.core.ext.AbstractBaseService;
import com.mks.energy.core.ext.BaseDao;
import com.mks.energy.dao.IBtDataAlertDao;
import com.mks.energy.entity.biz.BtDataAlert;
import com.mks.energy.service.IBtDataAlertService;


/**
 * @author lifetime
 *
 */
@Service
public class BtDataAlertService extends AbstractBaseService<BtDataAlert> implements IBtDataAlertService{
	@Autowired
	private IBtDataAlertDao dataAlertDao;

	@Override
	public BaseDao<BtDataAlert> getDao() {
		return dataAlertDao;
	}
	
	@Override
	public String add(BtDataAlert t) {
		return super.add(t);
	}

	/* (non-Javadoc)
	 * @see com.mks.energy.service.IBtDataAlertService#getBugByProjectId(java.lang.String)
	 */
	@Override
	public List<BtDataAlert> getBugByProjectId(String projectId) {
		
		return dataAlertDao.getBugByProjectId(projectId);
	}

}