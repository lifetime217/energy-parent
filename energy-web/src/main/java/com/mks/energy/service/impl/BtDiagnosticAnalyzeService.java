package com.mks.energy.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mks.energy.core.ext.AbstractBaseService;
import com.mks.energy.core.ext.BaseDao;
import com.mks.energy.dao.IBtDiagnosticAnalyzeDao;
import com.mks.energy.entity.biz.BtDiagnosticAnalyze;
import com.mks.energy.service.IBtDiagnosticAnalyzeService;


/**
 * @author lifetime
 *
 */
@Service
public class BtDiagnosticAnalyzeService extends AbstractBaseService<BtDiagnosticAnalyze> implements IBtDiagnosticAnalyzeService{
	@Autowired
	private IBtDiagnosticAnalyzeDao dao;

	@Override
	public BaseDao<BtDiagnosticAnalyze> getDao() {
		return dao;
	}
	
	@Override
	public String add(BtDiagnosticAnalyze t) {
		return super.add(t);
	}

}