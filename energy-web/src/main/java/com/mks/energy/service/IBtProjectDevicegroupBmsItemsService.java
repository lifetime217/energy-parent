package com.mks.energy.service;

import com.mks.energy.core.ext.IBaseService;
import com.mks.energy.entity.biz.BtProjectDevicegroupBmsItems;

/**
 * @author lifetime
 *
 */
public interface IBtProjectDevicegroupBmsItemsService extends IBaseService<BtProjectDevicegroupBmsItems>{
	
}
