package com.mks.energy.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mks.energy.core.ext.AbstractBaseService;
import com.mks.energy.core.ext.BaseDao;
import com.mks.energy.dao.IBtRoleMenuDao;
import com.mks.energy.entity.biz.BtRoleMenu;
import com.mks.energy.service.IBtRoleMenuService;


/**
 * @author lifetime
 *
 */
@Service
public class BtRoleMenuService extends AbstractBaseService<BtRoleMenu> implements IBtRoleMenuService{
	@Autowired
	private IBtRoleMenuDao roleMenuDao;

	@Override
	public BaseDao<BtRoleMenu> getDao() {
		return roleMenuDao;
	}
	
	@Override
	public String add(BtRoleMenu t) {
		return super.add(t);
	}

	/** 
	 * @param id
	 * @return
	 * @author ghl 2018年12月18日下午11:07:09
	 * getMenuByRoleId:获取当前角色的所有菜单
	 */  
	@Override
	public List<BtRoleMenu> getMenuByRoleId(String id) {
		List<BtRoleMenu> list = roleMenuDao.getMenuByRoleId(id);
		return list;
	}

}