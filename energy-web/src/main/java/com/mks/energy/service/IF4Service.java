package com.mks.energy.service;

import java.util.Map;

import org.beetl.sql.core.engine.PageQuery;

import com.mks.energy.entity.F4PageQueryInfo;

/**
 * @author lifetime
 *
 */
public interface IF4Service {
	
	public PageQuery<Map<String, Object>> f4Query(String sign, F4PageQueryInfo f4PageQueryInfo);
}
