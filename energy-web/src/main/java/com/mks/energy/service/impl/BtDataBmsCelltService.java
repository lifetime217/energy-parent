package com.mks.energy.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mks.energy.core.ext.AbstractBaseService;
import com.mks.energy.core.ext.BaseDao;
import com.mks.energy.dao.IBtDataBmsCelltDao;
import com.mks.energy.entity.biz.BtDataBmsCellt;
import com.mks.energy.service.IBtDataBmsCelltService;


/**
 * @author lifetime
 *
 */
@Service
public class BtDataBmsCelltService extends AbstractBaseService<BtDataBmsCellt> implements IBtDataBmsCelltService{
	@Autowired
	private IBtDataBmsCelltDao dao;

	@Override
	public BaseDao<BtDataBmsCellt> getDao() {
		return dao;
	}
	
	@Override
	public String add(BtDataBmsCellt t) {
		return super.add(t);
	}

}