package com.mks.energy.service;

import com.mks.energy.core.ext.IBaseService;
import com.mks.energy.entity.biz.BtProjectDevicegroupPcs;

/**
 * @author lifetime
 *
 */
public interface IBtProjectDevicegroupPcsService extends IBaseService<BtProjectDevicegroupPcs>{
	
}
