package com.mks.energy.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mks.energy.core.ext.AbstractBaseService;
import com.mks.energy.core.ext.BaseDao;
import com.mks.energy.dao.IBtRoleReportTypeDao;
import com.mks.energy.entity.biz.BtRoleReportType;
import com.mks.energy.service.IBtRoleReportTypeService;


/**
 * @author lifetime
 *
 */
@Service
public class BtRoleReportTypeService extends AbstractBaseService<BtRoleReportType> implements IBtRoleReportTypeService{
	@Autowired
	private IBtRoleReportTypeDao dao;

	@Override
	public BaseDao<BtRoleReportType> getDao() {
		return dao;
	}
	
	@Override
	public String add(BtRoleReportType t) {
		return super.add(t);
	}

}