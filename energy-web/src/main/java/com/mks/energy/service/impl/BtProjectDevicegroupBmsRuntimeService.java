package com.mks.energy.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mks.energy.core.ext.AbstractBaseService;
import com.mks.energy.core.ext.BaseDao;
import com.mks.energy.dao.IBtProjectDevicegroupBmsRuntimeDao;
import com.mks.energy.entity.biz.BtProjectDevicegroupBmsRuntime;
import com.mks.energy.service.IBtProjectDevicegroupBmsRuntimeService;


/**
 * @author lifetime
 *
 */
@Service
public class BtProjectDevicegroupBmsRuntimeService extends AbstractBaseService<BtProjectDevicegroupBmsRuntime> implements IBtProjectDevicegroupBmsRuntimeService{
	@Autowired
	private IBtProjectDevicegroupBmsRuntimeDao dao;

	@Override
	public BaseDao<BtProjectDevicegroupBmsRuntime> getDao() {
		return dao;
	}
	
	@Override
	public String add(BtProjectDevicegroupBmsRuntime t) {
		return super.add(t);
	}

}