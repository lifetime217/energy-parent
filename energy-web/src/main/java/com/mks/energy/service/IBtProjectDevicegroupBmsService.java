package com.mks.energy.service;

import com.mks.energy.core.ext.IBaseService;
import com.mks.energy.entity.biz.BtProjectDevicegroupBms;

/**
 * @author lifetime
 *
 */
public interface IBtProjectDevicegroupBmsService extends IBaseService<BtProjectDevicegroupBms>{
	
}
