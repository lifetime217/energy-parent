package com.mks.energy.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mks.energy.core.ext.AbstractBaseService;
import com.mks.energy.core.ext.BaseDao;
import com.mks.energy.dao.IBtPcsDao;
import com.mks.energy.entity.biz.BtPcs;
import com.mks.energy.service.IBtPcsService;

/**
 * @author lifetime
 *
 */
@Service
public class BtPcsService extends AbstractBaseService<BtPcs> implements IBtPcsService {
	@Autowired
	private IBtPcsDao dao;

	@Override
	public BaseDao<BtPcs> getDao() {
		return dao;
	}

	public String existsAndSave(BtPcs pcs) {
		BtPcs exisInfo = new BtPcs();
		exisInfo.setPcsFactory(pcs.getPcsFactory());
		exisInfo.setPcsModel(pcs.getPcsModel());
		exisInfo = dao.templateOne(exisInfo);
		if (exisInfo == null) {
			dao.insert(pcs);
			return pcs.getId();
		} else {
			return exisInfo.getId();
		}
	}

	@Override
	public String add(BtPcs t) {
		return super.add(t);
	}

}