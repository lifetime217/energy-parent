package com.mks.energy.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mks.energy.core.ext.AbstractBaseService;
import com.mks.energy.core.ext.BaseDao;
import com.mks.energy.dao.IBtProjectElectrictableDao;
import com.mks.energy.entity.biz.BtProjectElectrictable;
import com.mks.energy.service.IProjectElectrictableService;

/**
 * @author lifetime
 *
 */
@Service
public class ProjectElectrictableService extends AbstractBaseService<BtProjectElectrictable> implements IProjectElectrictableService{
	@Autowired
	private IBtProjectElectrictableDao btProjectElectrictableDao;

	@Override
	public BaseDao<BtProjectElectrictable> getDao() {
		return btProjectElectrictableDao;
	}
	
	@Override
	public String add(BtProjectElectrictable t) {
		return super.add(t);
	}

}
