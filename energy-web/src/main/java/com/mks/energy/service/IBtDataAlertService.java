package com.mks.energy.service;

import java.util.List;

import com.mks.energy.core.ext.IBaseService;
import com.mks.energy.entity.biz.BtDataAlert;

/**
 * @author lifetime
 *
 */
public interface IBtDataAlertService extends IBaseService<BtDataAlert>{

	/**
	 * @author ghl
	 * @Date 2019年1月31日下午3:15:59
	 * @TODO 
	 * @param projectId
	 * @return
	 */
	List<BtDataAlert> getBugByProjectId(String projectId);
	
}
