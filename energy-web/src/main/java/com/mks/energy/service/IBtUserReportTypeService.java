package com.mks.energy.service;

import com.mks.energy.core.ext.IBaseService;
import com.mks.energy.entity.biz.BtUserReportType;

/**
 * @author lifetime
 *
 */
public interface IBtUserReportTypeService extends IBaseService<BtUserReportType>{
	
}
