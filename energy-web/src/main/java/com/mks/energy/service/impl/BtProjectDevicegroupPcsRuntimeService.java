package com.mks.energy.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mks.energy.core.ext.AbstractBaseService;
import com.mks.energy.core.ext.BaseDao;
import com.mks.energy.dao.IBtProjectDevicegroupPcsRuntimeDao;
import com.mks.energy.entity.biz.BtProjectDevicegroupPcsRuntime;
import com.mks.energy.service.IBtProjectDevicegroupPcsRuntimeService;


/**
 * @author lifetime
 *
 */
@Service
public class BtProjectDevicegroupPcsRuntimeService extends AbstractBaseService<BtProjectDevicegroupPcsRuntime> implements IBtProjectDevicegroupPcsRuntimeService{
	@Autowired
	private IBtProjectDevicegroupPcsRuntimeDao dao;

	@Override
	public BaseDao<BtProjectDevicegroupPcsRuntime> getDao() {
		return dao;
	}
	
	@Override
	public String add(BtProjectDevicegroupPcsRuntime t) {
		return super.add(t);
	}

}