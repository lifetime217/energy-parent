package com.mks.energy.entity.biz;

import java.io.Serializable;

import org.beetl.sql.core.annotatoin.AssignID;

import com.mks.energy.entity.BaseInfo;


/**
 * 设备组_PCS_关系表
 */
public class BtProjectDevicegroupPcs extends BaseInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	public BtProjectDevicegroupPcs() {
	}

	/**
	 * 
	 */
	@AssignID("uuid")
	public String getId() {
		return getString("id");
	}

	public void setId(String id) {
		set("id", id);
	}

	/**
	 * 项目id
	 */
	public String getProjectId() {
		return getString("projectId");
	}

	public void setProjectId(String projectId) {
		set("projectId", projectId);
	}

	/**
	 * 设备组id
	 */
	public String getProjectGroupId() {
		return getString("projectGroupId");
	}

	public void setProjectGroupId(String projectGroupId) {
		set("projectGroupId", projectGroupId);
	}

	/**
	 * pcs的id
	 */
	public String getPcsId() {
		return getString("pcsId");
	}

	public void setPcsId(String pcsId) {
		set("pcsId", pcsId);
	}

	/**
	 * 该pcs的唯一标示
	 */
	public String getPcsSign() {
		return getString("pcsSign");
	}

	public void setPcsSign(String pcsSign) {
		set("pcsSign", pcsSign);
	}

}