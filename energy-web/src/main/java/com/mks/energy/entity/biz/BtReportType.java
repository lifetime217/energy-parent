package com.mks.energy.entity.biz;

import java.io.Serializable;
import java.util.Date;

import org.beetl.sql.core.annotatoin.AssignID;

import com.mks.energy.entity.BaseInfo;


/**
 * 报告分类表,便于维护不同的种类的报告
 */
public class BtReportType extends BaseInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	public BtReportType() {
	}

	/**
	 * 
	 */
	@AssignID("uuid")
	public String getId() {
		return getString("id");
	}

	public void setId(String id) {
		set("id", id);
	}

	/**
	 * 公司ID
	 */
	public String getCompanyId() {
		return getString("companyId");
	}

	public void setCompanyId(String companyId) {
		set("companyId", companyId);
	}

	/**
	 * 诊断报告URL
	 */
	public String getReportUrl() {
		return getString("reportUrl");
	}

	public void setReportUrl(String reportUrl) {
		set("reportUrl", reportUrl);
	}

	/**
	 * 报告名称
	 */
	public String getReportName() {
		return getString("reportName");
	}

	public void setReportName(String reportName) {
		set("reportName", reportName);
	}

	/**
	 * 报告分类,可定义10,11,12,13,14以此类推
	 */
	public Integer getReportType() {
		return getInteger("reportType");
	}

	public void setReportType(Integer reportType) {
		set("reportType", reportType);
	}

	/**
	 * 描述
	 */
	public String getDescription() {
		return getString("description");
	}

	public void setDescription(String description) {
		set("description", description);
	}

	/**
	 * 创建时间
	 */
	public Date getCreateTime() {
		return getDate("createTime");
	}

	public void setCreateTime(Date createTime) {
		set("createTime", createTime);
	}

	/**
	 * 更新时间
	 */
	public Date getUpdateTime() {
		return getDate("updateTime");
	}

	public void setUpdateTime(Date updateTime) {
		set("updateTime", updateTime);
	}

}