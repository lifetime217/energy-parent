package com.mks.energy.entity.biz;

import java.io.Serializable;
import java.math.BigDecimal;

import org.beetl.sql.core.annotatoin.AssignID;

import com.mks.energy.entity.BaseInfo;


/**
 * BMS信息表
 */
public class BtBms extends BaseInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	public BtBms() {
	}

	/**
	 * 
	 */
	@AssignID("uuid")
	public String getId() {
		return getString("id");
	}

	public void setId(String id) {
		set("id", id);
	}

	/**
	 * 电池厂家
	 */
	public String getBatteryFactory() {
		return getString("batteryFactory");
	}

	public void setBatteryFactory(String batteryFactory) {
		set("batteryFactory", batteryFactory);
	}

	/**
	 * 电芯厂家
	 */
	public String getCellFactory() {
		return getString("cellFactory");
	}

	public void setCellFactory(String cellFactory) {
		set("cellFactory", cellFactory);
	}

	/**
	 * 电池类型（10：磷酸铁锂，20：三元，30：钛酸锂，40：锰酸锂）
	 */
	public Integer getBatteryType() {
		return getInteger("batteryType");
	}

	public void setBatteryType(Integer batteryType) {
		set("batteryType", batteryType);
	}

	/**
	 * 电池形状（10：软包，20：方形，30：圆柱形）
	 */
	public Integer getBatteryShape() {
		return getInteger("batteryShape");
	}

	public void setBatteryShape(Integer batteryShape) {
		set("batteryShape", batteryShape);
	}

	/**
	 * 电池包标称电压(V)
	 */
	public BigDecimal getBatteryGroupV() {
		return getBigDecimal("batteryGroupV");
	}

	public void setBatteryGroupV(BigDecimal batteryGroupV) {
		set("batteryGroupV", batteryGroupV);
	}

	/**
	 * 电池包标称容量(Ah) 
	 */
	public BigDecimal getBatteryGroupA() {
		return getBigDecimal("batteryGroupA");
	}

	public void setBatteryGroupA(BigDecimal batteryGroupA) {
		set("batteryGroupA", batteryGroupA);
	}

	/**
	 * 电池标称电压(V) 
	 */
	public BigDecimal getBatteryV() {
		return getBigDecimal("batteryV");
	}

	public void setBatteryV(BigDecimal batteryV) {
		set("batteryV", batteryV);
	}

	/**
	 * 电池标称容量(Ah)
	 */
	public BigDecimal getBatteryA() {
		return getBigDecimal("batteryA");
	}

	public void setBatteryA(BigDecimal batteryA) {
		set("batteryA", batteryA);
	}

	/**
	 * 最大输出电流（A）
	 */
	public BigDecimal getMaxOutputA() {
		return getBigDecimal("maxOutputA");
	}

	public void setMaxOutputA(BigDecimal maxOutputA) {
		set("maxOutputA", maxOutputA);
	}

	/**
	 * 额定输出电流（A）
	 */
	public BigDecimal getStandardA() {
		return getBigDecimal("standardA");
	}

	public void setStandardA(BigDecimal standardA) {
		set("standardA", standardA);
	}

	/**
	 * 电池包串并联形式
	 */
	public String getBatteryGroupJointype() {
		return getString("batteryGroupJointype");
	}

	public void setBatteryGroupJointype(String batteryGroupJointype) {
		set("batteryGroupJointype", batteryGroupJointype);
	}

	/**
	 * 电池包模组数（个） 
	 */
	public Integer getBatteryGroupMzCount() {
		return getInteger("batteryGroupMzCount");
	}

	public void setBatteryGroupMzCount(Integer batteryGroupMzCount) {
		set("batteryGroupMzCount", batteryGroupMzCount);
	}

	/**
	 * BMS厂家 
	 */
	public String getBmsFactory() {
		return getString("bmsFactory");
	}

	public void setBmsFactory(String bmsFactory) {
		set("bmsFactory", bmsFactory);
	}

	/**
	 * BMS软件版本号
	 */
	public String getBmsSoftVersion() {
		return getString("bmsSoftVersion");
	}

	public void setBmsSoftVersion(String bmsSoftVersion) {
		set("bmsSoftVersion", bmsSoftVersion);
	}

	/**
	 * BMS硬件版本号
	 */
	public String getBmsHardVersion() {
		return getString("bmsHardVersion");
	}

	public void setBmsHardVersion(String bmsHardVersion) {
		set("bmsHardVersion", bmsHardVersion);
	}

	/**
	 * bms均衡类型（10：主动均衡，20：被动均衡，30：无均衡）
	 */
	public Integer getBmsBalancedType() {
		return getInteger("bmsBalancedType");
	}

	public void setBmsBalancedType(Integer bmsBalancedType) {
		set("bmsBalancedType", bmsBalancedType);
	}

	/**
	 * 最大均衡电流（mA）
	 */
	public BigDecimal getBmsMaxBalancedA() {
		return getBigDecimal("bmsMaxBalancedA");
	}

	public void setBmsMaxBalancedA(BigDecimal bmsMaxBalancedA) {
		set("bmsMaxBalancedA", bmsMaxBalancedA);
	}

}