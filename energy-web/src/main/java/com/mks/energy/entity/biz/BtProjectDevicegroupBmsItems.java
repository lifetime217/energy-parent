package com.mks.energy.entity.biz;

import java.io.Serializable;

import org.beetl.sql.core.annotatoin.AssignID;

import com.mks.energy.entity.BaseInfo;

/**
 * BMS 与 电池单体的关系表
 */
public class BtProjectDevicegroupBmsItems extends BaseInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	public BtProjectDevicegroupBmsItems() {
	}

	/**
	 * 
	 */
	@AssignID("uuid")
	public String getId() {
		return getString("id");
	}

	public void setId(String id) {
		set("id", id);
	}

	/**
	 * 电池单体唯一码
	 */
	public String getSign() {
		return getString("sign");
	}

	public void setSign(String sign) {
		set("sign", sign);
	}

	/**
	 * 电池单体名称
	 */
	public String getName() {
		return getString("name");
	}

	public void setName(String name) {
		set("name", name);
	}

	/**
	 * 电池模组名称
	 */
	public String getBgroupName() {
		return getString("bgroupName");
	}

	public void setBgroupName(String bgroupName) {
		set("bgroupName", bgroupName);
	}

	/**
	 * 设备组_BMS关联表的id
	 */
	public String getDevicegroupBmsId() {
		return getString("devicegroupBmsId");
	}

	public void setDevicegroupBmsId(String devicegroupBmsId) {
		set("devicegroupBmsId", devicegroupBmsId);
	}

}