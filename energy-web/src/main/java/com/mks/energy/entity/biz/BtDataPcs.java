package com.mks.energy.entity.biz;

import java.io.Serializable;
import java.math.BigDecimal;

import com.mks.energy.entity.BaseInfo;


/**
 * PCS 实时数据
 */
public class BtDataPcs extends BaseInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	public BtDataPcs() {
	}

	/**
	 * 
	 */
	public Integer getId() {
		return getInteger("id");
	}

	public void setId(Integer id) {
		set("id", id);
	}

	/**
	 * pcs唯一标识码
	 */
	public String getPcsSign() {
		return getString("pcsSign");
	}

	public void setPcsSign(String pcsSign) {
		set("pcsSign", pcsSign);
	}

	/**
	 * 
	 */
	public String getProjectId() {
		return getString("projectId");
	}

	public void setProjectId(String projectId) {
		set("projectId", projectId);
	}

	/**
	 * 交流电压A相有效值
	 */
	public BigDecimal getUinaRms() {
		return getBigDecimal("uinaRms");
	}

	public void setUinaRms(BigDecimal uinaRms) {
		set("uinaRms", uinaRms);
	}

	/**
	 * 交流电压B相有效值
	 */
	public BigDecimal getUinbRms() {
		return getBigDecimal("uinbRms");
	}

	public void setUinbRms(BigDecimal uinbRms) {
		set("uinbRms", uinbRms);
	}

	/**
	 * 交流电压C相有效值
	 */
	public BigDecimal getUincRms() {
		return getBigDecimal("uincRms");
	}

	public void setUincRms(BigDecimal uincRms) {
		set("uincRms", uincRms);
	}

	/**
	 * 交流电流A相有效值
	 */
	public BigDecimal getIinaRms() {
		return getBigDecimal("iinaRms");
	}

	public void setIinaRms(BigDecimal iinaRms) {
		set("iinaRms", iinaRms);
	}

	/**
	 * 交流电流B相有效值
	 */
	public BigDecimal getIinbRms() {
		return getBigDecimal("iinbRms");
	}

	public void setIinbRms(BigDecimal iinbRms) {
		set("iinbRms", iinbRms);
	}

	/**
	 * 交流电流C相有效值
	 */
	public BigDecimal getIincRms() {
		return getBigDecimal("iincRms");
	}

	public void setIincRms(BigDecimal iincRms) {
		set("iincRms", iincRms);
	}

	/**
	 * 电网频率
	 */
	public BigDecimal getW() {
		return getBigDecimal("w");
	}

	public void setW(BigDecimal w) {
		set("w", w);
	}

	/**
	 * 有功功率
	 */
	public BigDecimal getP() {
		return getBigDecimal("p");
	}

	public void setP(BigDecimal p) {
		set("p", p);
	}

	/**
	 * 无功功率
	 */
	public BigDecimal getQ() {
		return getBigDecimal("q");
	}

	public void setQ(BigDecimal q) {
		set("q", q);
	}

	/**
	 * DC直流电压值
	 */
	public BigDecimal getUbtraEver1() {
		return getBigDecimal("ubtraEver1");
	}

	public void setUbtraEver1(BigDecimal ubtraEver1) {
		set("ubtraEver1", ubtraEver1);
	}

	/**
	 * DC直流电流值
	 */
	public BigDecimal getIbtraEver1() {
		return getBigDecimal("ibtraEver1");
	}

	public void setIbtraEver1(BigDecimal ibtraEver1) {
		set("ibtraEver1", ibtraEver1);
	}

	/**
	 * DC直流侧功率
	 */
	public BigDecimal getPbtra1() {
		return getBigDecimal("pbtra1");
	}

	public void setPbtra1(BigDecimal pbtra1) {
		set("pbtra1", pbtra1);
	}

	/**
	 * PV直流电压值
	 */
	public BigDecimal getUbtraEver2() {
		return getBigDecimal("ubtraEver2");
	}

	public void setUbtraEver2(BigDecimal ubtraEver2) {
		set("ubtraEver2", ubtraEver2);
	}

	/**
	 * PV直流电流值
	 */
	public BigDecimal getIbtraEver2() {
		return getBigDecimal("ibtraEver2");
	}

	public void setIbtraEver2(BigDecimal ibtraEver2) {
		set("ibtraEver2", ibtraEver2);
	}

	/**
	 * PV直流侧功率
	 */
	public BigDecimal getPbtra2() {
		return getBigDecimal("pbtra2");
	}

	public void setPbtra2(BigDecimal pbtra2) {
		set("pbtra2", pbtra2);
	}

	/**
	 * A相IGBT温度
	 */
	public BigDecimal getAIgbtTemp() {
		return getBigDecimal("aIgbtTemp");
	}

	public void setAIgbtTemp(BigDecimal aIgbtTemp) {
		set("aIgbtTemp", aIgbtTemp);
	}

	/**
	 * B相IGBT温度
	 */
	public BigDecimal getBIgbtTemp() {
		return getBigDecimal("bIgbtTemp");
	}

	public void setBIgbtTemp(BigDecimal bIgbtTemp) {
		set("bIgbtTemp", bIgbtTemp);
	}

	/**
	 * C相IGBT温度
	 */
	public BigDecimal getCIgbtTemp() {
		return getBigDecimal("cIgbtTemp");
	}

	public void setCIgbtTemp(BigDecimal cIgbtTemp) {
		set("cIgbtTemp", cIgbtTemp);
	}

	/**
	 * 
	 */
	public Integer getAddTime() {
		return getInteger("addTime");
	}

	public void setAddTime(Integer addTime) {
		set("addTime", addTime);
	}

}