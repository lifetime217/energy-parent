package com.mks.energy.entity.biz;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import org.beetl.sql.core.annotatoin.AssignID;

import com.mks.energy.entity.BaseInfo;

/**
 * 项目表
 */
public class BtProject extends BaseInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	public BtProject() {
	}

	/**
	 * 
	 */
	@AssignID("uuid")
	public String getId() {
		return getString("id");
	}

	public void setId(String id) {
		set("id", id);
	}

	/**
	 * 项目名称
	 */
	public String getName() {
		return getString("name");
	}

	public void setName(String name) {
		set("name", name);
	}

	/**
	 * 项目地址
	 */
	public String getAddress() {
		return getString("address");
	}

	public void setAddress(String address) {
		set("address", address);
	}

	/**
	 * 纬度
	 */
	public BigDecimal getLat() {
		return getBigDecimal("lat");
	}

	public void setLat(BigDecimal lat) {
		set("lat", lat);
	}

	/**
	 * 经度
	 */
	public BigDecimal getLgt() {
		return getBigDecimal("lgt");
	}

	public void setLgt(BigDecimal lgt) {
		set("lgt", lgt);
	}

	/**
	 * 我方负责人
	 */
	public String getMyPerson() {
		return getString("myPerson");
	}

	public void setMyPerson(String myPerson) {
		set("myPerson", myPerson);
	}

	/**
	 * 我方负责人电话
	 */
	public String getMyPersonPhone() {
		return getString("myPersonPhone");
	}

	public void setMyPersonPhone(String myPersonPhone) {
		set("myPersonPhone", myPersonPhone);
	}

	/**
	 * 客户方负责人
	 */
	public String getCustomerPerson() {
		return getString("customerPerson");
	}

	public void setCustomerPerson(String customerPerson) {
		set("customerPerson", customerPerson);
	}

	/**
	 * 客户方负责人电话
	 */
	public String getCustomerPersonPhone() {
		return getString("customerPersonPhone");
	}

	public void setCustomerPersonPhone(String customerPersonPhone) {
		set("customerPersonPhone", customerPersonPhone);
	}

	/**
	 * 首次接入时间
	 */
	public Date getJoinTime() {
		return getDate("joinTime");
	}

	public void setJoinTime(Date joinTime) {
		set("joinTime", joinTime);
	}

	/**
	 * 0：离线   ，   1：在线
	 */
	public Integer getProjectStatus() {
		return getInteger("projectStatus");
	}

	public void setProjectStatus(Integer projectStatus) {
		set("projectStatus", projectStatus);
	}

	/**
	 * 项目唯一码
	 */
	public String getProjectSign() {
		return getString("projectSign");
	}

	public void setProjectSign(String projectSign) {
		set("projectSign", projectSign);
	}

	/**
	 * ip白名单
	 */
	public String getIpList() {
		return getString("ipList");
	}

	public void setIpList(String ipList) {
		set("ipList", ipList);
	}

	/**
	 * 上传周期（单位为秒）
	 */
	public Integer getUploadCycle() {
		return getInteger("uploadCycle");
	}

	public void setUploadCycle(Integer uploadCycle) {
		set("uploadCycle", uploadCycle);
	}

	/**
	 * 创建日期
	 */
	public Integer getAddTime() {
		return getInteger("addTime");
	}

	public void setAddTime(Integer addTime) {
		set("addTime", addTime);
	}

	/**
	 * 创建人
	 */
	public String getAddUser() {
		return getString("addUser");
	}

	public void setAddUser(String addUser) {
		set("addUser", addUser);
	}

	/**
	 * 安装位置
	 */
	public String getInstallAddress() {
		return getString("installAddress");
	}

	public void setInstallAddress(String installAddress) {
		set("installAddress", installAddress);
	}

	/**
	 * BMS厂家
	 */
	public String getSysBmsCompany() {
		return getString("sysBmsCompany");
	}

	public void setSysBmsCompany(String sysBmsCompany) {
		set("sysBmsCompany", sysBmsCompany);
	}

	/**
	 * 电池供应商
	 */
	public String getSysBatteryCompany() {
		return getString("sysBatteryCompany");
	}

	public void setSysBatteryCompany(String sysBatteryCompany) {
		set("sysBatteryCompany", sysBatteryCompany);
	}

	/**
	 * 变流器供应商
	 */
	public String getSysPcsCompany() {
		return getString("sysPcsCompany");
	}

	public void setSysPcsCompany(String sysPcsCompany) {
		set("sysPcsCompany", sysPcsCompany);
	}

	/**
	 * 项目图片1
	 */
	public String getProjectImg1() {
		return getString("projectImg1");
	}

	public void setProjectImg1(String projectImg1) {
		set("projectImg1", projectImg1);
	}

	/**
	 * 项目图片2
	 */
	public String getProjectImg2() {
		return getString("projectImg2");
	}

	public void setProjectImg2(String projectImg2) {
		set("projectImg2", projectImg2);
	}

	/**
	 * 项目图片3
	 */
	public String getProjectImg3() {
		return getString("projectImg3");
	}

	public void setProjectImg3(String projectImg3) {
		set("projectImg3", projectImg3);
	}

}