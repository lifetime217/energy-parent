package com.mks.energy.entity.biz;

import java.io.Serializable;
import java.util.Date;

import com.mks.energy.entity.BaseInfo;


/**
 * 诊断分析
 */
public class BtDiagnosticAnalyze extends BaseInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	public BtDiagnosticAnalyze() {
	}

	/**
	 * 
	 */
	public String getId() {
		return getString("id");
	}

	public void setId(String id) {
		set("id", id);
	}

	/**
	 * 
	 */
	public String getProjectId() {
		return getString("projectId");
	}

	public void setProjectId(String projectId) {
		set("projectId", projectId);
	}

	/**
	 * 诊断时间
	 */
	public Date getDiagnosticTime() {
		return getDate("diagnosticTime");
	}

	public void setDiagnosticTime(Date diagnosticTime) {
		set("diagnosticTime", diagnosticTime);
	}

	/**
	 * 诊断算法
	 */
	public String getDiagnosticAlg() {
		return getString("diagnosticAlg");
	}

	public void setDiagnosticAlg(String diagnosticAlg) {
		set("diagnosticAlg", diagnosticAlg);
	}

	/**
	 * 诊断报告
	 */
	public String getDiagnosticReport() {
		return getString("diagnosticReport");
	}

	public void setDiagnosticReport(String diagnosticReport) {
		set("diagnosticReport", diagnosticReport);
	}

}