package com.mks.energy.entity.biz;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import org.beetl.sql.core.annotatoin.AssignID;

import com.mks.energy.entity.BaseInfo;


/**
 * 设备组_BMS_关联表
 */
public class BtProjectDevicegroupBms extends BaseInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	public BtProjectDevicegroupBms() {
	}

	/**
	 * 
	 */
	@AssignID("uuid")
	public String getId() {
		return getString("id");
	}

	public void setId(String id) {
		set("id", id);
	}

	/**
	 * 所属的设备组id
	 */
	public String getDevicegroupId() {
		return getString("devicegroupId");
	}

	public void setDevicegroupId(String devicegroupId) {
		set("devicegroupId", devicegroupId);
	}

	/**
	 * 
	 */
	public String getBmsId() {
		return getString("bmsId");
	}

	public void setBmsId(String bmsId) {
		set("bmsId", bmsId);
	}

	/**
	 * 电池包的唯一标示符
	 */
	public String getBmsSign() {
		return getString("bmsSign");
	}

	public void setBmsSign(String bmsSign) {
		set("bmsSign", bmsSign);
	}

	/**
	 * 剩余电量
	 */
	public BigDecimal getDumpEnergy() {
		return getBigDecimal("dumpEnergy");
	}

	public void setDumpEnergy(BigDecimal dumpEnergy) {
		set("dumpEnergy", dumpEnergy);
	}

	/**
	 * 放出电量
	 */
	public BigDecimal getOutEnergy() {
		return getBigDecimal("outEnergy");
	}

	public void setOutEnergy(BigDecimal outEnergy) {
		set("outEnergy", outEnergy);
	}

	/**
	 * 总电压
	 */
	public BigDecimal getFullv() {
		return getBigDecimal("fullv");
	}

	public void setFullv(BigDecimal fullv) {
		set("fullv", fullv);
	}

	/**
	 * 电流
	 */
	public BigDecimal getFulli() {
		return getBigDecimal("fulli");
	}

	public void setFulli(BigDecimal fulli) {
		set("fulli", fulli);
	}

	/**
	 * SOC
	 */
	public Integer getSoc() {
		return getInteger("soc");
	}

	public void setSoc(Integer soc) {
		set("soc", soc);
	}

	/**
	 * 循环次数
	 */
	public Integer getLoopCount() {
		return getInteger("loopCount");
	}

	public void setLoopCount(Integer loopCount) {
		set("loopCount", loopCount);
	}

	/**
	 * 最高单体电压
	 */
	public BigDecimal getMaxItemv() {
		return getBigDecimal("maxItemv");
	}

	public void setMaxItemv(BigDecimal maxItemv) {
		set("maxItemv", maxItemv);
	}

	/**
	 * 最低单体电压
	 */
	public BigDecimal getMinItemv() {
		return getBigDecimal("minItemv");
	}

	public void setMinItemv(BigDecimal minItemv) {
		set("minItemv", minItemv);
	}

	/**
	 * 电压极差
	 */
	public BigDecimal getDiffv() {
		return getBigDecimal("diffv");
	}

	public void setDiffv(BigDecimal diffv) {
		set("diffv", diffv);
	}

	/**
	 * 最高温度
	 */
	public BigDecimal getMaxItemt() {
		return getBigDecimal("maxItemt");
	}

	public void setMaxItemt(BigDecimal maxItemt) {
		set("maxItemt", maxItemt);
	}

	/**
	 * 最低温度
	 */
	public BigDecimal getMinItemt() {
		return getBigDecimal("minItemt");
	}

	public void setMinItemt(BigDecimal minItemt) {
		set("minItemt", minItemt);
	}

	/**
	 * 温度极差
	 */
	public BigDecimal getDifft() {
		return getBigDecimal("difft");
	}

	public void setDifft(BigDecimal difft) {
		set("difft", difft);
	}

	/**
	 * 平均温度
	 */
	public BigDecimal getAvgt() {
		return getBigDecimal("avgt");
	}

	public void setAvgt(BigDecimal avgt) {
		set("avgt", avgt);
	}

	/**
	 * 
	 */
	public Integer getAddTime() {
		return getInteger("addTime");
	}

	public void setAddTime(Integer addTime) {
		set("addTime", addTime);
	}

	/**
	 * 
	 */
	public Date getLastchange() {
		return getDate("lastchange");
	}

	public void setLastchange(Date lastchange) {
		set("lastchange", lastchange);
	}

}