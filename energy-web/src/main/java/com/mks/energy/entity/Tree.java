package com.mks.energy.entity;

public class Tree extends BaseInfo{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Tree() {
	}
	
	public String getName() {
		return getString("name");
	}

	public void setName(String name) {
		set("name",name);
	}
	
	public String getId() {
		return getString("id");
	}

	public void setId(String id) {
		set("id",id);
	}
	
	public String getpId() {
		return getString("pId");
	}

	public void setpId(String pId) {
		set("pId",pId);
	}
	public String getIsParent() {
		return getString("isParent");
	}

	public void setIsParent(String isParent) {
		set("isParent",isParent);
	}
}
