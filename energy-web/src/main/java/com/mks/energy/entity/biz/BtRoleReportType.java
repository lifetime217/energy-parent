package com.mks.energy.entity.biz;

import java.io.Serializable;
import java.util.Date;

import org.beetl.sql.core.annotatoin.AssignID;

import com.mks.energy.entity.BaseInfo;


/**
 * 角色报告类型关系表,角色所对应的查看报告的权限
 */
public class BtRoleReportType extends BaseInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	public BtRoleReportType() {
	}

	/**
	 * 
	 */
	@AssignID("uuid")
	public String getId() {
		return getString("id");
	}

	public void setId(String id) {
		set("id", id);
	}

	/**
	 * 角色ID
	 */
	public String getRoleId() {
		return getString("roleId");
	}

	public void setRoleId(String roleId) {
		set("roleId", roleId);
	}

	/**
	 * 报告类型ID
	 */
	public String getReportTypeId() {
		return getString("reportTypeId");
	}

	public void setReportTypeId(String reportTypeId) {
		set("reportTypeId", reportTypeId);
	}

	/**
	 * 
	 */
	public Date getCreateTime() {
		return getDate("createTime");
	}

	public void setCreateTime(Date createTime) {
		set("createTime", createTime);
	}

	/**
	 * 
	 */
	public Date getUpdateTime() {
		return getDate("updateTime");
	}

	public void setUpdateTime(Date updateTime) {
		set("updateTime", updateTime);
	}

}