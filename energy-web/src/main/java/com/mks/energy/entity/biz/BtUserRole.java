package com.mks.energy.entity.biz;

import java.io.Serializable;
import java.util.Date;

import org.beetl.sql.core.annotatoin.AssignID;

import com.mks.energy.entity.BaseInfo;


/**
 * 用户角色表
 */
public class BtUserRole extends BaseInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	public BtUserRole() {
	}

	/**
	 * 
	 */
	@AssignID("uuid")
	public String getId() {
		return getString("id");
	}

	public void setId(String id) {
		set("id", id);
	}

	/**
	 * 用户ID
	 */
	public String getUserId() {
		return getString("userId");
	}

	public void setUserId(String userId) {
		set("userId", userId);
	}

	/**
	 * 角色ID
	 */
	public String getRoleId() {
		return getString("roleId");
	}

	public void setRoleId(String roleId) {
		set("roleId", roleId);
	}

	/**
	 * 创建时间
	 */
	public Date getCreateTime() {
		return getDate("createTime");
	}

	public void setCreateTime(Date createTime) {
		set("createTime", createTime);
	}

	/**
	 * 修改时间
	 */
	public Date getUpdateTime() {
		return getDate("updateTime");
	}

	public void setUpdateTime(Date updateTime) {
		set("updateTime", updateTime);
	}

}