package com.mks.energy.entity.biz;

import java.io.Serializable;

import com.mks.energy.entity.BaseInfo;


/**
 * 菜单表
 */
public class BtMenu extends BaseInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	public BtMenu() {
	}

	/**
	 * 
	 */
	public String getId() {
		return getString("id");
	}

	public void setId(String id) {
		set("id", id);
	}

	/**
	 * 菜单名称
	 */
	public String getMenuName() {
		return getString("menuName");
	}

	public void setMenuName(String menuName) {
		set("menuName", menuName);
	}

	/**
	 * 菜单编码（唯一，且影响页面显示的顺序）
	 */
	public String getMenuNumber() {
		return getString("menuNumber");
	}

	public void setMenuNumber(String menuNumber) {
		set("menuNumber", menuNumber);
	}

	/**
	 * 菜单的url。只需配置RequestMapping的映射路径
	 */
	public String getMenuUrl() {
		return getString("menuUrl");
	}

	public void setMenuUrl(String menuUrl) {
		set("menuUrl", menuUrl);
	}

	/**
	 * 菜单的图标icon的class
	 */
	public String getMenuIcon() {
		return getString("menuIcon");
	}

	public void setMenuIcon(String menuIcon) {
		set("menuIcon", menuIcon);
	}

	/**
	 * 父菜单的id
	 */
	public String getParentId() {
		return getString("parentId");
	}

	public void setParentId(String parentId) {
		set("parentId", parentId);
	}

	/**
	 * 菜单的状态（10：启用，99：禁用）
	 */
	public Integer getMenuStatus() {
		return getInteger("menuStatus");
	}

	public void setMenuStatus(Integer menuStatus) {
		set("menuStatus", menuStatus);
	}

}