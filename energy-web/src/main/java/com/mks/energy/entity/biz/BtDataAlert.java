package com.mks.energy.entity.biz;

import java.io.Serializable;
import java.util.Date;

import org.beetl.sql.core.annotatoin.AssignID;

import com.mks.energy.entity.BaseInfo;


/**
 * 预警数据表
 */
public class BtDataAlert extends BaseInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	public BtDataAlert() {
	}

	/**
	 * 
	 */
	@AssignID
	public String getId() {
		return getString("id");
	}

	public void setId(String id) {
		set("id", id);
	}

	/**
	 * 报警设备（10：PCS，20：BMS，30：电池单体）
	 */
	public Integer getAlertDevice() {
		return getInteger("alertDevice");
	}

	public void setAlertDevice(Integer alertDevice) {
		set("alertDevice", alertDevice);
	}

	/**
	 * 发生报警的时间
	 */
	public Date getAlertTime() {
		return getDate("alertTime");
	}

	public void setAlertTime(Date alertTime) {
		set("alertTime", alertTime);
	}

	/**
	 * 项目id
	 */
	public String getProjectId() {
		return getString("projectId");
	}

	public void setProjectId(String projectId) {
		set("projectId", projectId);
	}

	/**
	 * 报警设备的唯一标识符
	 */
	public String getSign() {
		return getString("sign");
	}

	public void setSign(String sign) {
		set("sign", sign);
	}

	/**
	 * 报警内容
	 */
	public String getAlertContent() {
		return getString("alertContent");
	}

	public void setAlertContent(String alertContent) {
		set("alertContent", alertContent);
	}

	/**
	 * 报警等级
	 */
	public Integer getAlertLevel() {
		return getInteger("alertLevel");
	}

	public void setAlertLevel(Integer alertLevel) {
		set("alertLevel", alertLevel);
	}

	/**
	 * 报警来源类型（10：BMS，20：PCS）
	 */
	public Integer getAlertSourceType() {
		return getInteger("alertSourceType");
	}

	public void setAlertSourceType(Integer alertSourceType) {
		set("alertSourceType", alertSourceType);
	}

	/**
	 * 事件追溯记录的id
	 */
	public String getAlertSoeId() {
		return getString("alertSoeId");
	}

	public void setAlertSoeId(String alertSoeId) {
		set("alertSoeId", alertSoeId);
	}

	/**
	 * 报警的处理建议
	 */
	public String getAlertHandleIdea() {
		return getString("alertHandleIdea");
	}

	public void setAlertHandleIdea(String alertHandleIdea) {
		set("alertHandleIdea", alertHandleIdea);
	}

	/**
	 * 报警的处理详细信息
	 */
	public String getAlertHandleDetail() {
		return getString("alertHandleDetail");
	}

	public void setAlertHandleDetail(String alertHandleDetail) {
		set("alertHandleDetail", alertHandleDetail);
	}

	/**
	 * 处理次数
	 */
	public Integer getHandleCount() {
		return getInteger("handleCount");
	}

	public void setHandleCount(Integer handleCount) {
		set("handleCount", handleCount);
	}
}