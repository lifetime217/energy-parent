package com.mks.energy.entity.biz;

import java.io.Serializable;

import org.beetl.sql.core.annotatoin.AssignID;

import com.mks.energy.entity.BaseInfo;


/**
 * 项目的设备组
 */
public class BtProjectDevicegroup extends BaseInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	public BtProjectDevicegroup() {
	}

	/**
	 * 
	 */
	@AssignID("uuid")
	public String getId() {
		return getString("id");
	}

	public void setId(String id) {
		set("id", id);
	}

	/**
	 * 组名称
	 */
	public String getName() {
		return getString("name");
	}

	public void setName(String name) {
		set("name", name);
	}

	/**
	 * 组编码
	 */
	public String getNumber() {
		return getString("number");
	}

	public void setNumber(String number) {
		set("number", number);
	}

	/**
	 * 属于的项目
	 */
	public String getProjectId() {
		return getString("projectId");
	}

	public void setProjectId(String projectId) {
		set("projectId", projectId);
	}

	/**
	 * 组描述
	 */
	public String getDesc() {
		return getString("desc");
	}

	public void setDesc(String desc) {
		set("desc", desc);
	}

	/**
	 * 排序用的编号
	 */
	public Integer getSortNum() {
		return getInteger("sortNum");
	}

	public void setSortNum(Integer sortNum) {
		set("sortNum", sortNum);
	}

	/**
	 * 设备组里pcs的数量
	 */
	public Integer getPcsNum() {
		return getInteger("pcsNum");
	}

	public void setPcsNum(Integer pcsNum) {
		set("pcsNum", pcsNum);
	}

	/**
	 * 设备组里bms的数量
	 */
	public Integer getBmsNum() {
		return getInteger("bmsNum");
	}

	public void setBmsNum(Integer bmsNum) {
		set("bmsNum", bmsNum);
	}

	/**
	 * 电池单体的数量
	 */
	public Integer getBatteryNum() {
		return getInteger("batteryNum");
	}

	public void setBatteryNum(Integer batteryNum) {
		set("batteryNum", batteryNum);
	}

	/**
	 * 
	 */
	public Integer getAddTime() {
		return getInteger("addTime");
	}

	public void setAddTime(Integer addTime) {
		set("addTime", addTime);
	}

	/**
	 * 是否删除（0：未删除，1：已删除）
	 */
	public Integer getIsDelete() {
		return getInteger("isDelete");
	}

	public void setIsDelete(Integer isDelete) {
		set("isDelete", isDelete);
	}

}