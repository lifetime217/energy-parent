package com.mks.energy.entity.biz;

import java.io.Serializable;
import java.util.Date;

import org.beetl.sql.core.annotatoin.AssignID;

import com.mks.energy.entity.BaseInfo;


/**
 * 角色对应的菜单表
 */
public class BtRoleMenu extends BaseInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	public BtRoleMenu() {
	}

	/**
	 * 
	 */
	@AssignID("uuid")
	public String getId() {
		return getString("id");
	}

	public void setId(String id) {
		set("id", id);
	}

	/**
	 * 角色ID
	 */
	public String getRoleId() {
		return getString("roleId");
	}

	public void setRoleId(String roleId) {
		set("roleId", roleId);
	}

	/**
	 * 菜单ID
	 */
	public String getMenuId() {
		return getString("menuId");
	}

	public void setMenuId(String menuId) {
		set("menuId", menuId);
	}

	/**
	 * 创建时间
	 */
	public Date getCreateTime() {
		return getDate("createTime");
	}

	public void setCreateTime(Date createTime) {
		set("createTime", createTime);
	}

	/**
	 * 修改时间
	 */
	public Date getUpdateTime() {
		return getDate("updateTime");
	}

	public void setUpdateTime(Date updateTime) {
		set("updateTime", updateTime);
	}

}