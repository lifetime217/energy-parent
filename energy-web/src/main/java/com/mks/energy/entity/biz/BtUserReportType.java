package com.mks.energy.entity.biz;

import java.io.Serializable;
import java.util.Date;

import org.beetl.sql.core.annotatoin.AssignID;

import com.mks.energy.entity.BaseInfo;


/**
 * 用户报告类型关系表,用户查看报告的权限表
 */
public class BtUserReportType extends BaseInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	public BtUserReportType() {
	}

	/**
	 * 
	 */
	@AssignID("uuid")
	public String getId() {
		return getString("id");
	}

	public void setId(String id) {
		set("id", id);
	}

	/**
	 * 用户ID
	 */
	public String getUserId() {
		return getString("userId");
	}

	public void setUserId(String userId) {
		set("userId", userId);
	}

	/**
	 * 报告类型ID
	 */
	public String getReportTypeId() {
		return getString("reportTypeId");
	}

	public void setReportTypeId(String reportTypeId) {
		set("reportTypeId", reportTypeId);
	}

	/**
	 * 
	 */
	public Date getCreateTime() {
		return getDate("createTime");
	}

	public void setCreateTime(Date createTime) {
		set("createTime", createTime);
	}

	/**
	 * 
	 */
	public Date getUpdateTime() {
		return getDate("updateTime");
	}

	public void setUpdateTime(Date updateTime) {
		set("updateTime", updateTime);
	}

}