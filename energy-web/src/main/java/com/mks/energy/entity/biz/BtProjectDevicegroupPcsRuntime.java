package com.mks.energy.entity.biz;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import org.beetl.sql.core.annotatoin.AssignID;

import com.mks.energy.entity.BaseInfo;


/**
 * 设备组里PCS的实时数据
 */
public class BtProjectDevicegroupPcsRuntime extends BaseInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	public BtProjectDevicegroupPcsRuntime() {
	}

	/**
	 * 
	 */
	@AssignID("uuid")
	public String getId() {
		return getString("id");
	}

	public void setId(String id) {
		set("id", id);
	}

	/**
	 * pcs在设备组的id
	 */
	public String getDevicegoupPcsId() {
		return getString("devicegoupPcsId");
	}

	public void setDevicegoupPcsId(String devicegoupPcsId) {
		set("devicegoupPcsId", devicegoupPcsId);
	}

	/**
	 * 天数的纯数字表示：20180808
	 */
	public Integer getDayVal() {
		return getInteger("dayVal");
	}

	public void setDayVal(Integer dayVal) {
		set("dayVal", dayVal);
	}

	/**
	 * 小时数
	 */
	public Integer getHourVal() {
		return getInteger("hourVal");
	}

	public void setHourVal(Integer hourVal) {
		set("hourVal", hourVal);
	}

	/**
	 * 当天充电量
	 */
	public BigDecimal getDayIn() {
		return getBigDecimal("dayIn");
	}

	public void setDayIn(BigDecimal dayIn) {
		set("dayIn", dayIn);
	}

	/**
	 * 当天放电量
	 */
	public BigDecimal getDayOut() {
		return getBigDecimal("dayOut");
	}

	public void setDayOut(BigDecimal dayOut) {
		set("dayOut", dayOut);
	}

	/**
	 * 累计充电量
	 */
	public BigDecimal getTotalIn() {
		return getBigDecimal("totalIn");
	}

	public void setTotalIn(BigDecimal totalIn) {
		set("totalIn", totalIn);
	}

	/**
	 * 累计放电量
	 */
	public BigDecimal getTotalOut() {
		return getBigDecimal("totalOut");
	}

	public void setTotalOut(BigDecimal totalOut) {
		set("totalOut", totalOut);
	}

	/**
	 * 有效功率
	 */
	public BigDecimal getValidVi() {
		return getBigDecimal("validVi");
	}

	public void setValidVi(BigDecimal validVi) {
		set("validVi", validVi);
	}

	/**
	 * 无效功率
	 */
	public BigDecimal getInvalidVi() {
		return getBigDecimal("invalidVi");
	}

	public void setInvalidVi(BigDecimal invalidVi) {
		set("invalidVi", invalidVi);
	}

	/**
	 * 功率因数
	 */
	public BigDecimal getViSign() {
		return getBigDecimal("viSign");
	}

	public void setViSign(BigDecimal viSign) {
		set("viSign", viSign);
	}

	/**
	 * 直流功率
	 */
	public BigDecimal getLineVi() {
		return getBigDecimal("lineVi");
	}

	public void setLineVi(BigDecimal lineVi) {
		set("lineVi", lineVi);
	}

	/**
	 * 功率模块的温度
	 */
	public Integer getViModet() {
		return getInteger("viModet");
	}

	public void setViModet(Integer viModet) {
		set("viModet", viModet);
	}

	/**
	 * 
	 */
	public Integer getAddTime() {
		return getInteger("addTime");
	}

	public void setAddTime(Integer addTime) {
		set("addTime", addTime);
	}

	/**
	 * 
	 */
	public Date getLastupdatetime() {
		return getDate("lastupdatetime");
	}

	public void setLastupdatetime(Date lastupdatetime) {
		set("lastupdatetime", lastupdatetime);
	}

	/**
	 * AB线电压
	 */
	public BigDecimal getAbv() {
		return getBigDecimal("abv");
	}

	public void setAbv(BigDecimal abv) {
		set("abv", abv);
	}

	/**
	 * BC线电压
	 */
	public BigDecimal getBcv() {
		return getBigDecimal("bcv");
	}

	public void setBcv(BigDecimal bcv) {
		set("bcv", bcv);
	}

	/**
	 * CA线电压
	 */
	public BigDecimal getCav() {
		return getBigDecimal("cav");
	}

	public void setCav(BigDecimal cav) {
		set("cav", cav);
	}

	/**
	 * A相电流
	 */
	public BigDecimal getAi() {
		return getBigDecimal("ai");
	}

	public void setAi(BigDecimal ai) {
		set("ai", ai);
	}

	/**
	 * B相电流
	 */
	public BigDecimal getBi() {
		return getBigDecimal("bi");
	}

	public void setBi(BigDecimal bi) {
		set("bi", bi);
	}

	/**
	 * C相电流
	 */
	public BigDecimal getCi() {
		return getBigDecimal("ci");
	}

	public void setCi(BigDecimal ci) {
		set("ci", ci);
	}

	/**
	 * 直流母线电流
	 */
	public BigDecimal getLinei() {
		return getBigDecimal("linei");
	}

	public void setLinei(BigDecimal linei) {
		set("linei", linei);
	}

	/**
	 * 直流母线电压
	 */
	public BigDecimal getLinev() {
		return getBigDecimal("linev");
	}

	public void setLinev(BigDecimal linev) {
		set("linev", linev);
	}

}