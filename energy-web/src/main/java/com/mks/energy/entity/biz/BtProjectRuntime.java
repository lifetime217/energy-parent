package com.mks.energy.entity.biz;

import java.io.Serializable;
import java.math.BigDecimal;

import org.beetl.sql.core.annotatoin.AssignID;

import com.mks.energy.entity.BaseInfo;

/**
 * 项目实时数据状态表
 */
public class BtProjectRuntime extends BaseInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	public BtProjectRuntime() {
	}

	/**
	 * 
	 */
	@AssignID("uuid")
	public String getId() {
		return getString("id");
	}

	public void setId(String id) {
		set("id", id);
	}

	/**
	 * 项目id
	 */
	public String getProjectId() {
		return getString("projectId");
	}

	public void setProjectId(String projectId) {
		set("projectId", projectId);
	}

	/**
	 * 日期（yyyyMMdd）
	 */
	public Integer getDateVal() {
		return getInteger("dateVal");
	}

	public void setDateVal(Integer dateVal) {
		set("dateVal", dateVal);
	}

	/**
	 * 储能功率
	 */
	public BigDecimal getCapKw() {
		return getBigDecimal("capKw");
	}

	public void setCapKw(BigDecimal capKw) {
		set("capKw", capKw);
	}

	/**
	 * 储能输出电压
	 */
	public BigDecimal getCapV() {
		return getBigDecimal("capV");
	}

	public void setCapV(BigDecimal capV) {
		set("capV", capV);
	}

	/**
	 * 储能电流
	 */
	public BigDecimal getCapA() {
		return getBigDecimal("capA");
	}

	public void setCapA(BigDecimal capA) {
		set("capA", capA);
	}

	/**
	 * 今日充电量（KWh）
	 */
	public BigDecimal getFill() {
		return getBigDecimal("fill");
	}

	public void setFill(BigDecimal fill) {
		set("fill", fill);
	}

	/**
	 * 累计充电量（KWh）
	 */
	public BigDecimal getFillTotal() {
		return getBigDecimal("fillTotal");
	}

	public void setFillTotal(BigDecimal fillTotal) {
		set("fillTotal", fillTotal);
	}

	/**
	 * 今日放电量（KWh）
	 */
	public BigDecimal getOut() {
		return getBigDecimal("out");
	}

	public void setOut(BigDecimal out) {
		set("out", out);
	}

	/**
	 * 累计放电量（KWh）
	 */
	public BigDecimal getOutTotal() {
		return getBigDecimal("outTotal");
	}

	public void setOutTotal(BigDecimal outTotal) {
		set("outTotal", outTotal);
	}

	/**
	 * 安全运行天数
	 */
	public Integer getSafeDay() {
		return getInteger("safeDay");
	}

	public void setSafeDay(Integer safeDay) {
		set("safeDay", safeDay);
	}

	/**
	 * 系统效率（百分比的值）
	 */
	public BigDecimal getSysEffic() {
		return getBigDecimal("sysEffic");
	}

	public void setSysEffic(BigDecimal sysEffic) {
		set("sysEffic", sysEffic);
	}

	/**
	 * 今日收益（元）
	 */
	public BigDecimal getMoney() {
		return getBigDecimal("money");
	}

	public void setMoney(BigDecimal money) {
		set("money", money);
	}

	/**
	 *  累计收益（元）
	 */
	public BigDecimal getMoneyTotal() {
		return getBigDecimal("moneyTotal");
	}

	public void setMoneyTotal(BigDecimal moneyTotal) {
		set("moneyTotal", moneyTotal);
	}

}