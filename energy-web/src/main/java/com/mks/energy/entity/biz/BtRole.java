package com.mks.energy.entity.biz;

import java.io.Serializable;
import java.util.Date;

import org.beetl.sql.core.annotatoin.AssignID;

import com.mks.energy.entity.BaseInfo;


/**
 * 角色表
 */
public class BtRole extends BaseInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	public BtRole() {
	}

	/**
	 * 
	 */
	@AssignID("uuid")
	public String getId() {
		return getString("id");
	}

	public void setId(String id) {
		set("id", id);
	}

	/**
	 * 公司ID
	 */
	public String getCompanyId() {
		return getString("companyId");
	}

	public void setCompanyId(String companyId) {
		set("companyId", companyId);
	}

	/**
	 * 角色名称
	 */
	public String getName() {
		return getString("name");
	}

	public void setName(String name) {
		set("name", name);
	}

	/**
	 * 创建时间
	 */
	public Date getCreateTime() {
		return getDate("createTime");
	}

	public void setCreateTime(Date createTime) {
		set("createTime", createTime);
	}

	/**
	 * 修改时间
	 */
	public Date getUpdateTime() {
		return getDate("updateTime");
	}

	public void setUpdateTime(Date updateTime) {
		set("updateTime", updateTime);
	}

	/**
	 * 描述
	 */
	public String getDescription() {
		return getString("description");
	}

	public void setDescription(String description) {
		set("description", description);
	}

}