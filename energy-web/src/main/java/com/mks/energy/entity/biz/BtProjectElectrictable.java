package com.mks.energy.entity.biz;

import java.io.Serializable;
import java.math.BigDecimal;

import org.beetl.sql.core.annotatoin.AssignID;

import com.mks.energy.entity.BaseInfo;

/**
 * 项目分时电价表
 */
public class BtProjectElectrictable extends BaseInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	public BtProjectElectrictable() {
	}

	/**
	 * 
	 */
	@AssignID("uuid")
	public String getId() {
		return getString("id");
	}

	public void setId(String id) {
		set("id", id);
	}

	/**
	 * 
	 */
	public String getProjectId() {
		return getString("projectId");
	}

	public void setProjectId(String projectId) {
		set("projectId", projectId);
	}

	/**
	 * 时段名称
	 */
	public String getName() {
		return getString("name");
	}

	public void setName(String name) {
		set("name", name);
	}

	/**
	 * 开始时间
	 */
	public Integer getTimeStart() {
		return getInteger("timeStart");
	}

	public void setTimeStart(Integer timeStart) {
		set("timeStart", timeStart);
	}

	/**
	 * 结束时间
	 */
	public Integer getTimeEnd() {
		return getInteger("timeEnd");
	}

	public void setTimeEnd(Integer timeEnd) {
		set("timeEnd", timeEnd);
	}

	/**
	 * 电价
	 */
	public BigDecimal getMoney() {
		return getBigDecimal("money");
	}

	public void setMoney(BigDecimal money) {
		set("money", money);
	}

}