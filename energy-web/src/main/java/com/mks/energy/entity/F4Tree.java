package com.mks.energy.entity;

public class F4Tree extends BaseInfo{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public F4Tree() {
	}
	
	public String getName() {
		return getString("name");
	}

	public void setName(String name) {
		set("name",name);
	}
	
	public String getId() {
		return getString("id");
	}

	public void setId(String id) {
		set("id",id);
	}
	
	public String getpId() {
		return getString("pId");
	}

	public void setpId(String pId) {
		set("pId",pId);
	}
}
