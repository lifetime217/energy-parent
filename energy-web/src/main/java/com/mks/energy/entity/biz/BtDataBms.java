package com.mks.energy.entity.biz;

import java.io.Serializable;
import java.math.BigDecimal;

import com.mks.energy.entity.BaseInfo;


/**
 * BMS主数据
 */
public class BtDataBms extends BaseInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	public BtDataBms() {
	}

	/**
	 * 
	 */
	public Integer getId() {
		return getInteger("id");
	}

	public void setId(Integer id) {
		set("id", id);
	}

	/**
	 * 项目id
	 */
	public String getProjectId() {
		return getString("projectId");
	}

	public void setProjectId(String projectId) {
		set("projectId", projectId);
	}

	/**
	 * 电池包的唯一标示符
	 */
	public String getBmsSign() {
		return getString("bmsSign");
	}

	public void setBmsSign(String bmsSign) {
		set("bmsSign", bmsSign);
	}

	/**
	 * 最高单体电压
	 */
	public BigDecimal getMaxCellVolt() {
		return getBigDecimal("maxCellVolt");
	}

	public void setMaxCellVolt(BigDecimal maxCellVolt) {
		set("maxCellVolt", maxCellVolt);
	}

	/**
	 * 最低单体电压
	 */
	public BigDecimal getMinCellVolt() {
		return getBigDecimal("minCellVolt");
	}

	public void setMinCellVolt(BigDecimal minCellVolt) {
		set("minCellVolt", minCellVolt);
	}

	/**
	 * 最高单体电压序号
	 */
	public Integer getMaxCellVoltNo() {
		return getInteger("maxCellVoltNo");
	}

	public void setMaxCellVoltNo(Integer maxCellVoltNo) {
		set("maxCellVoltNo", maxCellVoltNo);
	}

	/**
	 * 最低单体电压序号
	 */
	public Integer getMinCellVoltNo() {
		return getInteger("minCellVoltNo");
	}

	public void setMinCellVoltNo(Integer minCellVoltNo) {
		set("minCellVoltNo", minCellVoltNo);
	}

	/**
	 * 最高温度
	 */
	public BigDecimal getMaxTemp() {
		return getBigDecimal("maxTemp");
	}

	public void setMaxTemp(BigDecimal maxTemp) {
		set("maxTemp", maxTemp);
	}

	/**
	 * 最低温度
	 */
	public BigDecimal getMinTemp() {
		return getBigDecimal("minTemp");
	}

	public void setMinTemp(BigDecimal minTemp) {
		set("minTemp", minTemp);
	}

	/**
	 * 最高温度序号
	 */
	public Integer getMaxTempNo() {
		return getInteger("maxTempNo");
	}

	public void setMaxTempNo(Integer maxTempNo) {
		set("maxTempNo", maxTempNo);
	}

	/**
	 * 最低温度序号
	 */
	public Integer getMinTempNo() {
		return getInteger("minTempNo");
	}

	public void setMinTempNo(Integer minTempNo) {
		set("minTempNo", minTempNo);
	}

	/**
	 * 动力电池SOC
	 */
	public BigDecimal getBatSoc() {
		return getBigDecimal("batSoc");
	}

	public void setBatSoc(BigDecimal batSoc) {
		set("batSoc", batSoc);
	}

	/**
	 * 动力电池SOH
	 */
	public Integer getBatSoh() {
		return getInteger("batSoh");
	}

	public void setBatSoh(Integer batSoh) {
		set("batSoh", batSoh);
	}

	/**
	 * 动力电池总电压
	 */
	public BigDecimal getBatVoltage() {
		return getBigDecimal("batVoltage");
	}

	public void setBatVoltage(BigDecimal batVoltage) {
		set("batVoltage", batVoltage);
	}

	/**
	 * 动力电池总电流
	 */
	public BigDecimal getBatCurrent() {
		return getBigDecimal("batCurrent");
	}

	public void setBatCurrent(BigDecimal batCurrent) {
		set("batCurrent", batCurrent);
	}

	/**
	 * 
	 */
	public Integer getAddTime() {
		return getInteger("addTime");
	}

	public void setAddTime(Integer addTime) {
		set("addTime", addTime);
	}

}