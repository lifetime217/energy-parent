package com.mks.energy.entity.biz;

import java.io.Serializable;
import java.math.BigDecimal;

import com.mks.energy.entity.BaseInfo;


/**
 * BMS - 电池单体温度 关系数据表
 */
public class BtDataBmsCellt extends BaseInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	public BtDataBmsCellt() {
	}

	/**
	 * 
	 */
	public Integer getId() {
		return getInteger("id");
	}

	public void setId(Integer id) {
		set("id", id);
	}

	/**
	 * 单体电压
	 */
	public BigDecimal getTemp() {
		return getBigDecimal("temp");
	}

	public void setTemp(BigDecimal temp) {
		set("temp", temp);
	}

	/**
	 * 单体序号
	 */
	public Integer getSeq() {
		return getInteger("seq");
	}

	public void setSeq(Integer seq) {
		set("seq", seq);
	}

	/**
	 * 
	 */
	public Integer getBmsDataId() {
		return getInteger("bmsDataId");
	}

	public void setBmsDataId(Integer bmsDataId) {
		set("bmsDataId", bmsDataId);
	}

	/**
	 * 单体标示符
	 */
	public String getSign() {
		return getString("sign");
	}

	public void setSign(String sign) {
		set("sign", sign);
	}

}