package com.mks.energy.entity.biz;

import java.io.Serializable;

import org.beetl.sql.core.annotatoin.AssignID;

import com.mks.energy.entity.BaseInfo;


/**
 * PCS基础信息维护表
 */
public class BtPcs extends BaseInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	public BtPcs() {
	}

	/**
	 * 
	 */
	@AssignID("uuid")
	public String getId() {
		return getString("id");
	}

	public void setId(String id) {
		set("id", id);
	}

	/**
	 * PCS厂家
	 */
	public String getPcsFactory() {
		return getString("pcsFactory");
	}

	public void setPcsFactory(String pcsFactory) {
		set("pcsFactory", pcsFactory);
	}

	/**
	 * 产品型号
	 */
	public String getPcsModel() {
		return getString("pcsModel");
	}

	public void setPcsModel(String pcsModel) {
		set("pcsModel", pcsModel);
	}

	/**
	 * 额定功率(kW)
	 */
	public String getPowerRating() {
		return getString("powerRating");
	}

	public void setPowerRating(String powerRating) {
		set("powerRating", powerRating);
	}

	/**
	 * 电网电压(V)  区间值xxx～xxx
	 */
	public String getMainsVoltage() {
		return getString("mainsVoltage");
	}

	public void setMainsVoltage(String mainsVoltage) {
		set("mainsVoltage", mainsVoltage);
	}

	/**
	 * 直流侧电压(V)   区间值 xxx～xxx
	 */
	public String getDcLinkVoltage() {
		return getString("dcLinkVoltage");
	}

	public void setDcLinkVoltage(String dcLinkVoltage) {
		set("dcLinkVoltage", dcLinkVoltage);
	}

	/**
	 * 最大输入功率(kW) 
	 */
	public String getMaxInputPower() {
		return getString("maxInputPower");
	}

	public void setMaxInputPower(String maxInputPower) {
		set("maxInputPower", maxInputPower);
	}

	/**
	 * 电网频率(Hz) 
	 */
	public String getMainsHz() {
		return getString("mainsHz");
	}

	public void setMainsHz(String mainsHz) {
		set("mainsHz", mainsHz);
	}

	/**
	 * 最大容量（kVA） 
	 */
	public String getMaxCapacity() {
		return getString("maxCapacity");
	}

	public void setMaxCapacity(String maxCapacity) {
		set("maxCapacity", maxCapacity);
	}

	/**
	 * 最大输入电流(A)
	 */
	public String getMaxOutputPower() {
		return getString("maxOutputPower");
	}

	public void setMaxOutputPower(String maxOutputPower) {
		set("maxOutputPower", maxOutputPower);
	}

	/**
	 * 
	 */
	public String getPhotoImg() {
		return getString("photoImg");
	}

	public void setPhotoImg(String photoImg) {
		set("photoImg", photoImg);
	}

}