package com.mks.energy.entity;

import java.util.Map;

/**
 * @author lifetime
 *
 */
public class F4PageQueryInfo {

	private int pageSize;//每页的数据量
	private int pageNumber;//当前页码
	private Map<String, Object> queryParams;//查询条件
	private String orderBy;//排序sql: name desc,time asc
	
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public int getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}
	public Map<String, Object> getQueryParams() {
		return queryParams;
	}
	public void setQueryParams(Map<String, Object> queryParams) {
		this.queryParams = queryParams;
	}
	public String getOrderBy() {
		return orderBy;
	}
	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}
	
	
}
