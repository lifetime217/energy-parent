package com.mks.energy.entity.biz;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import org.beetl.sql.core.annotatoin.AssignID;

import com.mks.energy.entity.BaseInfo;


/**
 * 设备组里模组的实时数据
 */
public class BtProjectDevicegroupModuleRuntime extends BaseInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	public BtProjectDevicegroupModuleRuntime() {
	}

	/**
	 * 
	 */
	@AssignID("uuid")
	public String getId() {
		return getString("id");
	}

	public void setId(String id) {
		set("id", id);
	}

	/**
	 * 项目id
	 */
	public String getProjectId() {
		return getString("projectId");
	}

	public void setProjectId(String projectId) {
		set("projectId", projectId);
	}

	/**
	 * 所属的设备组id
	 */
	public String getDevicegroupId() {
		return getString("devicegroupId");
	}

	public void setDevicegroupId(String devicegroupId) {
		set("devicegroupId", devicegroupId);
	}

	/**
	 * 所属pcsid
	 */
	public String getPcsId() {
		return getString("pcsId");
	}

	public void setPcsId(String pcsId) {
		set("pcsId", pcsId);
	}

	/**
	 * pcs标识
	 */
	public String getPcsSign() {
		return getString("pcsSign");
	}

	public void setPcsSign(String pcsSign) {
		set("pcsSign", pcsSign);
	}

	/**
	 * bmsid
	 */
	public String getBmsId() {
		return getString("bmsId");
	}

	public void setBmsId(String bmsId) {
		set("bmsId", bmsId);
	}

	/**
	 * bms标识
	 */
	public String getBmsSign() {
		return getString("bmsSign");
	}

	public void setBmsSign(String bmsSign) {
		set("bmsSign", bmsSign);
	}

	/**
	 * 电池单体id
	 */
	public String getBatteryId() {
		return getString("batteryId");
	}

	public void setBatteryId(String batteryId) {
		set("batteryId", batteryId);
	}

	/**
	 * 电池单体唯一标识
	 */
	public String getBatterySign() {
		return getString("batterySign");
	}

	public void setBatterySign(String batterySign) {
		set("batterySign", batterySign);
	}

	/**
	 * 电池单体编号
	 */
	public String getBatteryNum() {
		return getString("batteryNum");
	}

	public void setBatteryNum(String batteryNum) {
		set("batteryNum", batteryNum);
	}

	/**
	 * 天的数字表示方式（20180808）
	 */
	public Integer getDayVal() {
		return getInteger("dayVal");
	}

	public void setDayVal(Integer dayVal) {
		set("dayVal", dayVal);
	}

	/**
	 * 小时数
	 */
	public Integer getHourVal() {
		return getInteger("hourVal");
	}

	public void setHourVal(Integer hourVal) {
		set("hourVal", hourVal);
	}

	/**
	 * 电压
	 */
	public BigDecimal getV() {
		return getBigDecimal("v");
	}

	public void setV(BigDecimal v) {
		set("v", v);
	}

	/**
	 * 电流
	 */
	public BigDecimal getI() {
		return getBigDecimal("i");
	}

	public void setI(BigDecimal i) {
		set("i", i);
	}

	/**
	 * 最高单体电压
	 */
	public BigDecimal getMaxItemv() {
		return getBigDecimal("maxItemv");
	}

	public void setMaxItemv(BigDecimal maxItemv) {
		set("maxItemv", maxItemv);
	}

	/**
	 * 最低单体电压
	 */
	public BigDecimal getMinItemv() {
		return getBigDecimal("minItemv");
	}

	public void setMinItemv(BigDecimal minItemv) {
		set("minItemv", minItemv);
	}

	/**
	 * 电压极差
	 */
	public BigDecimal getDiffv() {
		return getBigDecimal("diffv");
	}

	public void setDiffv(BigDecimal diffv) {
		set("diffv", diffv);
	}

	/**
	 * 最高单体温度
	 */
	public BigDecimal getMaxItemt() {
		return getBigDecimal("maxItemt");
	}

	public void setMaxItemt(BigDecimal maxItemt) {
		set("maxItemt", maxItemt);
	}

	/**
	 * 最低单体温度
	 */
	public BigDecimal getMinItemt() {
		return getBigDecimal("minItemt");
	}

	public void setMinItemt(BigDecimal minItemt) {
		set("minItemt", minItemt);
	}

	/**
	 * 温度极差
	 */
	public BigDecimal getDifft() {
		return getBigDecimal("difft");
	}

	public void setDifft(BigDecimal difft) {
		set("difft", difft);
	}

	/**
	 * 平均温度
	 */
	public BigDecimal getAvgt() {
		return getBigDecimal("avgt");
	}

	public void setAvgt(BigDecimal avgt) {
		set("avgt", avgt);
	}

	/**
	 * 添加时间
	 */
	public Integer getAddTime() {
		return getInteger("addTime");
	}

	public void setAddTime(Integer addTime) {
		set("addTime", addTime);
	}

	/**
	 * 
	 */
	public Date getLastchange() {
		return getDate("lastchange");
	}

	public void setLastchange(Date lastchange) {
		set("lastchange", lastchange);
	}

}