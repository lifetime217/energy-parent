package com.mks.energy.core.tags;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.beetl.core.Configuration;
import org.beetl.core.GroupTemplate;
import org.beetl.core.Tag;
import org.beetl.core.Template;
import org.beetl.core.resource.ClasspathResourceLoader;
import org.springframework.util.StringUtils;

/**
 * 列表界面表格部分
 * 
 * @author lifetime
 *
 */
public class ListTablePart extends Tag {

	public void render() {
		if (this.args.length > 1) {
			Map<String, Object> params = (Map<String, Object>) this.args[1];
			if(StringUtils.isEmpty(params.get("url"))){
				throw new NullPointerException("<#listTable 标签必须具有url属性，用来定义 /listQuery查询请求的上一级请求路径");
			}
			if(StringUtils.isEmpty(params.get("fields"))){
				throw new NullPointerException("<#listTable 标签必须具有fields属性");
			}
			boolean notShowOpColumn = false;
			if(!StringUtils.isEmpty(params.get("notShowOpColumn")) && "true".equalsIgnoreCase((String)params.get("notShowOpColumn"))){
				notShowOpColumn = true;
			}
			
			StringBuffer listTableField = new StringBuffer();
			Set<String> queryFieldSet = new HashSet<String>();
			if(!StringUtils.isEmpty(params.get("queryFields"))){
				for (String f : String.valueOf(params.get("queryFields")).split(",")) {
					if(!StringUtils.isEmpty(f)){
						queryFieldSet.add(f);
					}
				}
			}
			StringBuffer listQueryField = new StringBuffer();
			String fields = (String) params.get("fields");
			String tmps[] = fields.split("\\|");
			ArrayList<Map<String, Object>> bindMap = new ArrayList<>();
			for (int i = 0; i < tmps.length; i++) {
				String[] props = tmps[i].split(",");
				Map<String, Object> map = new HashMap<>();
				for (int j = 0; j < props.length; j++) {
					if (j == 0) {
						map.put("key", props[j]);
					} else if (j == 1) {
						map.put("name", props[j]);
					} 
					if (props[j].endsWith("%")) {
						map.put("width", props[j]);
						map.put("isFixWidth", false);
					} else if (props[j].endsWith("px")) {
						map.put("width", props[j]);
						map.put("isFixWidth", true);
					} else if ("sort".equals(props[j])) {
						map.put("sortable", Boolean.TRUE);
					} else if ("no-sort".equals(props[j])) {
						map.put("sortable", Boolean.FALSE);
					} else if ("query".equals(props[j])) {
						map.put("queryable", Boolean.TRUE);
					} else if ("no-query".equals(props[j])) {
						map.put("queryable", Boolean.FALSE);
					}
				}
				if(!map.containsKey("width")){
					map.put("width", "10%");
					map.put("isFixWidth", false);
				}
				bindMap.add(map);
				if (i == tmps.length - 1) {
					listTableField.append(props[0]);
				} else {
					listTableField.append(props[0]).append(",");
				}

				if (map.get("queryable") != null && Boolean.TRUE.compareTo((Boolean) map.get("queryable")) == 0) {
					queryFieldSet.add(props[0]);
				}
			}
			
			for (Iterator<String> it = queryFieldSet.iterator(); it.hasNext();) {
				String f = it.next();
				listQueryField.append(f).append(",");
			}
			
			ClasspathResourceLoader resourceLoader = new ClasspathResourceLoader(ListTablePart.class.getPackage().getName().replaceAll("\\.", "/") + "/");
			Configuration cfg;
			try {
				cfg = Configuration.defaultConfiguration();
				GroupTemplate gt = new GroupTemplate(resourceLoader, cfg);
				Template t = gt.getTemplate("table.tag");
				t.binding("fields", bindMap);
				t.binding("url", params.get("url"));
				t.binding("notShowOpColumn", notShowOpColumn);
				t.binding("tableField", listTableField.toString());
				if (listQueryField.toString().endsWith(",")) {
					t.binding("queryField", listQueryField.toString().substring(0, listQueryField.toString().length() - 1));
				}
				String str = t.render();
				this.bw.writeString(str);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
