package com.mks.energy.core.ext;

import org.beetl.sql.core.ClasspathLoader;
import org.beetl.sql.core.SQLSource;
import org.beetl.sql.core.db.DBStyle;
import org.beetl.sql.core.db.MySqlStyle;

public class DevClasspathLoader extends ClasspathLoader {
	public DevClasspathLoader() {
		super("/sql");
	}

	public DevClasspathLoader(String root) {
		super(root, new MySqlStyle());
	}

	public DevClasspathLoader(String root, DBStyle dbs) {
		super(root, dbs);
	}

	@Override
	public boolean isModified(String id) {
		return true;
	}
	
	@Override
	public SQLSource getSQL(String id) {
		SQLSource tmp = super.getSQL(id);
		sqlSourceMap.clear();
		return tmp;
	}

}
