package com.mks.energy.core;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.mks.energy.service.IF4TreeService;


/**
 * @author lifetime
 *
 */
@Controller
@RequestMapping("/f4tree")
public class F4TreeAction {

	@Autowired
	private IF4TreeService service;

	/**
	 * 数据格式[{id:1, pId:0, name:"演示", open:false}]
	 * 
	 * @param table
	 * @return
	 */
	@RequestMapping(value = "/get/{table}")
	@ResponseBody
	public JSONObject tree(@PathVariable String table) {
		JSONObject res = new JSONObject();
		res.put("state", true);
		res.put("data", service.tree(table,new String[]{"treeId"},null));
		return res;
	}

}

