package com.mks.energy.core;

import java.util.ArrayList;
import java.util.List;

/**
 * 前端菜单值对象
 * 
 * @author lifetime
 *
 */
public class UserMenuVO {
	/**
	 * 启用状态
	 */
	public static final Integer Enable_Status = 10;
	
	/**
	 * 禁用状态
	 */
	public static final Integer Disable_Status = 99;
	
	private String name;
	private String number;
	private String url;
	private String iconClass;
	private boolean isActive;
	private List<UserMenuVO> childs;

	public UserMenuVO() {
		childs = new ArrayList<UserMenuVO>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public List<UserMenuVO> getChilds() {
		return childs;
	}

	public void setChilds(List<UserMenuVO> childs) {
		this.childs = childs;
	}

	public String getIconClass() {
		return iconClass;
	}

	public void setIconClass(String iconClass) {
		this.iconClass = iconClass;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

}
