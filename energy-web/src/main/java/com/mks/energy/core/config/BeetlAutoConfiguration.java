package com.mks.energy.core.config;

import java.io.IOException;

import org.beetl.core.resource.WebAppResourceLoader;
import org.beetl.ext.spring.BeetlGroupUtilConfiguration;
import org.beetl.ext.spring.BeetlSpringViewResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.web.WebMvcAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.util.StringUtils;

import com.mks.energy.core.tags.ListTablePart;

/**
 * 启用beetl
 * 
 * @author lifetime
 *
 */
@Configuration
@ConditionalOnClass({ BeetlSpringViewResolver.class })
@AutoConfigureAfter({ WebMvcAutoConfiguration.class })
@EnableConfigurationProperties({ BeetlAutoConfigurationProperties.class })
public class BeetlAutoConfiguration {

	@Autowired
	private BeetlAutoConfigurationProperties btlProperties;

	@Bean
	@ConditionalOnMissingBean({ BeetlSpringViewResolver.class })
	@ConditionalOnProperty(name = { "spring.beetl.enabled" }, matchIfMissing = true)
	public BeetlSpringViewResolver beetlView() {
		BeetlGroupUtilConfiguration configuration = new BeetlGroupUtilConfiguration();
		DefaultResourceLoader resourceLoader = new DefaultResourceLoader();
		String viewPath = null;
		try {
			viewPath = resourceLoader.getResource(this.btlProperties.getViewDir()).getFile().getPath();
			configuration.setResourceLoader(new WebAppResourceLoader(viewPath));
		} catch (IOException e) {
			throw new RuntimeException("spring.beetl.viewDir : " + viewPath + " ,that view path is not exists.");
		}

		Resource conf = null;
		if (!(StringUtils.isEmpty(this.btlProperties.getConfigFile())))
			conf = resourceLoader.getResource(this.btlProperties.getConfigFile());

		if (conf != null)
			configuration.setConfigFileResource(conf);

		configuration.init();
		configuration.getGroupTemplate().registerTag("listTable", ListTablePart.class);

		BeetlSpringViewResolver springViewResolver = new BeetlSpringViewResolver();
		if (StringUtils.isEmpty(this.btlProperties.getPrefix()))
			springViewResolver.setPrefix("/");
		else
			springViewResolver.setPrefix(this.btlProperties.getPrefix());

		if (StringUtils.isEmpty(this.btlProperties.getSuffix()))
			springViewResolver.setSuffix(".html");
		else
			springViewResolver.setSuffix(this.btlProperties.getSuffix());

		if (StringUtils.isEmpty(this.btlProperties.getContentType()))
			springViewResolver.setContentType("text/html;charset=UTF-8");
		else
			springViewResolver.setContentType(this.btlProperties.getContentType());

		springViewResolver.setOrder(this.btlProperties.getOrder());
		springViewResolver.setConfig(configuration);
		return springViewResolver;
	}

	public BeetlAutoConfigurationProperties getBtlProperties() {
		return this.btlProperties;
	}

	public void setBtlProperties(BeetlAutoConfigurationProperties btlProperties) {
		this.btlProperties = btlProperties;
	}
}
