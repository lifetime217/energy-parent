package com.mks.energy.core.validator;

import com.mks.energy.utils.HttpResult;

/**
 * @author lifetime
 *
 * @param <T>
 */
public interface AddValidator {
	
	public HttpResult addValidator();
}
