package com.mks.energy.core;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.mks.energy.entity.biz.UserInfo;
import com.mks.energy.service.IUserService;
import com.mks.energy.utils.Constant;
import com.mks.energy.utils.MD5;

/**
 * @author lifetime
 *
 */
@Controller
@RequestMapping("/user")
public class LoginAction {

	@Autowired
	private IUserService userService;

	@RequestMapping("/login")
	public String index(@RequestParam(required = false) String redirectURL, Model model) {
		model.addAttribute("redirectURL", redirectURL);
		return "/login/login";
	}

	@RequestMapping("/fail")
	public String fail(@RequestParam(required = false) String redirectURL, Model model) {
		model.addAttribute("redirectURL", redirectURL);
		model.addAttribute("msg", "用户名或密码错误");
		return "/login/login";
	}

	@RequestMapping(value = "/login/auth", method = RequestMethod.POST)
	public String login(@RequestParam String number, @RequestParam String pwd,
			@RequestParam(required = false) String redirectURL, HttpServletResponse response,
			HttpServletRequest request) {
		UserInfo info = userService.getUser(number, MD5.get(pwd));
		if (info == null) {
			if (StringUtils.isEmpty(redirectURL)) {
				return "redirect:/user/fail";
			}
			try {
				return "redirect:/user/fail?redirectURL=" + URLEncoder.encode(redirectURL, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				return "redirect:/user/fail";
			}
		}
		String accessToken = SessionManager.put(number, info,
				swapMenuWithPath(userService.getUserMenus(info), request.getContextPath()));
		Cookie cookie = new Cookie(Constant.UserNumber, number);
		cookie.setPath("/");
		response.addCookie(cookie);
		cookie = new Cookie(Constant.AccessToken, accessToken);
		cookie.setPath("/");
		response.addCookie(cookie);
		UserContext.init(SessionManager.get(number));
		UserContext.setCtxPath(request.getContextPath());
		if (StringUtils.isEmpty(redirectURL)) {
			return "redirect:/workbench";
		} else {
			return "redirect:" + redirectURL;
		}
	}

	protected List<UserMenuVO> swapMenuWithPath(List<UserMenuVO> menus, String ctxPath) {
		for (UserMenuVO menu : menus) {
			if (!StringUtils.isEmpty(menu.getUrl())) {
				if (menu.getUrl().startsWith("/")) {
					menu.setUrl(ctxPath + menu.getUrl());
				} else {
					menu.setUrl(ctxPath + "/" + menu.getUrl());
				}
			}
			if (menu.getChilds().size() != 0) {
				for (UserMenuVO child : menu.getChilds()) {
					if (!StringUtils.isEmpty(child.getUrl())) {
						if (child.getUrl().startsWith("/")) {
							child.setUrl(ctxPath + child.getUrl());
						} else {
							child.setUrl(ctxPath + "/" + child.getUrl());
						}
					}
				}
			}
		}
		return menus;
	}

	@RequestMapping("/logout")
	public String logout(@CookieValue(value = Constant.UserNumber, required = false) String number,
			@CookieValue(value = Constant.AccessToken, required = false) String accessToken) {
		if (SessionManager.isValid(number, accessToken)) {
			SessionManager.clear(number);
			return "redirect:/";
		}
		return "redirect:/user/fail";
	}

}
