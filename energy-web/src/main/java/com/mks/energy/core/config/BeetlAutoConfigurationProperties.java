package com.mks.energy.core.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author lifetime
 *
 */
@ConfigurationProperties(prefix = "spring.beetl")
public class BeetlAutoConfigurationProperties {
	private String configFile;
	private String viewDir;
	private String prefix;
	private String suffix;
	private String contentType;
	private int order;

	public String getPrefix() {
		return this.prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getSuffix() {
		return this.suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public String getContentType() {
		return this.contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public int getOrder() {
		return this.order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public String getViewDir() {
		return this.viewDir;
	}

	public void setViewDir(String viewDir) {
		this.viewDir = viewDir;
	}

	public String getConfigFile() {
		return this.configFile;
	}

	public void setConfigFile(String configFile) {
		this.configFile = configFile;
	}
}
