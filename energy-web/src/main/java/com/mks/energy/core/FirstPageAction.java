package com.mks.energy.core;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;

import com.mks.energy.utils.Constant;

/**
 * @author lifetime
 *
 */
@Controller
public class FirstPageAction {

	@RequestMapping({ "/", "" })
	public String index(@CookieValue(value=Constant.UserNumber,required=false) String number,@CookieValue(value=Constant.AccessToken,required=false) String accessToken) {
		if(SessionManager.isValid(number, accessToken)){
			return "redirect:/workbench";
		}
		return "redirect:/user/login";
	}
	
}
