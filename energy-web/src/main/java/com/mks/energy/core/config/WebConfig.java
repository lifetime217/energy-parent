package com.mks.energy.core.config;

import java.nio.charset.Charset;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import javax.servlet.Filter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.format.FormatterRegistry;
import org.springframework.http.MediaType;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter4;
import com.mks.energy.core.SessionInterceptor;

/**
 * @author lifetime
 *
 */
@Configuration
public class WebConfig extends WebMvcConfigurerAdapter {
	private static final org.slf4j.Logger log = LoggerFactory.getLogger(WebConfig.class);

	@Override
	public void addFormatters(FormatterRegistry registry) {
		super.addFormatters(registry);
		registry.addConverter(new Converter<String, Date>() {
			private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

			@Override
			public Date convert(String source) {
				if (source != null && source.trim().length() != 0) {
					try {
						return sdf.parse(source);
					} catch (ParseException e) {
						log.error(e.getMessage(), e.getCause());
					}
				}
				return null;
			}
		});
	}

	public void addInterceptors(InterceptorRegistry registry) {
		InterceptorRegistration registration = registry.addInterceptor(new SessionInterceptor());
		registration.addPathPatterns("/**");
		registration.excludePathPatterns("/user/**", "/", "/error");
		super.addInterceptors(registry);
	}

	@Configuration
	@ConditionalOnClass({ FastJsonHttpMessageConverter4.class })
	@ConditionalOnProperty(name = {
			"spring.http.converters.preferred-json-mapper" }, havingValue = "fastjson", matchIfMissing = true)
	protected static class FastJson2HttpMessageConverterConfiguration {

		protected FastJson2HttpMessageConverterConfiguration() {
		}

		@Bean
		@ConditionalOnMissingBean({ FastJsonHttpMessageConverter4.class })
		public FastJsonHttpMessageConverter4 fastJsonHttpMessageConverter() {
			FastJsonHttpMessageConverter4 converter = new FastJsonHttpMessageConverter4();
			FastJsonConfig fastJsonConfig = new FastJsonConfig();
			fastJsonConfig.setCharset(Charset.forName("UTF-8"));
			fastJsonConfig.setSerializerFeatures(SerializerFeature.PrettyFormat, SerializerFeature.WriteMapNullValue,
					SerializerFeature.WriteDateUseDateFormat);

			converter.setFastJsonConfig(fastJsonConfig);

			converter.setSupportedMediaTypes(Arrays.asList(new MediaType[] { MediaType.APPLICATION_JSON }));
			return converter;
		}
	}

//	@Bean
	public HandlerExceptionResolver customerExceptionPage() {
		HandlerExceptionResolver exceptionResolver = new HandlerExceptionResolver() {
			public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response,
					Object handler, Exception ex) {
				ModelAndView mav = new ModelAndView("/error-500");
				log.error(ex.getMessage(), ex.fillInStackTrace());
				mav.addObject("errorMsg", ex.toString());
				return mav;
			}
		};
		return exceptionResolver;
	}

	@Bean
	public Filter characterEncodingFilter() {
		CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
		characterEncodingFilter.setEncoding("UTF-8");
		characterEncodingFilter.setForceEncoding(true);
		return characterEncodingFilter;
	}

}
