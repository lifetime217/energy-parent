<div class="list-content-div">
	<input type="hidden" value="${url! }" class="listQueryUrl" />
	<input type="hidden" value="${tableField! }" class="listTableFields" />
	<input type="hidden" value="${queryField! }" class="listQueryFields" />
	<input type="hidden" value="${notShowOpColumn! }" class="notShowOpColumn" />
		
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th style="width:5%;">序号</th>
				<% for(var item in fields){ %>
					<th data-field="${item.key }" 
					<% if(item.isFixWidth!false == true){%>
					style="width:${item.width!};"
					<%}else{ %>
						width="${item.width! }"
					<%} %> >
					${item.name! }
					<% if(item.sortable!false == true){%>
						<i class="iconfont icon-sort-small sort-default-color pull-right"></i>
					<%} %>
					</th>
				<%} %>
				<%if(!notShowOpColumn){ %>
				<th>操作</th>
				<%} %>
			</tr>
		</thead>
		<tbody>

		</tbody>
	</table>
	<div class="page-info">
		<nav class="pull-right">
			<ul class="pager">
			</ul>
		</nav>
	</div>
</div>