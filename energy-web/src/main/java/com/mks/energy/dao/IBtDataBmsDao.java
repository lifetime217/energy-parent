package com.mks.energy.dao;

import org.beetl.sql.core.engine.PageQuery;

import com.mks.energy.core.ext.BaseDao;
import com.mks.energy.entity.biz.BtDataBms;

/**
 * @author lifetime
 *
 */
public interface IBtDataBmsDao extends BaseDao<BtDataBms> {

	public void queryPage(PageQuery<BtDataBms> pageQuery);

}
