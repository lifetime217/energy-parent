package com.mks.energy.dao;

import org.beetl.sql.core.engine.PageQuery;

import com.mks.energy.core.ext.BaseDao;
import com.mks.energy.entity.biz.BtPcs;

/**
 * @author lifetime
 *
 */
public interface IBtPcsDao extends BaseDao<BtPcs> {

	public void queryPage(PageQuery<BtPcs> pageQuery);

}
