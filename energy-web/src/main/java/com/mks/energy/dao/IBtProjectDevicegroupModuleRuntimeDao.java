package com.mks.energy.dao;

import org.beetl.sql.core.engine.PageQuery;

import com.mks.energy.core.ext.BaseDao;
import com.mks.energy.entity.biz.BtProjectDevicegroupModuleRuntime;

/**
 * @author lifetime
 *
 */
public interface IBtProjectDevicegroupModuleRuntimeDao extends BaseDao<BtProjectDevicegroupModuleRuntime> {

	public void queryPage(PageQuery<BtProjectDevicegroupModuleRuntime> pageQuery);

}
