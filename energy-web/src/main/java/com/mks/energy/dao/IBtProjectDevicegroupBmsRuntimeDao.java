package com.mks.energy.dao;

import org.beetl.sql.core.engine.PageQuery;

import com.mks.energy.core.ext.BaseDao;
import com.mks.energy.entity.biz.BtProjectDevicegroupBmsRuntime;

/**
 * @author lifetime
 *
 */
public interface IBtProjectDevicegroupBmsRuntimeDao extends BaseDao<BtProjectDevicegroupBmsRuntime> {

	public void queryPage(PageQuery<BtProjectDevicegroupBmsRuntime> pageQuery);

}
