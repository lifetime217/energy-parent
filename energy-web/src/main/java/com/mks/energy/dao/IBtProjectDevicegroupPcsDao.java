package com.mks.energy.dao;

import org.beetl.sql.core.annotatoin.Param;
import org.beetl.sql.core.engine.PageQuery;

import com.mks.energy.core.ext.BaseDao;
import com.mks.energy.entity.biz.BtProjectDevicegroupPcs;

/**
 * @author lifetime
 *
 */
public interface IBtProjectDevicegroupPcsDao extends BaseDao<BtProjectDevicegroupPcs> {

	public void queryPage(PageQuery<BtProjectDevicegroupPcs> pageQuery);

	public BtProjectDevicegroupPcs getPcs(@Param("devicegroupId") String devicegroupId);
}
