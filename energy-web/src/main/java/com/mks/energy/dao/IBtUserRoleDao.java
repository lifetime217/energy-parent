package com.mks.energy.dao;

import java.util.List;

import org.beetl.sql.core.annotatoin.Param;
import org.beetl.sql.core.engine.PageQuery;

import com.mks.energy.core.ext.BaseDao;
import com.mks.energy.entity.biz.BtUserRole;

/**
 * @author lifetime
 *
 */
public interface IBtUserRoleDao extends BaseDao<BtUserRole> {

	public void queryPage(PageQuery<BtUserRole> pageQuery);

	/** 
	 * @param id
	 * @return 
	 * @since JDK 1.8
	 * @author ghl  by 2018年12月20日上午1:29:21
	 * getAllMyRoleById:根据用户ID获取用户角色
	 */ 
	public List<BtUserRole> getAllMyRoleById(@Param("id")String id);

	/** 
	 * @param id 
	 * @since JDK 1.8
	 * @author ghl  by 2018年12月21日上午12:21:48
	 * deleteRoleByUserId:
	 */ 
	public void deleteRoleByUserId(@Param("userId")String userId);

}
