package com.mks.energy.dao;

import java.util.List;

import org.beetl.sql.core.annotatoin.Param;
import org.beetl.sql.core.engine.PageQuery;

import com.mks.energy.core.ext.BaseDao;
import com.mks.energy.entity.biz.BtRoleMenu;

/**
 * @author lifetime
 *
 */
public interface IBtRoleMenuDao extends BaseDao<BtRoleMenu> {

	public void queryPage(PageQuery<BtRoleMenu> pageQuery);

	/** 
	 * @param id
	 * @return 
	 * @since JDK 1.8
	 * @author ghl  by 2018年12月18日下午11:08:47
	 * getMenuByRoleId:
	 */ 
	public List<BtRoleMenu> getMenuByRoleId(@Param("id")String id);

	/** 
	 * @param roleId 
	 * @since JDK 1.8
	 * @author ghl  by 2018年12月19日上午12:41:00
	 * deteleRoleMenuByRoleId:
	 */ 
	public void deleteRoleMenuByRoleId(@Param("roleId")String roleId);

}
