package com.mks.energy.dao;

import org.beetl.sql.core.engine.PageQuery;

import com.mks.energy.core.ext.BaseDao;
import com.mks.energy.entity.biz.BtMenu;

/**
 * @author lifetime
 *
 */
public interface IBtMenuDao extends BaseDao<BtMenu> {

	public void queryPage(PageQuery<BtMenu> pageQuery);

}
