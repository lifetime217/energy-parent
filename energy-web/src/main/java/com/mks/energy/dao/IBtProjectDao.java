package com.mks.energy.dao;

import org.beetl.sql.core.engine.PageQuery;

import com.mks.energy.core.ext.BaseDao;
import com.mks.energy.entity.biz.BtProject;

public interface IBtProjectDao extends BaseDao<BtProject> {

	public void queryPage(PageQuery<BtProject> pageQuery);

}
