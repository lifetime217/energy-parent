package com.mks.energy.dao;

import java.util.List;

import org.beetl.sql.core.annotatoin.Param;
import org.beetl.sql.core.mapper.BaseMapper;

import com.mks.energy.entity.biz.MenuInfo;


public interface IMenuDao extends BaseMapper<MenuInfo>{

	
	public List<MenuInfo> getUserMenus(@Param("userId") String userId);
}
