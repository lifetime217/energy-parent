package com.mks.energy.dao;

import org.beetl.sql.core.engine.PageQuery;

import com.mks.energy.core.ext.BaseDao;
import com.mks.energy.entity.biz.BtDataPcs;

/**
 * @author lifetime
 *
 */
public interface IBtDataPcsDao extends BaseDao<BtDataPcs> {

	public void queryPage(PageQuery<BtDataPcs> pageQuery);

}
