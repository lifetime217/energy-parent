package com.mks.energy.dao;

import java.util.List;

import org.beetl.sql.core.annotatoin.Param;
import org.beetl.sql.core.engine.PageQuery;

import com.mks.energy.core.ext.BaseDao;
import com.mks.energy.entity.biz.BtReportType;

/**
 * @author lifetime
 *
 */
public interface IBtReportTypeDao extends BaseDao<BtReportType> {

	public void queryPage(PageQuery<BtReportType> pageQuery);

	/** 
	 * @param reportName
	 * @return 
	 * @since JDK 1.8
	 * @author ghl  by 2018年12月22日上午3:45:25
	 * getReportTypeByName:
	 */ 
	public BtReportType getReportTypeByName(@Param("reportName")String reportName);

	/** 
	 * @param companyId
	 * @return 
	 * @since JDK 1.8
	 * @author ghl  by 2018年12月22日上午5:09:35
	 * getAllReportTypeByCompanyId:
	 */ 
	public List<BtReportType> getAllReportTypeByCompanyId(@Param("companyId")String companyId);

}
