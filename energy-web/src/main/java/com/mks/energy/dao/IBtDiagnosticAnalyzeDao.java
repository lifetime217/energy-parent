package com.mks.energy.dao;

import org.beetl.sql.core.engine.PageQuery;

import com.mks.energy.core.ext.BaseDao;
import com.mks.energy.entity.biz.BtDiagnosticAnalyze;

/**
 * @author lifetime
 *
 */
public interface IBtDiagnosticAnalyzeDao extends BaseDao<BtDiagnosticAnalyze> {

	public void queryPage(PageQuery<BtDiagnosticAnalyze> pageQuery);

}
