package com.mks.energy.dao;

import java.util.List;

import org.beetl.sql.core.annotatoin.Param;
import org.beetl.sql.core.engine.PageQuery;

import com.mks.energy.core.ext.BaseDao;
import com.mks.energy.entity.biz.BtProjectDevicegroup;

/**
 * @author lifetime
 *
 */
public interface IBtProjectDevicegroupDao extends BaseDao<BtProjectDevicegroup> {

	public void queryPage(PageQuery<BtProjectDevicegroup> pageQuery);

	public List<BtProjectDevicegroup> getGroup(@Param("projectId") String projectId);
	
	public void deleteByProject(@Param("projectId") String projectId);
}
