package com.mks.energy.dao;

import java.util.List;

import org.beetl.sql.core.annotatoin.Param;
import org.beetl.sql.core.engine.PageQuery;

import com.mks.energy.core.ext.BaseDao;
import com.mks.energy.entity.biz.BtUserReportType;

/**
 * @author lifetime
 *
 */
public interface IBtUserReportTypeDao extends BaseDao<BtUserReportType> {

	public void queryPage(PageQuery<BtUserReportType> pageQuery);

	/** 
	 * @param id
	 * @return 
	 * @since JDK 1.8
	 * @author ghl  by 2018年12月22日上午5:18:28
	 * getAllMyReportTypeByUserId:
	 */ 
	public List<BtUserReportType> getAllMyReportTypeByUserId(@Param("userId")String id);

	/** 
	 * @param id 
	 * @since JDK 1.8
	 * @author ghl  by 2018年12月22日上午6:17:15
	 * deleteUserReportTypeByUserId:
	 */ 
	public void deleteUserReportTypeByUserId(@Param("userId")String id);

}
