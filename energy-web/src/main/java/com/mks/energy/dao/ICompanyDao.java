package com.mks.energy.dao;

import org.beetl.sql.core.annotatoin.Param;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;

import com.mks.energy.entity.BaseInfo;
import com.mks.energy.entity.biz.CompanyInfo;

public interface ICompanyDao extends BaseMapper<CompanyInfo> {

	void queryPage(PageQuery<? extends BaseInfo> pageQuery);

	CompanyInfo selectCompanyInfoByCompanyName(@Param("companyName")String companyName);

}
