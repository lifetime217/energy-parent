package com.mks.energy.dao;

import java.util.List;

import org.beetl.sql.core.annotatoin.Param;
import org.beetl.sql.core.engine.PageQuery;

import com.mks.energy.core.ext.BaseDao;
import com.mks.energy.entity.biz.BtDataAlert;

/**
 * @author lifetime
 *
 */
public interface IBtDataAlertDao extends BaseDao<BtDataAlert> {

	public void queryPage(PageQuery<BtDataAlert> pageQuery);

	/**
	 * @author ghl
	 * @Date 2019年1月31日下午5:13:23
	 * @TODO 
	 * @param projectId
	 * @return
	 */
	public List<BtDataAlert> getBugByProjectId(@Param("projectId")String projectId);

}
