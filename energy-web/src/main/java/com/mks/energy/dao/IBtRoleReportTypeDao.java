package com.mks.energy.dao;

import java.util.List;

import org.beetl.sql.core.annotatoin.Param;
import org.beetl.sql.core.engine.PageQuery;

import com.mks.energy.core.ext.BaseDao;
import com.mks.energy.entity.biz.BtRoleReportType;

/**
 * @author lifetime
 *
 */
public interface IBtRoleReportTypeDao extends BaseDao<BtRoleReportType> {

	public void queryPage(PageQuery<BtRoleReportType> pageQuery);

	/** 
	 * @param id
	 * @return 
	 * @since JDK 1.8
	 * @author ghl  by 2018年12月22日下午5:57:45
	 * getReportTypeByRoleId:获取当前角色的所有的可查看的报告类型
	 */ 
	public List<BtRoleReportType> getReportTypeByRoleId(@Param("roleId")String id);

	/** 
	 * @param roleId 
	 * @since JDK 1.8
	 * @author ghl  by 2018年12月22日下午6:36:40
	 * deleteRoleReportTypeByRoleId:删除角色原有的查看报告权限
	 */ 
	public void deleteRoleReportTypeByRoleId(@Param("roleId")String roleId);

}
