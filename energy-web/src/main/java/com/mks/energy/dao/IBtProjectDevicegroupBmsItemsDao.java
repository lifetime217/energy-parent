package com.mks.energy.dao;

import java.util.List;

import org.beetl.sql.core.annotatoin.Param;
import org.beetl.sql.core.engine.PageQuery;

import com.mks.energy.core.ext.BaseDao;
import com.mks.energy.entity.biz.BtProjectDevicegroupBmsItems;

/**
 * @author lifetime
 *
 */
public interface IBtProjectDevicegroupBmsItemsDao extends BaseDao<BtProjectDevicegroupBmsItems> {

	public void queryPage(PageQuery<BtProjectDevicegroupBmsItems> pageQuery);

	public List<BtProjectDevicegroupBmsItems> getBmsItems(@Param("devicegroupBmsId") String devicegroupBmsId);
}
