package com.mks.energy.dao;

import java.util.List;

import org.beetl.sql.core.annotatoin.Param;
import org.beetl.sql.core.engine.PageQuery;

import com.mks.energy.core.ext.BaseDao;
import com.mks.energy.entity.biz.BtProjectDevicegroupBms;

/**
 * @author lifetime
 *
 */
public interface IBtProjectDevicegroupBmsDao extends BaseDao<BtProjectDevicegroupBms> {

	public void queryPage(PageQuery<BtProjectDevicegroupBms> pageQuery);

	public List<BtProjectDevicegroupBms> getBms(@Param("devicegroupId") String devicegroupId);
}
