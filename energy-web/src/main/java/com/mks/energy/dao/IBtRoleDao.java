package com.mks.energy.dao;

import java.util.List;

import org.beetl.sql.core.annotatoin.Param;
import org.beetl.sql.core.engine.PageQuery;

import com.mks.energy.core.ext.BaseDao;
import com.mks.energy.entity.biz.BtRole;

/**
 * @author lifetime
 *
 */
public interface IBtRoleDao extends BaseDao<BtRole> {

	public void queryPage(PageQuery<BtRole> pageQuery);

	/** 
	 * @author ghl  by 2018年12月17日下午5:35:48
	 * @param name
	 * @return 
	 * @since JDK 1.8
	 * getRoleByName:
	 */ 
	public BtRole getRoleByName(@Param("name")String name);

	/** 
	 * @param companyId
	 * @return 
	 * @since JDK 1.8
	 * @author ghl  by 2018年12月20日上午1:17:05
	 * getAllRoleByCompanyId:
	 */ 
	public List<BtRole> getAllRoleByCompanyId(@Param("companyId")String companyId);

}
