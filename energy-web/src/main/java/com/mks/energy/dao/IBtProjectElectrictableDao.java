package com.mks.energy.dao;

import org.beetl.sql.core.engine.PageQuery;

import com.mks.energy.core.ext.BaseDao;
import com.mks.energy.entity.biz.BtProjectElectrictable;

public interface IBtProjectElectrictableDao extends BaseDao<BtProjectElectrictable> {

	public void queryPage(PageQuery<BtProjectElectrictable> pageQuery);

}
