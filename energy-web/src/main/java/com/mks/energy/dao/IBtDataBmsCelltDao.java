package com.mks.energy.dao;

import org.beetl.sql.core.engine.PageQuery;

import com.mks.energy.core.ext.BaseDao;
import com.mks.energy.entity.biz.BtDataBmsCellt;

/**
 * @author lifetime
 *
 */
public interface IBtDataBmsCelltDao extends BaseDao<BtDataBmsCellt> {

	public void queryPage(PageQuery<BtDataBmsCellt> pageQuery);

}
