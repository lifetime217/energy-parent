package com.mks.energy.dao;

import org.beetl.sql.core.annotatoin.Param;
import org.beetl.sql.core.engine.PageQuery;

import com.mks.energy.core.ext.BaseDao;
import com.mks.energy.entity.biz.BtProjectRuntime;

/**
 * @author lifetime
 *
 */
public interface IBtProjectRuntimeDao extends BaseDao<BtProjectRuntime> {

	public void queryPage(PageQuery<BtProjectRuntime> pageQuery);

	/**
	 * @author ghl
	 * @Date 2019年1月30日下午6:48:51
	 * @param projectId
	 * @param date
	 * @return
	 */
	public BtProjectRuntime getProjectRuntimeDataByProjectIdAndDate(@Param("projectId")String projectId, @Param("date")Integer date);

}
