package com.mks.energy.dao;

import org.beetl.sql.core.engine.PageQuery;

import com.mks.energy.core.ext.BaseDao;
import com.mks.energy.entity.biz.BtBms;

/**
 * @author lifetime
 *
 */
public interface IBtBmsDao extends BaseDao<BtBms> {

	public void queryPage(PageQuery<BtBms> pageQuery);

}
