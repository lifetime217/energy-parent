package com.mks.energy.dao;

import org.beetl.sql.core.annotatoin.Param;
import org.beetl.sql.core.mapper.BaseMapper;

import com.mks.energy.entity.biz.BtUserMenu;

public interface IBtUserMenuDao extends BaseMapper<BtUserMenu> {

	/** 
	 * @param id 
	 * @since JDK 1.8
	 * @author ghl  by 2018年12月21日上午1:22:18
	 * deleteAllMenuByUserId:
	 */ 
	void deleteAllMenuByUserId(@Param("userId")String id);

}
