package com.mks.energy.dao;

import org.beetl.sql.core.engine.PageQuery;

import com.mks.energy.core.ext.BaseDao;
import com.mks.energy.entity.biz.BtProjectDevicegroupPcsRuntime;

/**
 * @author lifetime
 *
 */
public interface IBtProjectDevicegroupPcsRuntimeDao extends BaseDao<BtProjectDevicegroupPcsRuntime> {

	public void queryPage(PageQuery<BtProjectDevicegroupPcsRuntime> pageQuery);

}
