package com.mks.energy.dao;

import org.beetl.sql.core.engine.PageQuery;

import com.mks.energy.core.ext.BaseDao;
import com.mks.energy.entity.biz.BtDataBmsCellv;

/**
 * @author lifetime
 *
 */
public interface IBtDataBmsCellvDao extends BaseDao<BtDataBmsCellv> {

	public void queryPage(PageQuery<BtDataBmsCellv> pageQuery);

}
