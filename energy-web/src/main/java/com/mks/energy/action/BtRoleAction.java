package com.mks.energy.action;

import java.util.Date;

import org.beetl.sql.core.engine.PageQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.mks.energy.core.UserContext;
import com.mks.energy.core.ext.BaseAction;
import com.mks.energy.core.ext.IBaseService;
import com.mks.energy.entity.biz.BtRole;
import com.mks.energy.service.IBtRoleService;
import com.mks.energy.utils.HttpResult;

/**
 * @author lifetime
 *
 */
@Controller
@RequestMapping("/system/role")
public class BtRoleAction  implements BaseAction<BtRole> {
	
	public static final Logger log = LoggerFactory.getLogger(BtRole.class);

	@Autowired
	private IBtRoleService roleService;
	
	@RequestMapping
	public String index() {
		return "system/role-list";
	}

	@Override
	public IBaseService<BtRole> getService() {
		return roleService;
	}
	

	/** 
	 * @param pageQuery
	 * @return
	 * @author ghl 2018年12月18日上午12:05:34
	 * getQueryList:
	 */  
	@Override
	public PageQuery<BtRole> getQueryList(PageQuery<BtRole> pageQuery) {
		return roleService.getQueryList(pageQuery);
	}

	
	/** 
	 * @param entity
	 * @return
	 * @author ghl 2018年12月18日上午12:05:41
	 * add:
	 */  
	@RequestMapping("/addRole")
	@ResponseBody
	public HttpResult add(BtRole entity) {
		if(StringUtils.isEmpty(entity.getName())) {
			return HttpResult.fail(500, "角色名称不能为空");
		}
		//角色名称唯一性校验
		BtRole role = roleService.checkNameUnique(entity.getName()); 
		if(null != role ) {
			return HttpResult.fail( "角色已经存在");
		}
		try {
			entity.setCompanyId(UserContext.get().getCompanyId());
			entity.setCreateTime(new Date());
			roleService.add(entity);
			return HttpResult.success("角色【"+entity.getName()+"】添加成功");
		} catch (Exception e) {
			log.error(e.getMessage(), e.fillInStackTrace());
			return HttpResult.fail(e.getMessage());
		}
	}
	
	
	/** 
	 * @param id
	 * @return 
	 * @since JDK 1.8
	 * @author ghl  by 2018年12月18日上午12:05:54
	 * view:
	 */ 
	@RequestMapping("/view/{id}")
	@ResponseBody
	public HttpResult view(@PathVariable("id")String id) {
		//根据id获取对象
		try {
			BtRole btRole = roleService.get(id);
			return HttpResult.success(null, btRole);
		} catch (Exception e) {
			log.error(e.getMessage(), e.fillInStackTrace());
			return HttpResult.fail(e.getMessage());
		}
	}
	
	
	/** 
	 * @param role
	 * @return 
	 * @since JDK 1.8
	 * @author ghl  by 2018年12月18日上午12:06:37
	 * update:
	 */ 
	@RequestMapping("/update")
	@ResponseBody
	public HttpResult update(BtRole role) {
		try {
			role.setUpdateTime(new Date());
			 roleService.updatePart(role);
			return HttpResult.success("更新成功");
		} catch (Exception e) {
			log.error(e.getMessage(), e.fillInStackTrace());
			return HttpResult.fail(e.getMessage());
		}
	}
	
	
	
	/** 
	 * @param id
	 * @return 
	 * @since JDK 1.8
	 * @author ghl  by 2018年12月18日上午12:06:52
	 * delete:
	 */ 
	@RequestMapping("/del/{id}")
	@ResponseBody
	public HttpResult delete(@PathVariable("id")String id) {
		//根据id删除对象
		try {
			 roleService.delete(id);
			return HttpResult.success("删除成功");
		} catch (Exception e) {
			log.error(e.getMessage(), e.fillInStackTrace());
			return HttpResult.fail(e.getMessage());
		}
	}
	
	
	
	/** 
	 * @param id
	 * @param companyId
	 * @return 
	 * @since JDK 1.8
	 * @author ghl  by 2018年12月22日下午4:58:17
	 * getMenu:点击分配权限
	 */ 
	@RequestMapping("/getPermission/{id}")
	@ResponseBody
	public HttpResult getPermission(@PathVariable("id")String id) {
		try {
			//获取当前角色的权限(菜单+查看报告)
			JSONObject data= roleService.getPermission(id,UserContext.get().getCompanyId());
			return HttpResult.success("成功", data);
		} catch (Exception e) {
			e.printStackTrace();
			return HttpResult.fail("网络异常");
		}
	}
	
	
	/** 
	 * @param roleId
	 * @param menuIds
	 * @param rptTypeIds
	 * @return 
	 * @since JDK 1.8
	 * @author ghl  by 2018年12月22日下午6:26:45
	 * saveRolePermission:保存角色权限
	 */ 
	@RequestMapping("/savePermission/{id}")
	@ResponseBody
	public HttpResult saveRolePermission(@PathVariable("id")String roleId,@RequestParam("menuIds[]")String[] menuIds,@RequestParam("rptTypeIds[]")String[] rptTypeIds) {
		//保存角色权限的逻辑是先删除该角色原有的菜单和查看报告的权限,再新增这一次选中的菜单和查看报告的权限
		try {
			roleService.saveRolePermission(roleId,menuIds,rptTypeIds);
			return HttpResult.success("操作成功");
		} catch (Exception e) {
			log.error(e.getMessage(),e.fillInStackTrace());
			return HttpResult.fail(e.getMessage());
		}
	}
	
	
}
