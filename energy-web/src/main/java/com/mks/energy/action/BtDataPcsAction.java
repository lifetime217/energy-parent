package com.mks.energy.action;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.mks.energy.core.ext.BaseAction;
import com.mks.energy.core.ext.IBaseService;
import com.mks.energy.entity.biz.BtDataPcs;
import com.mks.energy.service.IBtDataPcsService;
import com.mks.energy.service.IProjectService;

/**
 * @author lifetime
 *
 */
@Controller
@RequestMapping("pcs")
public class BtDataPcsAction  implements BaseAction<BtDataPcs> {
	@Autowired
	private IProjectService projectService;
	
	@Autowired
	private IBtDataPcsService service;
	
	@RequestMapping
	public String index(Model m) {
		m.addAttribute("projects", projectService.findAll());
		return "pcs/pcs-list";
	}

	@Override
	public IBaseService<BtDataPcs> getService() {
		return service;
	}

}
