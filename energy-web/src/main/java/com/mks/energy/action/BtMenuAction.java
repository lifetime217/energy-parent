package com.mks.energy.action;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.mks.energy.core.ext.BaseAction;
import com.mks.energy.core.ext.IBaseService;
import com.mks.energy.entity.biz.BtMenu;
import com.mks.energy.service.IBtMenuService;

/**
 * @author lifetime
 *
 */
//@Controller
@RequestMapping("btmenu")
public class BtMenuAction  implements BaseAction<BtMenu> {

	@Autowired
	private IBtMenuService service;
	
	@RequestMapping
	public String index() {
		return "btmenu";
	}

	@Override
	public IBaseService<BtMenu> getService() {
		return service;
	}

}
