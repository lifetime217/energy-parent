package com.mks.energy.action;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.mks.energy.core.ext.BaseAction;
import com.mks.energy.core.ext.IBaseService;
import com.mks.energy.entity.biz.BtDataBms;
import com.mks.energy.service.IBtDataBmsService;
import com.mks.energy.service.IProjectService;

/**
 * @author lifetime
 *
 */
@Controller
@RequestMapping("bms")
public class BtDataBmsAction  implements BaseAction<BtDataBms> {

	@Autowired
	private IProjectService projectService;
	
	@Autowired
	private IBtDataBmsService service;
	
	@RequestMapping
	public String index(Model m) {
		m.addAttribute("projects", projectService.findAll());
		return "bms/bms-list";
	}

	@Override
	public IBaseService<BtDataBms> getService() {
		return service;
	}

}
