package com.mks.energy.action;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.mks.energy.core.ext.BaseAction;
import com.mks.energy.core.ext.IBaseService;
import com.mks.energy.entity.biz.BtUserReportType;
import com.mks.energy.service.IBtUserReportTypeService;

/**
 * @author lifetime
 *
 */
//@Controller
@RequestMapping("btuserreporttype")
public class BtUserReportTypeAction  implements BaseAction<BtUserReportType> {

	@Autowired
	private IBtUserReportTypeService service;
	
	@RequestMapping
	public String index() {
		return "btuserreporttype";
	}

	@Override
	public IBaseService<BtUserReportType> getService() {
		return service;
	}

}
