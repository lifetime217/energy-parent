package com.mks.energy.action;

import java.util.Date;
import java.util.Map;

import org.beetl.sql.core.engine.PageQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.mks.energy.core.UserContext;
import com.mks.energy.core.ext.BaseAction;
import com.mks.energy.core.ext.IBaseService;
import com.mks.energy.entity.biz.UserInfo;
import com.mks.energy.service.IUserService;
import com.mks.energy.utils.HttpResult;
import com.mks.energy.utils.MD5;

/**
 * 用户相关的控制器
 * 
 * @author lifetime
 *
 */
@Controller
@RequestMapping("/system/user")
public class UserAction implements BaseAction<UserInfo> {
	private static final Logger logger = LoggerFactory.getLogger(UserAction.class);

	@Autowired
	private IUserService userService;


	@RequestMapping
	public String index() {
		return "system/users-list";
	}

	@RequestMapping("/edit")
	public String my(Model model) {
		model.addAttribute("userId", UserContext.get().getId());
		model.addAttribute("nick", UserContext.get().getAlias());
		return "common/user-edit";
	}


	@RequestMapping("/update/{id}")
	@ResponseBody
	public HttpResult updateUserInfo(@PathVariable("id") String id, @RequestParam Map<String, String> params) {
		if (StringUtils.isEmpty(params.get("pwd1")) || StringUtils.isEmpty(params.get("pwd2"))) {
			return HttpResult.fail("密码不能为空");
		}
		if (params.get("pwd1") != null && params.get("pwd2") != null
				&& !params.get("pwd1").equals(params.get("pwd2"))) {
			return HttpResult.fail("两次密码不一致");
		}
		UserInfo user = new UserInfo();
		if (!StringUtils.isEmpty(params.get("nick"))) {
			user.setNick(params.get("nick"));
		}
		user.setId(id);
		user.setLoginPwd(MD5.get(params.get("pwd1")));
		user.setUpdateTime(new Date());
		try {
			userService.updatePart(user);
			return HttpResult.success("修改成功，请重新登录生效。");
		} catch (Exception e) {
			logger.error(e.getMessage(), e.fillInStackTrace());
			return HttpResult.fail("网络异常");
		}

	}


	@Override
	public PageQuery<UserInfo> getQueryList(PageQuery<UserInfo> pageQuery) {
		return userService.getQueryList(pageQuery);
	}

	@RequestMapping("/delete/{id}")
	@ResponseBody
	public HttpResult delete(@PathVariable String id) {
		UserInfo info = new UserInfo();
		info.setId(id);
		info.setStatus(0);
		try {
			userService.updatePart(info);
			return HttpResult.success();
		} catch (Exception e) {
			logger.error(e.getMessage(), e.fillInStackTrace());
			return HttpResult.fail(e.getMessage());
		}
	}

	@RequestMapping("/resetPwd/{id}")
	@ResponseBody
	public HttpResult resetPwd(@PathVariable String id) {
		if (UserContext.get().isAdmin() || UserContext.get().isSupperAdmin()) {
			UserInfo info = userService.get(id);
			info.setLoginPwd(MD5.get("888888"));
			info.setUpdateTime(new Date());
			try {
				userService.save(info);
				return HttpResult.success("恭喜，账号【" + info.getLoginNo() + "】密码已重置成功！</br>新密码为【888888】，请妥善保管。");
			} catch (Exception e) {
				logger.error(e.getMessage(), e.fillInStackTrace());
				return HttpResult.fail(e.getMessage());
			}
		} else {
			return HttpResult.fail("请使用管理员账号进行操作！");
		}
	}

	@RequestMapping("/addUser")
	@ResponseBody
	public HttpResult add(@RequestParam Map<String, String> params) {
		if (StringUtils.isEmpty(params.get("newAccount")) || StringUtils.isEmpty(params.get("pwd1"))
				|| StringUtils.isEmpty(params.get("pwd2"))) {
			return HttpResult.missParams();
		}
		if (params.get("pwd1") != null && !params.get("pwd1").equals(params.get("pwd2"))) {
			return HttpResult.fail("两次密码不一致");
		}
		UserInfo info = userService.checkUserName(params.get("newAccount"));
		if (info != null) {
			return HttpResult.fail("账号已被占用");
		}
		info = new UserInfo();
		info.setLoginNo(params.get("newAccount"));
		info.setLoginPwd(MD5.get(params.get("pwd1")));
		info.setCompanyId(UserContext.get().getCompanyId());
		info.setCompanyCode(UserContext.get().getCompanyCode());
		info.setCompanyName(UserContext.get().getCompanyName());
		info.setNick(params.get("nick"));
		info.setStatus(1);
		info.setUserType(1);
		info.setCreateTime(new Date());
		try {
			userService.add(info);
			return HttpResult.success("账号【" + params.get("newAccount") + "】添加成功 ");
		} catch (Exception e) {
			logger.error(e.getMessage(), e.fillInStackTrace());
			return HttpResult.fail(e.getMessage());
		}
	}


	/** 
	 * @param id
	 * @return 
	 * @since JDK 1.8
	 * @author ghl  by 2018年12月20日上午12:14:52
	 * getPermission:获取权限(角色)
	 */
	@RequestMapping("/permission/{id}")
	@ResponseBody
	public HttpResult getPermission(@PathVariable("id") String id) {
		if (UserContext.get().isAdmin() || UserContext.get().isSupperAdmin()) {
			try {
				// 获取当前用户所有的权限
				JSONObject res = userService.getAllPermissionByUserId(id, UserContext.get().getCompanyId());
				return HttpResult.success("成功", res);
			} catch (Exception e) {
				logger.error(e.getMessage(), e.fillInStackTrace());
				return HttpResult.fail(e.getMessage());
			}
		} else {
			return HttpResult.fail("请使用管理员账号进行操作！");
		}
	}


	/** 
	 * @param id
	 * @param roleIds
	 * @return 
	 * @since JDK 1.8
	 * @author ghl  by 2018年12月21日上午12:11:01
	 * savePermission:权限保存(保存角色)
	 */
	@RequestMapping("/permission/save/{id}")
	@ResponseBody
	public HttpResult savePermission(@PathVariable("id") String id, @RequestParam("roleIds[]") String[] roleIds) {
		if (UserContext.get().isAdmin() || UserContext.get().isSupperAdmin()) {
			// 保存角色,先删除用户原有角色,再新增
			try {
				UserInfo userInfo = userService.get(id);
				userService.saveAllMyPermission(id, roleIds);
				return HttpResult.success("恭喜，账号【" + userInfo.getLoginNo() + "】授权成功<br>下次登录生效！");
			} catch (Exception e) {
				logger.error(e.getMessage(), e.fillInStackTrace());
				return HttpResult.fail(e.getMessage());
			}
		} else {
			return HttpResult.fail("请使用管理员账号进行操作！");
		}
	}


	@Override
	public IBaseService<UserInfo> getService() {
		return userService;
	}

}
