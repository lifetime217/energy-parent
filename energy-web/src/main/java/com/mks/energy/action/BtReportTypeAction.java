package com.mks.energy.action;

import java.util.Date;

import org.beetl.sql.core.engine.PageQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mks.energy.core.UserContext;
import com.mks.energy.core.ext.BaseAction;
import com.mks.energy.core.ext.IBaseService;
import com.mks.energy.entity.biz.BtReportType;
import com.mks.energy.service.IBtReportTypeService;
import com.mks.energy.utils.HttpResult;

/**
 * @author lifetime
 *
 */
@Controller
@RequestMapping("/system/report")
public class BtReportTypeAction implements BaseAction<BtReportType> {

	public static final Logger log = LoggerFactory.getLogger(BtReportTypeAction.class);

	@Autowired
	private IBtReportTypeService reportTypeService;

	@RequestMapping
	public String index() {
		return "system/report-type";
	}

	@Override
	public IBaseService<BtReportType> getService() {
		return reportTypeService;
	}

	/** 
	 * @param pageQuery
	 * @return
	 * @author ghl 2018年12月22日上午2:11:08
	 * getQueryList:
	 */
	@Override
	public PageQuery<BtReportType> getQueryList(PageQuery<BtReportType> pageQuery) {
		return reportTypeService.getQueryList(pageQuery);
	}

	/** 
	 * @param entity
	 * @return 
	 * @since JDK 1.8
	 * @author ghl  by 2018年12月22日上午3:54:07
	 * addReportType:添加报告类型
	 */
	@RequestMapping("/addReportType")
	@ResponseBody
	public HttpResult addReportType(BtReportType entity) {
		if (StringUtils.isEmpty(entity.getReportName())) {
			return HttpResult.fail(500, "报告名称不能为空");
		}
		// 报告名称唯一性校验
		BtReportType reportType = reportTypeService.checkNameUnique(entity.getReportName());
		if (null != reportType) {
			return HttpResult.fail("报告类型已经存在");
		}
		try {
			entity.setCompanyId(UserContext.get().getCompanyId());
			entity.setCreateTime(new Date());
			reportTypeService.add(entity);
			return HttpResult.success("报告类型【" + entity.getReportName() + "】添加成功");
		} catch (Exception e) {
			log.error(e.getMessage(), e.fillInStackTrace());
			return HttpResult.fail(e.getMessage());
		}
	}


	/** 
	 * @param id
	 * @return 
	 * @since JDK 1.8
	 * @author ghl  by 2018年12月22日上午3:59:52
	 * viewReportType:查看
	 */
	@RequestMapping("/view/{id}")
	@ResponseBody
	public HttpResult viewReportType(@PathVariable("id") String id) {
		// 根据id获取对象
		try {
			BtReportType reportType = reportTypeService.get(id);
			return HttpResult.success(null, reportType);
		} catch (Exception e) {
			log.error(e.getMessage(), e.fillInStackTrace());
			return HttpResult.fail(e.getMessage());
		}
	}


	/** 
	 * @param entity
	 * @return 
	 * @since JDK 1.8
	 * @author ghl  by 2018年12月22日上午4:00:01
	 * updateReportType:更新
	 */
	@RequestMapping("/update")
	@ResponseBody
	public HttpResult updateReportType(BtReportType entity) {
		try {
			entity.setUpdateTime(new Date());
			reportTypeService.updatePart(entity);
			return HttpResult.success("更新成功");
		} catch (Exception e) {
			log.error(e.getMessage(), e.fillInStackTrace());
			return HttpResult.fail(e.getMessage());
		}
	}


	/** 
	 * @param id
	 * @return 
	 * @since JDK 1.8
	 * @author ghl  by 2018年12月22日上午4:00:08
	 * deleteReportTypeById:删除
	 */
	@RequestMapping("/del/{id}")
	@ResponseBody
	public HttpResult deleteReportTypeById(@PathVariable("id") String id) {
		// 根据id删除对象
		try {
			reportTypeService.delete(id);
			return HttpResult.success("删除成功");
		} catch (Exception e) {
			log.error(e.getMessage(), e.fillInStackTrace());
			return HttpResult.fail(e.getMessage());
		}
	}

}
