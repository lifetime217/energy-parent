package com.mks.energy.action;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.mks.energy.core.ext.BaseAction;
import com.mks.energy.core.ext.IBaseService;
import com.mks.energy.entity.biz.BtProjectDevicegroupModuleRuntime;
import com.mks.energy.service.IBtProjectDevicegroupModuleRuntimeService;

/**
 * @author lifetime
 *
 */
//@Controller
@RequestMapping("btprojectdevicegroupmoduleruntime")
public class BtProjectDevicegroupModuleRuntimeAction  implements BaseAction<BtProjectDevicegroupModuleRuntime> {

	@Autowired
	private IBtProjectDevicegroupModuleRuntimeService service;
	
	@RequestMapping
	public String index() {
		return "btprojectdevicegroupmoduleruntime";
	}

	@Override
	public IBaseService<BtProjectDevicegroupModuleRuntime> getService() {
		return service;
	}

}
