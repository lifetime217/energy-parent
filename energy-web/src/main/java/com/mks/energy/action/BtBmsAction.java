package com.mks.energy.action;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.mks.energy.core.ext.BaseAction;
import com.mks.energy.core.ext.IBaseService;
import com.mks.energy.entity.biz.BtBms;
import com.mks.energy.service.IBtBmsService;

/**
 * @author lifetime
 *
 */
@Controller
@RequestMapping("base/bms")
public class BtBmsAction  implements BaseAction<BtBms> {

	@Autowired
	private IBtBmsService service;

	@Override
	public IBaseService<BtBms> getService() {
		return service;
	}

}
