package com.mks.energy.action;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.mks.energy.core.ext.BaseAction;
import com.mks.energy.core.ext.IBaseService;
import com.mks.energy.entity.biz.BtRoleMenu;
import com.mks.energy.service.IBtRoleMenuService;

/**
 * @author lifetime
 *
 */
//@Controller
@RequestMapping("btrolemenu")
public class BtRoleMenuAction  implements BaseAction<BtRoleMenu> {

	@Autowired
	private IBtRoleMenuService service;
	
	@RequestMapping
	public String index() {
		return "btrolemenu";
	}

	@Override
	public IBaseService<BtRoleMenu> getService() {
		return service;
	}

}
