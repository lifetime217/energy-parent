package com.mks.energy.action;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mks.energy.entity.biz.BtDataAlert;
import com.mks.energy.entity.biz.BtProjectRuntime;
import com.mks.energy.service.IBtDataAlertService;
import com.mks.energy.service.IBtProjectRuntimeService;
import com.mks.energy.service.IProjectService;
import com.mks.energy.utils.Constant;
import com.mks.energy.utils.HttpResult;

/**
 * @author lifetime217
 *
 */
@Controller
@RequestMapping("monitor")
public class MonitorAction {
	private static final Logger log = LoggerFactory.getLogger(MonitorAction.class);

	@Autowired
	private IProjectService projectService;
	
	@Autowired
	private IBtProjectRuntimeService projectRuntimeService;
	
	@Autowired
	private IBtDataAlertService dataAlertService;

	@RequestMapping(value= {"/{projectId}","/"})
	public String index(Model m, @PathVariable(required = false) String projectId) {
		m.addAttribute("projects", projectService.findAll());
		m.addAttribute("defaultProjectShow", projectId);
		return "monitor/monitor";
	}

//	@GetMapping("project/{projectId}")
//	public String project(@PathVariable String projectId, Model m) {
//
//		return "monitor/monitor-project";
//	}

	@RequestMapping("project/tree/{projectId}")
	@ResponseBody
	public HttpResult projectTree(@PathVariable String projectId) {
		log.info("获取项目的结构树信息 参数 projectId:[{}] ",projectId);
		try {
			return HttpResult.success("成功", projectService.getTreeJsonString(projectId));
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return HttpResult.fail(e.getMessage());
		}
	}

	@GetMapping("site/{projectId}")
	public String site(@PathVariable String projectId, Model m) {
		log.info("获取项目实时信息.参数 projectId:[{}] ",projectId);
		//根据项目ID获取当天的项目信息.
		String dateStr = Constant.DateFormat1.format(new Date());
		Integer date = Integer.valueOf(dateStr);
		BtProjectRuntime projectRuntime = projectRuntimeService.getProjectRuntimeData(projectId,date);
		m.addAttribute("money", projectRuntime.getMoney());//今日收益
		m.addAttribute("money_total", projectRuntime.getMoneyTotal());//累计收益
		m.addAttribute("fill",projectRuntime.getFill() );//今日充电量
		m.addAttribute("fill_total", projectRuntime.getFillTotal());//累计充电量
		m.addAttribute("out", projectRuntime.getOut());//今日放电量
		m.addAttribute("out_total", projectRuntime.getOutTotal());//累计放电量
		m.addAttribute("safe_day", projectRuntime.getSafeDay());//安全运行天数
		m.addAttribute("sys_effic", projectRuntime.getSysEffic());//系统效率
		m.addAttribute("cap_v", projectRuntime.getCapV());//储能输出电压
		m.addAttribute("cap_a", projectRuntime.getCapA());//储能电池电流
		m.addAttribute("cap_kw",projectRuntime.getCapKw() );//储能功率
		return "monitor/monitor-site";
	}
	
	
	
	/**
	 * @author ghl
	 * @Date 2019年1月31日下午3:06:01
	 * @TODO 获取项目故障
	 * @param projectId
	 * @return
	 */
	@RequestMapping("/site/{projectId}/bug")
	@ResponseBody
	public HttpResult getProjectBug(@PathVariable("projectId")String projectId) {
		log.info("获取项目的故障信息.参数 projectId:[{}] ",projectId);
		try {
			List<BtDataAlert> bugs = dataAlertService.getBugByProjectId(projectId);
			if(bugs!=null &&bugs.size()>0 ) {
				return HttpResult.success("成功", bugs);
			}else {
				return HttpResult.success("成功",null);
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return HttpResult.fail("网络异常");
		}
		
		
	}

	@GetMapping("bms/{projectId}")
	public String bms(@PathVariable String projectId, Model m) {
		log.info("获取bms信息..参数 projectId:[{}] ",projectId);
		return "monitor/monitor-bms";
	}

	@GetMapping("pcs/{projectId}")
	public String pcs(@PathVariable String projectId, Model m) {
		log.info("获取pcs信息..参数 projectId:[{}] ",projectId);
		return "monitor/monitor-pcs";
	}

	@GetMapping("group/{projectId}")
	public String group(@PathVariable String projectId, Model m) {
		log.info("获取电池模组信息...参数 projectId:[{}] ",projectId);
		return "monitor/monitor-group";
	}

	@GetMapping("cell/{projectId}")
	public String cell(@PathVariable String projectId, Model m) {
		log.info("获取电池单体信息..参数 projectId:[{}] ",projectId);
		return "monitor/monitor-cell";
	}

}
