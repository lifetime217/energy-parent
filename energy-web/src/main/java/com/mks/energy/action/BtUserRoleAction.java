package com.mks.energy.action;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.mks.energy.core.ext.BaseAction;
import com.mks.energy.core.ext.IBaseService;
import com.mks.energy.entity.biz.BtUserRole;
import com.mks.energy.service.IBtUserRoleService;

/**
 * @author lifetime
 *
 */
//@Controller
@RequestMapping("btuserrole")
public class BtUserRoleAction  implements BaseAction<BtUserRole> {

	@Autowired
	private IBtUserRoleService service;
	
	@RequestMapping
	public String index() {
		return "btuserrole";
	}

	@Override
	public IBaseService<BtUserRole> getService() {
		return service;
	}

}
