package com.mks.energy.action;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.mks.energy.core.ext.BaseAction;
import com.mks.energy.core.ext.IBaseService;
import com.mks.energy.entity.biz.BtRoleReportType;
import com.mks.energy.service.IBtRoleReportTypeService;

/**
 * @author lifetime
 *
 */
//@Controller
@RequestMapping("btrolereporttype")
public class BtRoleReportTypeAction  implements BaseAction<BtRoleReportType> {

	@Autowired
	private IBtRoleReportTypeService service;
	
	@RequestMapping
	public String index() {
		return "btrolereporttype";
	}

	@Override
	public IBaseService<BtRoleReportType> getService() {
		return service;
	}

}
