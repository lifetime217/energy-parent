package com.mks.energy.action;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.mks.energy.core.ext.BaseAction;
import com.mks.energy.core.ext.IBaseService;
import com.mks.energy.entity.biz.BtPcs;
import com.mks.energy.service.IBtPcsService;

/**
 * @author lifetime
 *
 */
@Controller
@RequestMapping("base/pcs")
public class BtPcsAction  implements BaseAction<BtPcs> {

	@Autowired
	private IBtPcsService service;

	@Override
	public IBaseService<BtPcs> getService() {
		return service;
	}

}
