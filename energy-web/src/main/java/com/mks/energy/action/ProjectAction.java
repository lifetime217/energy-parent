package com.mks.energy.action;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.mks.energy.core.ext.BaseAction;
import com.mks.energy.core.ext.IBaseService;
import com.mks.energy.entity.biz.BtDataBms;
import com.mks.energy.entity.biz.BtDataBmsCellt;
import com.mks.energy.entity.biz.BtDataBmsCellv;
import com.mks.energy.entity.biz.BtDataPcs;
import com.mks.energy.entity.biz.BtProject;
import com.mks.energy.service.IProjectService;
import com.mks.energy.utils.ExcelUtil;
import com.mks.energy.utils.HttpResult;

/**
 * @author lifetime217
 *
 */
@Controller
@RequestMapping("/system/project")
public class ProjectAction implements BaseAction<BtProject> {
	private static final Logger logger = LoggerFactory.getLogger(UserAction.class);

	@Autowired
	private IProjectService projectService;

	@RequestMapping
	public String index() {
		return "project/project-list";
	}

	@RequestMapping("/view/{projectId}")
	public String edit(@PathVariable String projectId, Model m) {
		m.addAttribute("projectId", projectId);
		m.addAttribute("project", projectService.get(projectId));
		return "project/project-view";
	}

	@RequestMapping("/get/{id}")
	@ResponseBody
	public HttpResult get(@PathVariable String id) {
		return HttpResult.success(id, projectService.get(id));
	}

	
	@RequestMapping("/update/{id}")
	@ResponseBody
	public HttpResult updateProject(@PathVariable("id")String id , BtProject info) {
		if (StringUtils.isEmpty(info.getName())) {
			return HttpResult.fail("项目名称不能为空");
		}
		if (StringUtils.isEmpty(info.getLat())) {
			return HttpResult.fail("项目地址不能为空");
		}
		try {
			projectService.updatePart(info);
			return HttpResult.success("项目【" + info.getName() + "】修改成功 ");
		} catch (Exception e) {
			logger.error(e.getMessage(), e.fillInStackTrace());
			return HttpResult.fail(e.getMessage());
		}
	}
	
	@RequestMapping("/add")
	@ResponseBody
	public HttpResult add(BtProject info) {
		if (StringUtils.isEmpty(info.getName())) {
			return HttpResult.fail("项目名称不能为空");
		}
		if (StringUtils.isEmpty(info.getLat())) {
			return HttpResult.fail("项目地址不能为空");
		}
		try {
			projectService.add(info);
			return HttpResult.success("项目【" + info.getName() + "】添加成功 ");
		} catch (Exception e) {
			logger.error(e.getMessage(), e.fillInStackTrace());
			return HttpResult.fail(e.getMessage());
		}
	}

	@Override
	public IBaseService<BtProject> getService() {
		return projectService;
	}

	@PostMapping("/bmsimp/{projectId}")
	@ResponseBody
	public HttpResult bmsimp(@PathVariable String projectId, @RequestParam("file") MultipartFile file) {
		if (file == null || projectId == null) {
			return HttpResult.missParams();
		} else if (file.getOriginalFilename() == null || !file.getOriginalFilename().toLowerCase().endsWith(".xlsx")) {
			return HttpResult.fail("您上传的文件格式不正确，请先下载模板后导入！");
		} else {
			int line = -1;
			InputStream is = null;
			try {
				is = file.getInputStream();
				XSSFWorkbook wb = new XSSFWorkbook(is);//工作簿对象
				XSSFSheet sht = wb.getSheetAt(0);//第一个sheet
				String[] columns = getExcelColumns(sht.getRow(0));//获取BMS模板标题
				int rowCount = sht.getLastRowNum();//行数
				for (int i = 1; i < rowCount; i++) {
					line = i;
					XSSFRow row = sht.getRow(i);
					BtDataBms bmsData = getBmsData(columns, row, projectId);//获取BMS信息
					if (bmsData != null) {
						List<BtDataBmsCellt> listt = getCelltData(columns, row, projectId);
						List<BtDataBmsCellv> listv = getCellvData(columns, row, projectId);
						projectService.saveBmsData(bmsData, listt, listv);
					}
				}
				wb.close();
				return HttpResult.success("导入成功");
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				return HttpResult.fail("Excel第" + (line + 1) + "行导入失败：" + e.getMessage());
			} finally {
				if (is != null) {
					try {
						is.close();
					} catch (IOException e) {
					}
				}
			}
		}
	}

	protected String[] getExcelColumns(XSSFRow row) {
		int colCount = row.getPhysicalNumberOfCells();//获取不为空的列个数
		String[] ss = new String[colCount];
		for (int i = 0; i < ss.length; i++) {
			ss[i] = ExcelUtil.getCellValue(row.getCell(i));
		}
		return ss;
	}

	
	/**
	 * @author ghl
	 * @Date 2019年2月1日下午7:08:04
	 * @TODO 
	 * @param columns BMS模板标题
	 * @param row 模板每一行对象
	 * @param projectId 项目ID
	 * @return
	 */
	protected BtDataBms getBmsData(String[] columns, XSSFRow row, String projectId) {
		// 如果bms唯一码不存在，则忽略该记录
		String bmsSign = ExcelUtil.getCellValue(row.getCell(0));
		if (StringUtils.isEmpty(bmsSign)) {
			return null;
		}
		BtDataBms bms = new BtDataBms();
		for (int i = 0; i < columns.length; i++) {
			if (columns[i] != null && columns[i].toLowerCase().startsWith("cell_")) {
				// 如果是单体信息了，则直接跳出，说明bms数据已收集完毕
				break;
			}
			String cellVal = ExcelUtil.getCellValue(row.getCell(i));
			if ("BmsSign".equalsIgnoreCase(columns[i])) {//电池包的唯一标示符
				bms.setBmsSign(cellVal);
			} else if ("MaxCellVolt".equalsIgnoreCase(columns[i])) {//最高单体电
				bms.setMaxCellVolt(new BigDecimal(cellVal));
			} else if ("MinCellVolt".equalsIgnoreCase(columns[i])) {//最低单体电压
				bms.setMinCellVolt(new BigDecimal(cellVal));
			} else if ("MaxCellVoltNo".equalsIgnoreCase(columns[i])) {//最高单体电压序号
				bms.setMaxCellVoltNo(ExcelUtil.getCellIntValue(row.getCell(i)));
			} else if ("MinCellVoltNo".equalsIgnoreCase(columns[i])) {//最低单体电压序号
				bms.setMinCellVoltNo(ExcelUtil.getCellIntValue(row.getCell(i)));
			} else if ("MaxTemp".equalsIgnoreCase(columns[i])) {//最高温度
				bms.setMaxTemp(new BigDecimal(cellVal));
			} else if ("MinTemp".equalsIgnoreCase(columns[i])) {//最低温度
				bms.setMinTemp(new BigDecimal(cellVal));
			} else if ("MaxTempNo".equalsIgnoreCase(columns[i])) {//最高温度序号
				bms.setMaxTempNo(ExcelUtil.getCellIntValue(row.getCell(i)));
			} else if ("MinTempNo".equalsIgnoreCase(columns[i])) {//最低温度序号
				bms.setMinTempNo(ExcelUtil.getCellIntValue(row.getCell(i)));
			} else if ("BatSoc".equalsIgnoreCase(columns[i])) {//动力电池SOC
				bms.setBatSoc(new BigDecimal(cellVal));
			} else if ("BatSoh".equalsIgnoreCase(columns[i])) {//动力电池SOH
				bms.setBatSoh(ExcelUtil.getCellIntValue(row.getCell(i)));
			} else if ("BatVoltage".equalsIgnoreCase(columns[i])) {//动力电池总电压
				bms.setBatVoltage(new BigDecimal(cellVal));
			} else if ("BatCurrent".equalsIgnoreCase(columns[i])) {//动力电池总电流
				bms.setBatCurrent(new BigDecimal(cellVal));
			}
		}
		bms.setProjectId(projectId);
		return bms;
	}

	/**
	 * 获取单体温度信息
	 * 
	 * @param columns BMS模板标题
	 * @param row 模板每一行对象
	 * @param projectId 项目ID
	 * @return
	 */
	protected List<BtDataBmsCellt> getCelltData(String[] columns, XSSFRow row, String projectId) {
		List<BtDataBmsCellt> items = new ArrayList<>();
		for (int i = 0; i < columns.length; i++) {
			if (columns[i] != null && columns[i].toLowerCase().startsWith("cell_temp_")) {
				BtDataBmsCellt bmsCellt = new BtDataBmsCellt();
				String cellVal = ExcelUtil.getCellValue(row.getCell(i));
				if (!StringUtils.isEmpty(cellVal)) {
					bmsCellt.setTemp(new BigDecimal(cellVal));
					int seq = Integer.parseInt(columns[i].substring(10));
					bmsCellt.setSeq(seq);
					items.add(bmsCellt);
				}
			}
		}
		return items;
	}

	/**
	 * 获取单体电压信息
	 * 
	 * @param columns BMS模板标题
	 * @param row 模板每一行对象
	 * @param projectId 项目ID
	 * @return
	 */
	protected List<BtDataBmsCellv> getCellvData(String[] columns, XSSFRow row, String projectId) {
		List<BtDataBmsCellv> items = new ArrayList<>();
		for (int i = 0; i < columns.length; i++) {
			if (columns[i] != null && columns[i].toLowerCase().startsWith("cell_volt_")) {
				BtDataBmsCellv bmsCellv = new BtDataBmsCellv();
				String cellVal = ExcelUtil.getCellValue(row.getCell(i));
				if (!StringUtils.isEmpty(cellVal)) {
					bmsCellv.setVolt(new BigDecimal(cellVal));
					int seq = Integer.parseInt(columns[i].substring(10));
					bmsCellv.setSeq(seq);

					items.add(bmsCellv);
				}
			}
		}
		return items;
	}

	@PostMapping("/pcsimp/{projectId}")
	@ResponseBody
	public HttpResult pcsimp(@PathVariable String projectId, @RequestParam("file") MultipartFile file) {
		if (file == null || projectId == null) {
			return HttpResult.missParams();
		} else if (file.getOriginalFilename() == null || !file.getOriginalFilename().endsWith(".xlsx")) {
			return HttpResult.fail("您上传的文件格式不正确，请先下载模板后导入！");
		} else {
			InputStream is = null;
			int line = -1;
			try {
				is = file.getInputStream();
				XSSFWorkbook wb = new XSSFWorkbook(is);
				XSSFSheet sht = wb.getSheetAt(0);
				String[] columns = getExcelColumns(sht.getRow(0));
				int rowCount = sht.getLastRowNum();
				for (int i = 1; i < rowCount; i++) {
					line = i;
					XSSFRow row = sht.getRow(i);
					BtDataPcs bmsData = getPcsData(columns, row, projectId);
					if (bmsData != null) {
						projectService.savePcsData(bmsData);
					}
				}
				wb.close();
				return HttpResult.success("导入成功");
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				return HttpResult.fail("Excel第" + (line + 1) + "行导入失败：" + e.getMessage());
			} finally {
				if (is != null) {
					try {
						is.close();
					} catch (IOException e) {
					}
				}
			}
		}
	}

	protected BtDataPcs getPcsData(String[] columns, XSSFRow row, String projectId) {
		BtDataPcs bms = new BtDataPcs();
		for (int i = 0; i < columns.length; i++) {
			bms.set(columns[i], ExcelUtil.getCellValue(row.getCell(i)));
		}
		bms.setProjectId(projectId);
		return bms;
	}

}
