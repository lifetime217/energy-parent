package com.mks.energy.action;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.mks.energy.entity.biz.BtBms;
import com.mks.energy.entity.biz.BtPcs;
import com.mks.energy.entity.biz.BtProject;
import com.mks.energy.entity.biz.BtProjectDevicegroupBmsItems;
import com.mks.energy.service.IProjectService;
import com.mks.energy.utils.HttpResult;

/**
 * @author lifetime
 *
 */
@Controller
@RequestMapping("/system/project/device")
public class ProjectDeviceAction {
	private static final Logger log = LoggerFactory.getLogger(ProjectDeviceAction.class);

	@Autowired
	private IProjectService projectService;

	@RequestMapping("/{projectId}")
	public String index(@PathVariable String projectId, Model m) {
		//获取项目名称
		BtProject project = projectService.get(projectId);
		if(project!=null) {
			m.addAttribute("projectName", project.getName());
		}
		m.addAttribute("projectId", projectId);
		m.addAttribute("treeString", projectService.getTreeJsonString(projectId));
		return "project/project-device";
	}

	@RequestMapping("/tree/{projectId}")
	@ResponseBody
	public HttpResult getTreeJsonString(@PathVariable String projectId) {
		try {
			return HttpResult.success("成功", projectService.getTreeJsonString(projectId));
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return HttpResult.fail(e.getMessage());
		}
	}

	/**
	 * 删除项目的设备组
	 * 
	 * @param id
	 * @param m
	 * @return
	 */
	@RequestMapping("/del/{id}")
	@ResponseBody
	public HttpResult delDeviceGroup(@PathVariable String id, Model m) {
		try {
			projectService.deleteDeviceGroupInfo(id);
			return HttpResult.success("删除成功");
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return HttpResult.fail(e.getMessage());
		}
	}

	/**
	 * 项目的设备模板导入
	 * 
	 * @param projectId
	 * @param file
	 * @return
	 */
	@RequestMapping("/imp/{projectId}")
	@ResponseBody
	public HttpResult imp(@PathVariable String projectId, @RequestParam("file") MultipartFile file) {
		if (file == null || projectId == null) {
			return HttpResult.missParams();
		} else if (file.getOriginalFilename() == null || !file.getOriginalFilename().endsWith(".xlsx")) {
			return HttpResult.fail("您上传的文件格式不正确，请先下载模板后导入！");
		} else {
			InputStream is = null;
			try {
				is = file.getInputStream();
				XSSFWorkbook wb = new XSSFWorkbook(is);
				int sheetCount = wb.getNumberOfSheets();
				for (int i = 0; i < sheetCount; i++) {
					XSSFSheet sht = wb.getSheetAt(i);
					String groupName = sht.getSheetName();
					BtPcs pcs = getPcs(sht);
					List<BtBms> bmsList = getBms(sht);
					List<BtProjectDevicegroupBmsItems> bmsItems = getBmsItems(sht);

					projectService.saveDeviceGroupInfo(projectId, groupName, pcs, pcs.getString("sign"),
							bmsList.toArray(new BtBms[bmsList.size()]), getBmsSigns(bmsList),
							bmsItems.toArray(new BtProjectDevicegroupBmsItems[bmsItems.size()]));
				}
				wb.close();
				return HttpResult.success("导入成功");
			} catch (Exception e) {
				log.error(e.getMessage(), e);
				return HttpResult.fail(e.getMessage());
			} finally {
				if (is != null) {
					try {
						is.close();
					} catch (IOException e) {
					}
				}
			}
		}
	}

	protected String[] getBmsSigns(List<BtBms> bmsList) {
		HashSet<String> signs = new HashSet<>();
		for (int i = 0; i < bmsList.size(); i++) {
			signs.add(bmsList.get(i).getString("sign"));
		}
		return signs.toArray(new String[signs.size()]);
	}

	protected List<BtBms> getBms(XSSFSheet sht) {
		List<BtBms> bmsList = new ArrayList<>();
		int rowCount = sht.getLastRowNum();
		List<Integer> bmsIndexs = new ArrayList<>();
		for (int i = 0; i < rowCount; i++) {
			XSSFRow row = sht.getRow(i);
			if (row == null) {
				continue;
			}
			String cellStr = getCellValue(row.getCell(0));
			if (cellStr != null && "BMS".equalsIgnoreCase(cellStr)) {
				bmsIndexs.add(i + 2);
			} else if (cellStr != null && "BatteryItem".equalsIgnoreCase(cellStr)) {
				break;
			}
		}

		for (int i = 0; i < bmsIndexs.size(); i++) {
			XSSFRow bmsRow = sht.getRow(bmsIndexs.get(i));
			if (bmsRow == null) {
				continue;
			}
			BtBms bms = new BtBms();
			bms.set("sign", getCellValue(bmsRow.getCell(0)));
			bms.setBatteryFactory(getCellValue(bmsRow.getCell(1)));
			bms.setCellFactory(getCellValue(bmsRow.getCell(2)));
			bms.setBatteryType(getIntegerCellValue(bmsRow.getCell(3)));
			bms.setBatteryShape(getIntegerCellValue(bmsRow.getCell(4)));
			bms.setBatteryGroupV(getBigdecimalCellValue(bmsRow.getCell(5)));
			bms.setBatteryGroupA(getBigdecimalCellValue(bmsRow.getCell(6)));
			bms.setBatteryV(getBigdecimalCellValue(bmsRow.getCell(7)));
			bms.setBatteryA(getBigdecimalCellValue(bmsRow.getCell(8)));
			bms.setMaxOutputA(getBigdecimalCellValue(bmsRow.getCell(9)));
			bms.setStandardA(getBigdecimalCellValue(bmsRow.getCell(10)));
			bms.setBatteryGroupJointype(getCellValue(bmsRow.getCell(11)));
			bms.setBatteryGroupMzCount(getIntegerCellValue(bmsRow.getCell(12)));
			bms.setBmsFactory(getCellValue(bmsRow.getCell(13)));
			bms.setBmsSoftVersion(getCellValue(bmsRow.getCell(14)));
			bms.setBmsHardVersion(getCellValue(bmsRow.getCell(15)));
			bms.setBmsBalancedType(getIntegerCellValue(bmsRow.getCell(16)));
			bms.setBmsMaxBalancedA(getBigdecimalCellValue(bmsRow.getCell(17)));
			bmsList.add(bms);
		}

		return bmsList;
	}

	protected BtPcs getPcs(XSSFSheet sht) {
		int rowCount = sht.getLastRowNum();
		int pcsIndex = -1;
		for (int i = 0; i < rowCount; i++) {
			XSSFRow row = sht.getRow(i);
			if (row == null) {
				continue;
			}
			XSSFCell cell = row.getCell(0);
			String cellStr = getCellValue(cell);
			if (cellStr != null && "PCS".equalsIgnoreCase(cellStr)) {
				pcsIndex = i + 2;
				break;
			}
		}
		if (pcsIndex != -1) {
			XSSFRow row = sht.getRow(pcsIndex);
			if (row == null) {
				return null;
			}
			BtPcs pcs = new BtPcs();
			pcs.set("sign", getCellValue(row.getCell(0)));
			pcs.setPcsFactory(getCellValue(row.getCell(1)));
			pcs.setPcsModel(getCellValue(row.getCell(2)));
			pcs.setPowerRating(getCellValue(row.getCell(3)));
			pcs.setMainsVoltage(getCellValue(row.getCell(4)));
			pcs.setDcLinkVoltage(getCellValue(row.getCell(5)));
			pcs.setMaxInputPower(getCellValue(row.getCell(6)));
			pcs.setMainsHz(getCellValue(row.getCell(7)));
			pcs.setMaxCapacity(getCellValue(row.getCell(8)));
			pcs.setMaxOutputPower(getCellValue(row.getCell(9)));
			return pcs;
		}
		return null;
	}

	protected List<BtProjectDevicegroupBmsItems> getBmsItems(XSSFSheet sht) {
		int rowCount = sht.getLastRowNum();
		int pcsIndex = -1;
		for (int i = 0; i < rowCount; i++) {
			XSSFRow row = sht.getRow(i);
			if (row == null) {
				continue;
			}
			XSSFCell cell = row.getCell(0);
			String cellStr = getCellValue(cell);
			if (cellStr != null && "BatteryItem".equalsIgnoreCase(cellStr)) {
				pcsIndex = i + 2;
				break;
			}
		}
		if (pcsIndex != -1) {
			List<BtProjectDevicegroupBmsItems> bmsItems = new ArrayList<BtProjectDevicegroupBmsItems>();
			for (int i = pcsIndex; i < rowCount; i++) {
				XSSFRow row = sht.getRow(i);
				if (row == null) {
					continue;
				}
				BtProjectDevicegroupBmsItems bmsItemInfo = new BtProjectDevicegroupBmsItems();
				bmsItemInfo.setSign(getCellValue(row.getCell(0)));
				bmsItemInfo.setName(getCellValue(row.getCell(1)));
				bmsItemInfo.setBgroupName(getCellValue(row.getCell(2)));
				bmsItemInfo.set("bms_sign", getCellValue(row.getCell(3)));
				bmsItems.add(bmsItemInfo);
			}
			return bmsItems;
		}
		return null;
	}

	Integer getIntegerCellValue(XSSFCell cell) {
		String val = getCellValue(cell);
		if (val != null) {
			if (val.indexOf(".") != -1) {
				return Double.valueOf(val).intValue();
			}
			return Integer.valueOf(val);
		}
		return null;
	}

	BigDecimal getBigdecimalCellValue(XSSFCell cell) {
		String val = getCellValue(cell);
		if (val != null) {
			return new BigDecimal(val);
		}
		return null;
	}

	String getCellValue(XSSFCell cell) {
		if (cell != null) {
			if (CellType.STRING.compareTo(cell.getCellType()) == 0) {
				XSSFRichTextString cellStr = cell.getRichStringCellValue();
				if (cellStr != null) {
					return cellStr.getString();
				}
			} else if (CellType.NUMERIC.compareTo(cell.getCellType()) == 0) {
				return String.valueOf(cell.getNumericCellValue());
			} else if (CellType.FORMULA.compareTo(cell.getCellType()) == 0) {
				return cell.getCellFormula();
			}
		}
		return null;
	}
}
