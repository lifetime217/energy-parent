/** 
 * Project Name:energy-web 
 * File Name:EventTracingAction.java 
 * Package Name:com.mks.energy.action 
 * Date:2018年12月23日上午3:30:26 
 * Copyright (c) 2018, Luoran, Inc. All Rights Reserved.
 * 
 */  
package com.mks.energy.action;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/** 
 * @author ghl 
 * @since JDK 1.8
 * ClassName: EventTracingAction
 * date: 2018年12月23日 上午3:30:26
 *	事件追溯
 */
@Controller
@RequestMapping("event/trace")
public class EventTracingAction {

	
	
}
