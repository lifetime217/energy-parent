package com.mks.energy.action;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.mks.energy.core.ext.BaseAction;
import com.mks.energy.core.ext.IBaseService;
import com.mks.energy.entity.biz.BtDiagnosticAnalyze;
import com.mks.energy.service.IBtDiagnosticAnalyzeService;

/**
 * @author lifetime
 *
 */
@Controller
@RequestMapping("diagnostic/analyze")
public class BtDiagnosticAnalyzeAction  implements BaseAction<BtDiagnosticAnalyze> {

	@Autowired
	private IBtDiagnosticAnalyzeService service;
	
	@RequestMapping
	public String index() {
		return "diagnostic/analyze-list";
	}
	
	@RequestMapping("report/{reportId}")
	public String report(@PathVariable String reportId,Model m) {
		m.addAttribute("reportId", reportId);
		return "diagnostic/report";
	}

	@Override
	public IBaseService<BtDiagnosticAnalyze> getService() {
		return service;
	}

}
