package com.mks.energy.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.mks.energy.core.ext.BaseAction;
import com.mks.energy.core.ext.IBaseService;
import com.mks.energy.entity.biz.BtProjectElectrictable;
import com.mks.energy.service.IProjectElectrictableService;

/**
 * @author lifetime
 *
 */
@Controller
@RequestMapping("/system/electrictable")
public class ProjectElectrictableAction  implements BaseAction<BtProjectElectrictable> {
	static final Logger logger = LoggerFactory.getLogger(ProjectElectrictableAction.class);
	
	@Autowired
	private IProjectElectrictableService projectElectrictableService;
	
	@RequestMapping("/{projectId}")
	public String electrictable(@PathVariable String projectId,Model m) {
		m.addAttribute("projectId", projectId);
		return "project/project-electrictable";
	}
	
	@Override
	public IBaseService<BtProjectElectrictable> getService() {
		return projectElectrictableService;
	}

}
