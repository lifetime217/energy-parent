package com.mks.energy.action;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/workbench")
public class WorkbenchAction {

	@RequestMapping
	public String success(Model model) {
		return "index";
	}

}
