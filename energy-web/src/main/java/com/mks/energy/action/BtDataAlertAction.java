package com.mks.energy.action;

import java.security.Principal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.mks.energy.core.ext.BaseAction;
import com.mks.energy.core.ext.IBaseService;
import com.mks.energy.entity.biz.BtDataAlert;
import com.mks.energy.service.IBtDataAlertService;
import com.mks.energy.service.IProjectService;
import com.mks.energy.utils.HttpResult;

/**
 * @author lifetime
 *
 */
@Controller
@RequestMapping("alert")
public class BtDataAlertAction  implements BaseAction<BtDataAlert> {
	
	private static final Logger logger = LoggerFactory.getLogger(BtDataAlertAction.class);

	@Autowired
	private IProjectService projectService;
	
	@Autowired
	private IBtDataAlertService dataAlertservice;
	
	@GetMapping
	public String index(Model m) {
		m.addAttribute("projects", projectService.findAll());
		return "alert/alert-list";
	}
	
	@PostMapping("all")
	@ResponseBody
	public HttpResult all(){
		return HttpResult.success("success", dataAlertservice.findAll());
	}

	@Override
	public IBaseService<BtDataAlert> getService() {
		return dataAlertservice;
	}
	
	@RequestMapping("handle/{id}")
	@ResponseBody
	public HttpResult handleAlert(@PathVariable("id")String id) {
		JSONObject obj = new JSONObject();
		try {
			BtDataAlert dataAlert = dataAlertservice.get(id);
			if(dataAlert!=null) {
				obj.put("handleCount", dataAlert.getHandleCount());
				obj.put("detail", dataAlert.getAlertHandleDetail());
				obj.put("alertId", dataAlert.getId());
			}
			return HttpResult.success("成功",obj);
		} catch (Exception e) {
			logger.error(e.getMessage(),e.fillInStackTrace());
			return HttpResult.fail("网络异常");
		}
		
	}
	
	
	@RequestMapping("handle/save/{id}")
	@ResponseBody
	public HttpResult saveHandleInfo(@PathVariable("id")String id,@RequestParam("handleInfo")String handleInfo) {
		try {
			if(StringUtils.isEmpty(handleInfo)) {
				return HttpResult.fail("请填写相关处理信息");
			}
			BtDataAlert dataAlert = dataAlertservice.get(id);
			if(dataAlert!=null) {
				dataAlert.setHandleCount(dataAlert.getHandleCount()+1);
				dataAlert.setAlertHandleIdea(handleInfo);
				if(StringUtils.isEmpty(dataAlert.getAlertHandleDetail())) {
					dataAlert.setAlertHandleDetail("第"+dataAlert.getHandleCount()+"次处理记录【"+handleInfo+"】>>>>>");
				}else {
					dataAlert.setAlertHandleDetail(dataAlert.getAlertHandleDetail()+"第"+dataAlert.getHandleCount()+"次处理记录【"+handleInfo+"】>>>>>");
				}
				dataAlertservice.updatePart(dataAlert);//更新
			}
			return HttpResult.success("处理完成");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage(),e.fillInStackTrace());
			return HttpResult.fail("网络异常");
		}
		
	}

}
