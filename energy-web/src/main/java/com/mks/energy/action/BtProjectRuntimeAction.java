package com.mks.energy.action;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.mks.energy.core.ext.BaseAction;
import com.mks.energy.core.ext.IBaseService;
import com.mks.energy.entity.biz.BtProjectRuntime;
import com.mks.energy.service.IBtProjectRuntimeService;

/**
 * @author lifetime
 *
 */
@Controller
@RequestMapping("btprojectruntime")
public class BtProjectRuntimeAction  implements BaseAction<BtProjectRuntime> {

	@Autowired
	private IBtProjectRuntimeService service;
	
	@RequestMapping
	public String index() {
		return "btprojectruntime";
	}

	@Override
	public IBaseService<BtProjectRuntime> getService() {
		return service;
	}

}
