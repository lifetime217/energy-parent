package com.mks.energy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * @author lifetime
 *
 */
@SpringBootApplication
public class energyWebStart {

	public static void main(String[] args) {
		ConfigurableApplicationContext ctx = SpringApplication.run(energyWebStart.class, args);
		String[] activeProfiles = ctx.getEnvironment().getActiveProfiles();
		for (String profile : activeProfiles) {
			System.out.println("========【energy - Web】========= Spring Boot 使用profile为:" + profile);
		}
	}
}
