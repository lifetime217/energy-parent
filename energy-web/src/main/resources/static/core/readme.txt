author : lifetime217@163.com

F4的使用规则
	1：前端页面
		<input class="f4" type="text" name="test" id="testF4" table="user" displayFormat="userName" urlParams='{"aaa":"111","bbb":222}'/>
	2：获取f4的值
		$('#testF4').f4().getValue();   ===>    {id: "10", userName: "Cyril ", userNumber: "Cyr51target.com"}
	3：参数介绍
		name【可选】，在表单里时需包含name用于提交表单。
		displayFormat【必填】，用于显示默认展示的f4的值的字符串形式。
		commitFormat【可选，默认为“id”】，作为表单字段时为【必填】，表单提交时该commitFormat的属性值会随表单提交。如果该值为“_objValue_”则提交完整的json字符串值。
		value【可选】，做初始化值使用，初始化的值必须为json格式的对象值。 ★★★★★★注意value在html中需使用单引号★★★★★★
		isEditable【可选】，用来标示F4控件是否可以编辑
	4：F4值改变事件
		$("#shop_name").f4().setChangedListener(function(oldVal,newVal){
	       alert(newVal);
	       //return false;  则代表此次值改变事件失效，F4的值不会改变。
	    });	
	5：F4其他方法说明
		setEditable(isEdit) ： 是否可以编辑
		setValue(json) ： 设置F4的json值
		getValue() : 获取F4的json值
		getDisplayFormat() : 获取默认显示的属性值
		getCommitFormat() : 获取提交时的属性值, 若值为"_objValue_"则F4会提交整个值
		
	6：框架后台需要添加的内容
		f4.xml 里需添加table对应的两个查询（见例子）。
		F4Mapper.java 里需要对应添加两个方法（见例子）。
		f4-column-alias.properties里添加对前台table列的定义和描述（见例子）。

F4Tree的使用
	1：引入必要的文件["metroStyle.css","jquery.ztree.all.min.js","plugin-f4tree.js"]
	2：前端页面
		<input class="f4tree" id="testTree1" name="tree1" title="组织多选" data-multiSelect="true" value='${val!}'/>
	3：参数介绍
		name【可选】，在表单里时需包含name用于提交表单。
		title【可选，默认为 "选择数据" 】 树控件的标题。
		displayFormat【必填，默认为 "name"】，用于显示默认展示的f4的值的字符串形式。
		commitFormat【可选，默认为“id”】，作为表单字段时为【必填】，表单提交时该commitFormat的属性值会随表单提交。如果该值为“_objValue_”则提交完整的json字符串值。
		value【可选★★注意value在html中需使用单引号★★】，做初始化值使用，初始化的值必须为json格式的对象值。 
	4：F4Tree值改变事件【监听需放在dom加载完毕之后进行】
		$(document).ready(function(){
			$('#testTree2').on('valueChanged',function(event,oldData,newData){
				
			});
		});