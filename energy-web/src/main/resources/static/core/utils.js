/**
 * 用于原生html table某列的行合并
 * 用法：$('#table').rowMerge(0); //对table的第1列做行合并
 * 
 * @author lifetime
 */
(function($){
	$.fn.rowMerge = function(colIndex){
        var tmpObj = {"text":"","start":0,"end":0};
        var handMerge = function(text,start,end){
            if(start == end){
                return;
            }
            this.find('tbody>tr:eq(' + start + ')>td:eq(' + colIndex + ')').attr('rowspan',end - start + 1);
            for(var i = start + 1;i <= end;i++){
                this.find('tbody>tr:eq(' + i + ')>td:eq(' + colIndex + ')').remove();
            }
        };
        var tds = this.find('tbody>tr>td:nth-child(' + (colIndex + 1) + ')');
        var that = this;
        tds.each(function(index,td){
            if(index == 0){
                tmpObj.text = $(td).text();
            }else{
                if($(td).text() == tmpObj.text){
                    tmpObj.end = index;
                }else{
                    tmpObj.end = index - 1;
                    handMerge.call(that,tmpObj.text,tmpObj.start,tmpObj.end);
                    tmpObj.start = index;
                    tmpObj.text = $(td).text();
                    tmpObj.end = index;
                }
            }

            if(index == tds.length - 1){
                handMerge.call(that,tmpObj.text,tmpObj.start,index);
            }
        });
    }
})(jQuery);


function formatDateTime(timeStamp) { 
    var date = new Date();
    date.setTime(timeStamp);
    var y = date.getFullYear();    
    var m = date.getMonth() + 1;    
    m = m < 10 ? ('0' + m) : m;    
    var d = date.getDate();    
    d = d < 10 ? ('0' + d) : d;    
    var h = date.getHours();  
    h = h < 10 ? ('0' + h) : h;  
    var minute = date.getMinutes();  
    var second = date.getSeconds();  
    minute = minute < 10 ? ('0' + minute) : minute;    
    second = second < 10 ? ('0' + second) : second;   
    return y + '-' + m + '-' + d+' '+h+':'+minute+':'+second;    
}

function formatDate(obj){
    var date =  new Date(obj);
    var y = 1900+date.getYear();
    var m = "0"+(date.getMonth()+1);
    var d = "0"+date.getDate();
    return y+"-"+m.substring(m.length-2,m.length)+"-"+d.substring(d.length-2,d.length);
}