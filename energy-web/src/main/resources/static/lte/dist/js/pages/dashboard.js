/*
 * Author: Abdullah A Almsaeed
 * Date: 4 Jan 2014
 * Description:
 *      This is a demo file used only for the main dashboard (index.html)
 **/

$(function () {

  'use strict';

  // Make the dashboard widgets sortable Using jquery UI
  $('.connectedSortable').sortable({
    placeholder         : 'sort-highlight',
    connectWith         : '.connectedSortable',
    handle              : '.box-header, .nav-tabs',
    forcePlaceholderSize: true,
    zIndex              : 999999
  });
  $('.connectedSortable .box-header, .connectedSortable .nav-tabs-custom').css('cursor', 'move');

  // jQuery UI sortable for the todo list
  $('.todo-list').sortable({
    placeholder         : 'sort-highlight',
    handle              : '.handle',
    forcePlaceholderSize: true,
    zIndex              : 999999
  });


  $('.todo-list').todoList({
    onCheck  : function () {
      window.console.log($(this), 'The element has been checked');
    },
    onUnCheck: function () {
      window.console.log($(this), 'The element has been unchecked');
    }
  });
  
  var myChart = echarts.init(document.getElementById("pieChart"),'macarons');
  var option = {
      tooltip: {
          trigger: 'item',
          formatter: "{a} <br/>{b}: {c}"
      },
      legend: {
          orient: 'vertical',
          x: 'left',
          data:['充电','放电']
      },
      series: [
          {
              name:'充放电占比',
              type:'pie',
              radius: ['50%', '70%'],
              avoidLabelOverlap: false,
              label: {
                  normal: {
                      show: false,
                      position: 'center'
                  },
                  emphasis: {
                      show: true
                  }
              },
              labelLine: {
                  normal: {
                      show: false
                  }
              },
              data:[
                  {value:335, name:'充电'},
                  {value:310, name:'放电'}
              ]
          }
      ]
  };
  
  myChart.setOption(option);
  
//百度地图API功能	
	var map = new BMap.Map("allmap");
	var top_left_control = new BMap.ScaleControl({anchor: BMAP_ANCHOR_TOP_LEFT});// 左上角，添加比例尺
	var top_left_navigation = new BMap.NavigationControl();  //左上角，添加默认缩放平移控件
	var top_right_navigation = new BMap.NavigationControl({anchor: BMAP_ANCHOR_TOP_RIGHT, type: BMAP_NAVIGATION_CONTROL_SMALL}); //右上角，仅包含平移和缩放按钮
	/*缩放控件type有四种类型:
	BMAP_NAVIGATION_CONTROL_SMALL：仅包含平移和缩放按钮；BMAP_NAVIGATION_CONTROL_PAN:仅包含平移按钮；BMAP_NAVIGATION_CONTROL_ZOOM：仅包含缩放按钮*/
	map.addControl(top_left_control);        
	map.addControl(top_left_navigation);     
	map.addControl(top_right_navigation); 
	
	map.centerAndZoom(new BMap.Point(116.417854,39.921988), 15);
	map.enableScrollWheelZoom();
	var url = $('body') + "/system/project/view/5d4caf9206794dbb9ef190472ab91be4";
	var ctn1 = "<div style='border: 1px solid gainsboro;border-radius: 5px;margin-top: 10px;'>" +
			"<a href='" + url + "' style='font-size: 16px;display: block;background-color: gainsboro;padding: 5px;'>万达广场</a><table class='table'><tbody>" +
			"<tr><td>总存储能量</td><td style='text-align: right;'>3283KWH</td></tr>" +
			"<tr><td>安全运行天数</td><td style='text-align: right;'>388天</td></tr>" +
			"<tr><td>总电量</td><td style='text-align: right;'>3748Ah</td></tr>" +
			"<tr><td>故障数</td><td style='text-align: right;'>3,1,0</td></tr>" +
			"<tr><td>联系人</td><td style='text-align: right;'>张经理</td></tr>" +
			"<tr><td>联系方式</td><td style='text-align: right;'>13721314123</td></tr>" +
			"</tbody></table></div>";
	var myGeo = new BMap.Geocoder();
	var opts = {
		width : 250, // 信息窗口宽度
		height : 310, // 信息窗口高度
		title : "信息窗口", // 信息窗口标题
		enableMessage : true
	};
	myGeo.getPoint("上海宝山万达", function(point){
		if (point) {
			map.centerAndZoom(point, 16);
			var data_info = [[121.453517,31.33011,ctn1], [ 121.454022, 31.325454, ctn1 ], [ 121.451192, 31.332107, ctn1 ]];
			
			for(var i=0;i<data_info.length;i++){
				var marker = new BMap.Marker(new BMap.Point(data_info[i][0],data_info[i][1]));  // 创建标注
				var content = data_info[i][2];
				map.addOverlay(marker);               // 将标注添加到地图中
				addClickHandler(content,marker);
			}
		}else{
			alert("您选择地址没有解析到结果!");
		}
	}, "上海市");
	
	function addClickHandler(content,marker){
		marker.addEventListener("click",function(e){
			openInfo(content,e)}
		);
	}
	function openInfo(content,e){
		var p = e.target;
		var point = new BMap.Point(p.getPosition().lng, p.getPosition().lat);
		var infoWindow = new BMap.InfoWindow(content,opts);  // 创建信息窗口对象 
		map.openInfoWindow(infoWindow,point); //开启信息窗口
	}
});
