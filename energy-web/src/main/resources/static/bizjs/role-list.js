var ctxPath = $('body').attr('ctx');
function getOpBtns() {
	return [ {
		css : "view",
		text : "编辑"
	}, {
		css : "delete",
		text : "删除"
	}, {
		css : "menu",
		text : "分配权限"
	} ];
}

$(function() {
	var addDlgIndex;
	var menuDigIndex;
	$('#add').click(function() {
		$('#role_id').val('');
		$('#role_name').val('');
		$('#role_description').val('');
		$('#add_role_dlg_msg').empty();
		$('#add_role_submit').show();
		$('#update_role').hide();
		addDlgIndex = layer.open({
			title : "新增角色",
			type : 1,
			area : [ '300px', '270px' ], // 宽高
			content : $('#add_role_dlg')
		});
	});
	
	$('#add_role_submit').click(function(){
		$('#add_role_dlg_msg').text('');
		$.getJSON($("#add_role_form").attr('action'), $("#add_role_form").serialize()).done(function(data) {
			if(data.statusCode == 200){
				layer.alert(data.msg);
				if(addDlgIndex){
					layer.close(addDlgIndex);
				}
				listui.refresh();
			}else{
				$('#add_role_dlg_msg').text(data.msg);
			}
		}).fail(function(err){
			layer.alert("网络异常");
		});
		return false;
	});
	
	
	$('.list-content-div table>tbody').delegate('tr>td>.view', 'click', function() {
		var id = $(this).data("id");
		if (id) {
			$.ajax({
				url:ctxPath+"/system/role/view/"+id+"?_t="+new Date().getTime(),
				type:'POST',
				dataType:'json',
				success:function(data){
					if(data.statusCode==200){
						$('#role_id').val(data.data.id);
						$('#role_name').val(data.data.name);
						$('#role_description').val(data.data.description);
						$('#update_role').show();
						$('#add_role_submit').hide();
						$('#add_role_dlg_msg').empty();
						addDlgIndex = layer.open({
							title : "修改角色",
							type : 1,
							area : [ '300px', '270px' ], // 宽高
							content : $('#add_role_dlg')
						});
					}else{
						layer.alert("网络异常");
					}
				},
				error:function(errMsg){
					layer.alert("网络异常");
				}
			});
		}
	});
	
	//点击修改保存按钮
	$('#update_role').on('click',function(){
		$.ajax({
			url:ctxPath+"/system/role/update?_t="+new Date().getTime(),
			type:'post',
			data:$("#add_role_form").serialize(),
			dataType:'json',
			success:function(data){
				if(data.statusCode==200){
					layer.alert(data.msg);
					if(addDlgIndex){
						layer.close(addDlgIndex);
					}
					listui.refresh();
				}
			},
			error:function(errMsg){
				layer.alert("网络异常");
			}
		});
	})
	
	//删除
	$('.list-content-div table>tbody').delegate('tr>td>.delete', 'click', function() {
		var id = $(this).data("id");
		if (id) {
			layer.confirm('确定删除吗？', {
				  btn: ['确认','取消'] // 按钮
				}, function(){
					$.ajax({
						url:ctxPath+"/system/role/del/"+id+"?_t="+new Date().getTime(),
						type:'post',
						dataType:'json',
						success:function(data){
							if(data.statusCode==200){
								layer.msg(data.msg);
								listui.refresh();
							}
						},
						error:function(errMsg){
							layer.alert("网络异常");
						}
					});
				})
		}else{
			layer.alert("网络异常,请联系管理员");
		}
	});
	
	
	//点击分配权限
	$('.list-content-div table>tbody').delegate('tr>td>.menu', 'click', function() {
		var id = $(this).data("id");
		$('#this_role_id').val(id);
		//获取相应用户的菜单树
		$.ajax({
			url:ctxPath +'/system/role/getPermission/'+id+'?_t='+new Date().getTime(),
			type:'POST',
			dataType:'json',
			success:function(data){
				if(data.statusCode==200){
					menuDigIndex = layer.open({
						title : "分配权限",
						type : 1,
						area : [ '550px', '450px' ], // 宽高
						content : $('#add_permission_dlg')
					});
					//菜单树
					var menuTreeData=data.data.menuTreeData;
					$.fn.zTree.init($("#menuTree"),setting,menuTreeData);
					var menuTreeObj = $.fn.zTree.getZTreeObj("menuTree");
					menuTreeObj.expandAll(true);
					
					//报告树
					var reportTreeData=data.data.reportTreeData;
					$.fn.zTree.init($("#reportViewTree"),settingReportType,reportTreeData);
					var reportTreeObj = $.fn.zTree.getZTreeObj("reportViewTree");
					reportTreeObj.expandAll(true);
					
				}else{
					layer.alert(data.msg);
				}
			},
			error:function(errMsg){
				layer.alert('网络异常');
			}
		});
	});
	
	
	//点击保存权限
	$('#save_permission').on('click',function(){
		var menuIds = [];
		var rptTypeIds = [];
		var id = $('#this_role_id').val();
		var menuTreeObj = $.fn.zTree.getZTreeObj("menuTree");
		var menuTreeNodes = menuTreeObj.getCheckedNodes();
		
		var reportTreeObj = $.fn.zTree.getZTreeObj("reportViewTree");
		var reportTreeNodes = reportTreeObj.getCheckedNodes();
		
		if(menuTreeNodes.length != 0 && reportTreeNodes.length!=0){
			for (var i = 0; i < menuTreeNodes.length; i++) {
				menuIds.push(menuTreeNodes[i].id);
			}
			for (var i = 0; i < reportTreeNodes.length; i++) {
				rptTypeIds.push(reportTreeNodes[i].id);
			}
		}else{
			layer.alert("请勾选菜单和查看报告权限");
			return;
		}
		console.log('菜单ID集:'+menuIds);
		console.log('报告ID集:'+rptTypeIds);
		var param = {'menuIds':menuIds,'rptTypeIds':rptTypeIds};//参数
		$.ajax({
			url:ctxPath + '/system/role/savePermission/'+ id +'?_t='+new Date().getTime(),
			data:param,
			type:'POST',
			dataType:'json',
			success:function(data){
				if(data.statusCode==200){
					layer.alert(data.msg);
					if(menuDigIndex){
						layer.close(menuDigIndex);
					}
					listui.refresh();
				}else{
					layer.alert(data.msg);
				}
			},
			error:function(errMsg){
				layer.alert('网络异常');
			}
		});
	});
	
	
	//zTree属性设置
	var setting = {
			callback: {
//				点击回调
//				onCheck: zTreeOnCheck
			},
			check: {
				enable: true
			},
	        view: {
	        	showIcon: true,
				showLine: true,
				selectedMulti: false
	        },
	        data: {
	        	key: {
	    			name: "menuName"
	    		},
	            simpleData: {
	                enable: true,
	                idKey: "id",
	    			pIdKey: "parentId",
	    			rootPId: 0
	            }
	        }
	    };
	
	
	//zTree属性设置
	var settingReportType = {
			callback: {
//				点击回调
//				onCheck: zTreeOnCheck
			},
			check: {
				enable: true
			},
	        view: {
	        	showIcon: true,
				showLine: true,
				selectedMulti: false
	        },
	        data: {
	        	key: {
	    			name: "reportName"
	    		},
	            simpleData: {
	                enable: true,
	                idKey: "id",
	    			pIdKey: "parentId",
	    			rootPId: 0
	            }
	        }
	    };


//	function zTreeOnCheck(event, treeId, treeNode) {
//		var treeObj = $.fn.zTree.getZTreeObj("menuTree");
//		var nodes = treeObj.getCheckedNodes();
//		console.log(nodes);
//	};

});