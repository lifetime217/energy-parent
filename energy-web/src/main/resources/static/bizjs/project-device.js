var setting = {
	data : {
		simpleData : {
			enable : true
		}
	},
	view : {
		selectedMulti : false
	},
	callback : {
		onClick : zTreeOnClick
	}
};

function zTreeOnClick(event, treeId, treeNode) {
	if (treeNode.level == 0) {
		$('#delDevice').css('color', 'red');
		$('#btnImp').css('color', 'green');
	} else {
		$('#delDevice').css('color', 'gray');
		$('#btnImp').css('color', 'gray');
	}
};

function refreshTree(){
	$("#deviceTree").empty();
	$.getJSON($('body').attr('ctx') + '/system/project/device/tree/' + $("#deviceTree").data('projectid')).done(function(res){
		if(res.statusCode == 200){
			$.fn.zTree.init($("#deviceTree"), setting, JSON.parse(res.data));
		}else{
			alert("操作失败：" + res.msg);
		}
	}).fail(function(res){
		alert("操作失败：" + res.msg);
	});;
}

$(function() {
	$('#delDevice').on('click', function() {
		var treeObj = $.fn.zTree.getZTreeObj("deviceTree");
		var nodes = treeObj.getSelectedNodes();
		if (nodes.length > 0) {
			if (nodes[0].level == 0) {
				if(confirm("是否确认删除该项目的所有设备？")){
					var id = nodes[0].uuid;
					$.getJSON($('body').attr('ctx') + '/system/project/device/del/' + id).done(function(res){
						if(res.statusCode == 200){
							alert(res.msg);
							refreshTree();
						}else{
							alert("操作失败：" + res.msg);
						}
					}).fail(function(res){
						alert("操作失败：" + res.msg);
					});
				}
			} else {
				alert("请选择根节点");
			}
		} else {
			alert("请选择根节点");
		}
	});

	$('#btnImp').on('click', function() {
		var treeObj = $.fn.zTree.getZTreeObj("deviceTree");
		var nodes = treeObj.getSelectedNodes();
		if (nodes.length > 0) {
			if (nodes[0].level == 0) {
				$('#fileChonse').click();
			} else {
				alert("请选择根节点");
			}
		} else {
			alert("请选择根节点");
		}
	});

	$('#fileChonse').on('input propertychange', function() {
		if($(this).val()){
			var index = layer.load(1, {
				shade : [ 0.5, '#ccc' ]
			});
			var form = $('#fileImpForm').ajaxSubmit();
			var xhr = form.data('jqxhr');
			xhr.done(function(res) {
				console.info(res);
				layer.close(index);
				$('#fileChonse').val('');
				alert(res.msg);
				refreshTree();
			}).fail(function(err) {
				console.error(err);
				layer.close(index);
				$('#fileChonse').val('');
			});
		}
	});

	$(document).ready(function() {
		$.fn.zTree.init($("#deviceTree"), setting, treeData);
	});
});
