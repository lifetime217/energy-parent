$(function(){
	
	var option = {
		    title: {
		        text: "今日电流曲线"
		    },
		    tooltip: {
		        trigger: "axis"
		    },
		    legend: {
		        data: ["线电流Ia", "线电流Ib", "线电流Ic"]
		    },
		    toolbox: {
		        show: true,
		        feature: {
		            dataView: {
		                readOnly: true
		            },
		            magicType: {
		                type: ["line", "bar"],
		                show: false
		            },
		            saveAsImage: {
		                show: true
		            }
		        }
		    },
		    calculable: true,
		    xAxis: [
		        {
		            type: "category",
		            boundaryGap: false,
		            data: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24"]
		        }
		    ],
		    yAxis: [
		        {
		            type: "value",
		            name: "°C"
		        }
		    ],
		    series: [
		        {
		            name: "线电流Ia",
		            type: "line",
		            data: [222, 212, 200, 188, 239, 248, 268, 278, 288, 299, 340, 368, 420, 560, 689, 729, 701, 630]
		        },
		        {
		            name: "线电流Ib",
		            type: "line",
		            data: [212, 222, 230, 209, 246, 240, 288, 320, 300, 320, 350, 378, 410, 500, 629, 719, 748, 680]
		        },
		        {
		            name: "线电流Ic",
		            type: "line",
		            data: [202, 202, 210, 232, 220, 230, 278, 300, 320, 310, 330, 358, 400, 540, 669, 709, 732, 650]
		        }
		    ]
		};
	
	var myChart = echarts.init(document.getElementById("cell_data"), 'macarons');
	myChart.setOption(option);
	
	
});