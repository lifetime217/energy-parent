var xdata = [],dianyaData = [],socData=[],maxDianya=[],minDianya=[];
for(var i=1;i<=24;i++){
	xdata.push(i);
	dianyaData.push((i+10)/5.00);
	socData.push(100 - (i+10)/6.00);
	maxDianya.push(5);
	minDianya.push(3);
}
var option = {
	title : {
		text : "电压/SOC 今日曲线"
	},
	tooltip : {
		trigger : "axis"
	},
	legend : {
		data : [ "电压", "SOC" ]
	},
	toolbox : {
		show : true,
		feature : {
			dataView : {
				readOnly : false
			},
			saveAsImage : {
				show : true
			}
		}
	},
	calculable : true,
	xAxis : [ {
		type : "category",
		axisLine : {
			onZero : false
		},
		data : xdata
	} ],
	grid : {
		y : 70
	},
	yAxis : [ {
		type : "value",
		name : "电压（V）"
	}, {
		type : "value",
		name : "SOC",
		min : 0,
		max : 100
	} ],
	series : [ {
		name : "电压",
		barWidth: 10,
		type : "bar",
		data : dianyaData
	}, {
		type : "line",
		name : "SOC",
		yAxisIndex : 1,
		data : socData
	} ]
};
var myChart2 = echarts.init(document.getElementById('dianya_soc'), "macarons");
myChart2.setOption(option);




option = {
	    tooltip: {
	        trigger: 'axis',
	        axisPointer: {
	            type: 'shadow',
	            label: {
	                show: true
	            }
	        }
	    },
	    legend: {
	        data: ['最高电压','最低电压','最高温度','最低温度']
	    },
	    xAxis: {
	         type: 'category',
	        data: xdata,
	        axisTick: {
	            alignWithLabel: true
	        },
	       
	        splitLine: {
	                show: false,
	               
	            },
	        axisLabel:{
	                //fontWeight:10,
	                //interval:2,
	                fontsize:2,
	                align:'center'
	            }
	    },
	     yAxis: [{
	        type: 'value',
	        splitLine: {
	                show: true
	            },
	            axisLine:{
	                show:false
	            },
	            axisLabel:{
	               
	                fontWeight:10,
	                fontsize:5
	            }
	            
	    }],
	    series: [{
	        name: '最高电压',
	        type: 'bar',
	        stack: '电压极值',
	        barWidth: 10,
	        itemStyle: {
	            normal: {
	                barBorderRadius: 2,
	                color: new echarts.graphic.LinearGradient(
	                    0, 0, 0, 1,
	                    [
	                        {offset: 0, color: '#BC34BC'},
	                        {offset: 1, color: '#7F3594'}
	                    ]
	                )
	            }
	        },
	        data: maxDianya
	    },{
	        name: '最低电压',
	        type: 'bar',
	        stack: '电压极值',
	        barWidth: 10,
	        itemStyle: {
	            normal: {
	                barBorderRadius: 2,
	                color: new echarts.graphic.LinearGradient(
	                    0, 0, 0, 1,
	                    [
	                        {offset: 0, color: '#4740C8'},
	                        {offset: 1, color: '#EF71FF'}
	                    ]
	                )
	            }
	        },
	        data: minDianya
	    },{
	        name: '最高温度',
	        type: 'bar',
	        stack: '温度极值',
	        barWidth: 10,
	        data: maxDianya
	    },{
	        name: '最低温度',
	        type: 'bar',
	        stack: '温度极值',
	        barWidth: 10,
	        data: minDianya
	    }]
	};
var myChart3 = echarts.init(document.getElementById('jizhi'), "macarons");
myChart3.setOption(option);




