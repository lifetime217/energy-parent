$(function() {
	$('#update_user').on('click',function(){
		var id = $('#userId').val();
		var pwd1 = $('#pwd1').val();
		var pwd2 = $('#pwd2').val();
		if(!pwd1 || !pwd2){
			layer.msg('密码不能为空！');
			return;
		}
		if(pwd1!=pwd2){
			layer.msg('两次输入密码不一致！');
			return;
		}
		if(id){
			$.ajax({
				url:$('body').attr('ctx')+'/system/user/update/'+id+"?_t="+new Date().getTime(),
				type:'post',
				dataType:'json',
				data:$('#userInfoForm').serialize(),
				success:function(data){
					if(data.statusCode==200){
						alert(data.msg);
						location.href=$('body').attr('ctx')+"/user/logout";
					}else{
						layer.alert(data.msg);
					}
				},
				error:function(errMsg){
					layer.alert('网络异常');
				}
			})
		}else{
			layer.alert('网络异常');
		}
	})
	
});