/**
 * 
 * @param columnName
 *            列名
 * @param cellVal
 *            单元格值
 * @param obj
 *            行对象
 * @returns
 */
function handleCellValue(columnName, cellVal, obj) {
	if("addTime" == columnName){
		return formatDateTime(cellVal * 1000);
	}
	return cellVal;
}

function getOpBtns() {
	return [ {
		css : "view",
		text : "查看"
	}, {
		css : "del",
		text : "删除"
	}];
}