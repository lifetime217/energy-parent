function fill(index) {
	var moneys = [];
	for (var i = 0; i < 31; i++) {
		moneys[i] = Math.random() * (index > 2 ? 10 : 1000);
	}
	$('.money-chart' + index).sparkline(moneys, {
		type : 'bar',
		tooltipFormatter : function(a, b, c) {
			return Math.floor(c[0].value * 100) / 100;
		},
		barColor : 'green'
	});

	var chong = [], fang = [];
	for (var i = 0; i < 31; i++) {
		chong[i] = Math.random() * (index > 2 ? 10 : 1000);
		fang[i] = Math.random() * (index > 2 ? 10 : 1000);
	}
	$('.cfd-chart' + index).sparkline(chong, {
		fillColor : false,
		tooltipFormatter : function(a, b, c) {
			return "冲：" + Math.floor(c.y * 100) / 100;
		}
	});
	$('.cfd-chart' + index).sparkline(fang, {
		composite : true,
		fillColor : false,
		tooltipFormatter : function(a, b, c) {
			return "<br>放：" + Math.floor(c.y * 100) / 100;
		},
		lineColor : 'red'
	});

	var pcs = [], bms = [], cells = [];
	if (index > 2) {
		for (var i = 0; i < 31; i++) {
			pcs[i] = parseInt(Math.random() * 10) + 2 * 6 - 10;
			bms[i] = parseInt(Math.random() * 100);
			cells[i] = parseInt(Math.random() * 1000);
		}
	} else {
		for (var i = 0; i < 31; i++) {
			pcs[i] = parseInt(Math.random() * 10) + 24 * 6 - 10;
			bms[i] = parseInt(Math.random() * 100) + 24 * 60 * 10 * 2 - 100;
			cells[i] = bms[i] * 68 + parseInt(Math.random() * 1000);
		}
	}

	$('.datacollect-chart' + index).sparkline(pcs, {
		fillColor : false,
		tooltipFormatter : function(a, b, c) {
			return "采集数据集<br>pcs：" + Math.floor(c.y * 100) / 100;
		}
	});
	$('.datacollect-chart' + index).sparkline(bms, {
		composite : true,
		fillColor : false,
		tooltipFormatter : function(a, b, c) {
			return "<br>bms：" + Math.floor(c.y * 100) / 100;
		},
		lineColor : 'red'
	});
	$('.datacollect-chart' + index).sparkline(cells, {
		composite : true,
		fillColor : false,
		tooltipFormatter : function(a, b, c) {
			return "<br>cell：" + Math.floor(c.y * 100) / 100;
		},
		lineColor : 'green'
	});

}

function allMap() {
	var maps = [];
	var u1 = $('body').attr('ctx') + "/system/project/view/5d4caf9206794dbb9ef190472ab91be4";
	var t1 = "<div style='border: 1px solid gainsboro;border-radius: 5px;margin-top: 10px;'>" + "<a href='" + u1
			+ "' style='font-size: 16px;display: block;background-color: gainsboro;padding: 5px;'>腾飞大厦</a><table class='table'><tbody>"
			+ "<tr><td>总存储能量</td><td style='text-align: right;'>276kWh</td></tr>" + "<tr><td>安全运行天数</td><td style='text-align: right;'>253天</td></tr>"
			+ "<tr><td>总电量</td><td style='text-align: right;'>86400Ah</td></tr>" + "<tr><td>故障数</td><td style='text-align: right;'>0</td></tr>"
			+ "<tr><td>联系人</td><td style='text-align: right;'>王经理</td></tr>" + "<tr><td>联系方式</td><td style='text-align: right;'>18916256580</td></tr>" + "</tbody></table></div>";
	var m1 = [ 121.4487155580, 31.1947458680, t1];

	var u2 = $('body').attr('ctx') + "/system/project/view/5d4caf9206794dbb9ef190472ab91be4";
	var t2 = "<div style='border: 1px solid gainsboro;border-radius: 5px;margin-top: 10px;'>" + "<a href='" + u2
	+ "' style='font-size: 16px;display: block;background-color: gainsboro;padding: 5px;'>绿地科创大厦</a><table class='table'><tbody>"
	+ "<tr><td>总存储能量</td><td style='text-align: right;'>414kWh</td></tr>" + "<tr><td>安全运行天数</td><td style='text-align: right;'>192天</td></tr>"
	+ "<tr><td>总电量</td><td style='text-align: right;'>129600Ah</td></tr>" + "<tr><td>故障数</td><td style='text-align: right;'>0</td></tr>"
	+ "<tr><td>联系人</td><td style='text-align: right;'>刘经理</td></tr>" + "<tr><td>联系方式</td><td style='text-align: right;'>13801772760</td></tr>" + "</tbody></table></div>";
	var m2 = [ 121.4268610000, 31.2410610000 ,t2];
	
	
	var u3 = $('body').attr('ctx') + "/system/project/view/5d4caf9206794dbb9ef190472ab91be4";
	var t3 = "<div style='border: 1px solid gainsboro;border-radius: 5px;margin-top: 10px;'>" + "<a href='" + u3
	+ "' style='font-size: 16px;display: block;background-color: gainsboro;padding: 5px;'>测试站点一</a><table class='table'><tbody>"
	+ "<tr><td>总存储能量</td><td style='text-align: right;'>4kWh</td></tr>" + "<tr><td>安全运行天数</td><td style='text-align: right;'>41天</td></tr>"
	+ "<tr><td>总电量</td><td style='text-align: right;'>600Ah</td></tr>" + "<tr><td>故障数</td><td style='text-align: right;'>0</td></tr>"
	+ "<tr><td>联系人</td><td style='text-align: right;'>王东征</td></tr>" + "<tr><td>联系方式</td><td style='text-align: right;'>138******60</td></tr>" + "</tbody></table></div>";
	var m3 = [ 121.226791, 31.021245 ,t3];
	
	return [ m1, m2, m3 ];
}

$(function() {

	fill(1);
	fill(2);
	fill(3);
	fill(4);

	var myChart = echarts.init(document.getElementById("pieChart"), 'macarons');
	var option = {
		tooltip : {
			trigger : 'item',
			formatter : "{a}{b}: {c}"
		},
		legend : {
			orient : 'vertical',
			x : 'left',
			data : [ '充电', '放电' ]
		},
		series : [ {
			name : '充放电占比',
			type : 'pie',
			radius : [ '50%', '70%' ],
			avoidLabelOverlap : false,
			label : {
				normal : {
					show : false,
					position : 'center'
				},
				emphasis : {
					show : true
				}
			},
			labelLine : {
				normal : {
					show : false
				}
			},
			data : [ {
				value : 335,
				name : '充电'
			}, {
				value : 210,
				name : '放电'
			} ]
		} ]
	};

	myChart.setOption(option);

	// 百度地图API功能
	var map = new BMap.Map("allmap");
	var top_left_control = new BMap.ScaleControl({
		anchor : BMAP_ANCHOR_TOP_LEFT
	});// 左上角，添加比例尺
	var top_left_navigation = new BMap.NavigationControl(); // 左上角，添加默认缩放平移控件
	var top_right_navigation = new BMap.NavigationControl({
		anchor : BMAP_ANCHOR_TOP_RIGHT,
		type : BMAP_NAVIGATION_CONTROL_SMALL
	}); // 右上角，仅包含平移和缩放按钮
	/*
	 * 缩放控件type有四种类型:
	 * BMAP_NAVIGATION_CONTROL_SMALL：仅包含平移和缩放按钮；BMAP_NAVIGATION_CONTROL_PAN:仅包含平移按钮；BMAP_NAVIGATION_CONTROL_ZOOM：仅包含缩放按钮
	 */
	map.addControl(top_left_control);
	map.addControl(top_left_navigation);
	map.addControl(top_right_navigation);

	map.centerAndZoom(new BMap.Point(121.448715558, 31.1947458680), 15);
	map.enableScrollWheelZoom();
	var myGeo = new BMap.Geocoder();
	var opts = {
		width : 250, // 信息窗口宽度
		height : 310, // 信息窗口高度
		title : "信息窗口", // 信息窗口标题
		enableMessage : true
	};
	myGeo.getPoint("上海市徐汇区天钥桥路333号", function(point) {
		if (point) {
			map.centerAndZoom(point, 16);
			var data_info = allMap();

			for (var i = 0; i < data_info.length; i++) {
				var marker = new BMap.Marker(new BMap.Point(data_info[i][0], data_info[i][1])); // 创建标注
				var content = data_info[i][2];
				map.addOverlay(marker); // 将标注添加到地图中
				addClickHandler(content, marker);
			}
		} else {
			alert("您选择地址没有解析到结果!");
		}
	}, "上海市");

	function addClickHandler(content, marker) {
		marker.addEventListener("click", function(e) {
			openInfo(content, e)
		});
	}
	function openInfo(content, e) {
		var p = e.target;
		var point = new BMap.Point(p.getPosition().lng, p.getPosition().lat);
		var infoWindow = new BMap.InfoWindow(content, opts); // 创建信息窗口对象
		map.openInfoWindow(infoWindow, point); // 开启信息窗口
	}
});
