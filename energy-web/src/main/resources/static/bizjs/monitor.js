var currentNodeUuid;
function zTreeOnClick(event, treeId, treeNode) {
	if (currentNodeUuid == treeNode.id) {
		return;
	}
	var ctx = $('body').attr('ctx');
	var randomTime = "?t=_" + new Date().getTime();
	if (treeNode.level == 0 && treeNode.uuid) {
		//site
		$('#mainContent>iframe').css('height','900px').attr('src', ctx + '/monitor/site/' + treeNode.uuid + randomTime);
	} else if (treeNode.level == 1) {
	} else if (treeNode.level == 2) {
		if(treeNode.name.indexOf('bms') != -1 || treeNode.name.indexOf('BMS') != -1){
			$('#mainContent>iframe').css('height','1200px').attr('src', ctx + '/monitor/bms/' + treeNode.uuid + randomTime);
		}else if(treeNode.name.indexOf('pcs') != -1 || treeNode.name.indexOf('PCS') != -1){
			$('#mainContent>iframe').css('height','800px').attr('src', ctx + '/monitor/pcs/' + treeNode.uuid + randomTime);
		}
	} else if (treeNode.level == 3) {
		$('#mainContent>iframe').css('height','1000px').attr('src', ctx + '/monitor/group/' + treeNode.name + randomTime);
	} else if (treeNode.level == 4) {
		$('#mainContent>iframe').css('height','800px').attr('src', ctx + '/monitor/cell/' + treeNode.name + randomTime);
	}
	currentNodeUuid = treeNode.id;
}

$(function() {
	var projectChonseIndex;
	
	$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
		checkboxClass : 'icheckbox_minimal-blue',
		radioClass : 'iradio_minimal-blue'
	});
	
	if($('#defaultProjectShow').val()){
		//如果有默认的要展示
		$.getJSON($('body').attr('ctx') + '/monitor/project/tree/' + $('#defaultProjectShow').val()).done(function(res) {
			$.fn.zTree.destroy();
			$('.container-fluid').show();
			var treeObj = $.fn.zTree.init($(".ztree"), setting, JSON.parse(res.data));
			var nodes = treeObj.getNodes();
			if (nodes.length>0) {
				zTreeOnClick(null,null,nodes[0]);
			}
		});
	}else{
		projectChonseIndex = layer.open({
			type : 1,
			title : "请选择要监控的项目",
			closeBtn : 0,
			anim : 2,
			offset : '50px',
			area : [ '380px', '300px' ],
			content : $('#projectSelected')
		});
	}
	
	
	$('#switchProject').on('click',function(){
		projectChonseIndex = layer.open({
			type : 1,
			title : "请选择要监控的项目",
			anim : 2,
			offset : '50px',
			area : [ '380px', '300px' ],
			content : $('#projectSelected')
		});
	});

	$('#btnOk').on('click', function() {
		var pid = $('input[name="r1"]:checked').val();
		if (pid) {
			$('#projectName').text($('input[name="r1"]:checked').attr('data-pname'));
			$.getJSON($('body').attr('ctx') + '/monitor/project/tree/' + pid).done(function(res) {
				$.fn.zTree.destroy();
				$('.container-fluid').show();
				var treeObj = $.fn.zTree.init($(".ztree"), setting, JSON.parse(res.data));
				var nodes = treeObj.getNodes();
				if (nodes.length>0) {
					zTreeOnClick(null,null,nodes[0]);
				}
				layer.close(projectChonseIndex);
			});
			
		} else {
			layer.alert("请选择一个项目 ");
		}
	});

	var setting = {
		data : {
			simpleData : {
				enable : true
			}
		},
		view : {
			selectedMulti : false
		},
		callback : {
			onClick : zTreeOnClick
		}
	};

});