var ctxPath = $('body').attr('ctx') ;
function getOpBtns() {
	return [ {
		css : "resetPwd",
		text : "重置密码"
	}, {
		css : "del",
		text : "删除"
	}, {
		css : "permission",
		text : "授权"
	} ];
}

$(function() {
	var addDlgIndex;
	var permissionDigIndex;
	$('#add').click(function() {
		addDlgIndex = layer.open({
			title : "添加新用户",
			type : 1,
			area : [ '300px', '420px' ], // 宽高
			content : $('#add_user_dlg')
		});
	});
	
	$('#add_user_submit').click(function(){
		$('#add_user_dlg_msg').text('');
		$.getJSON($("#add_user_form").attr('action'), $("#add_user_form").serialize()).done(function(data) {
			if(data.statusCode == 200){
				layer.alert(data.msg);
				if(addDlgIndex){
					layer.close(addDlgIndex);
				}
				listui.refresh();
			}else{
				$('#add_user_dlg_msg').text(data.msg);
			}
		}).fail(function(err){
			layer.alert(err);
		});
		return false;
	});
	
	
	$('.list-content-div table>tbody').delegate('tr>td>.resetPwd', 'click', function() {
		var id = $(this).data("id");
		if (id) {
			$.ajax({
				url:ctxPath+ '/system/user/resetPwd/' + id+'?_t='+new Date().getTime(),
				type:'POST',
				dataType:'json',
				success:function(data){
					if(data.statusCode==200){
						layer.alert(data.msg);
						listui.refresh();
					}else{
						layer.alert(data.msg);
					}
				},
				error:function(errMsg){
					layer.alert('网络异常');
				}
			});
		}
	});
	
	//点击授权按钮
	$('.list-content-div table>tbody').delegate('tr>td>.permission', 'click', function() {
		var id = $(this).data("id");
		if (id) {
			$('#userId').val(id);
			//获取相应用户的权限列表
			$.ajax({
				url:ctxPath +'/system/user/permission/'+id+'?_t='+new Date().getTime(),
				type:'POST',
				dataType:'json',
				success:function(data){
					if(data.statusCode==200){
						permissionDigIndex =  layer.open({
							title : "用户授权",
							type : 1,
							area : [ '280px', '270px' ], // 宽高
							content : $('#permission')
						});
						//角色树
						var roleTreeData=data.data.role;
						$.fn.zTree.init($("#roleTree"),setting,roleTreeData);
						var roleTreeObj = $.fn.zTree.getZTreeObj("roleTree");
						roleTreeObj.expandAll(true);
						
					}else{
						layer.alert(data.msg);
					}
				},
				error:function(errMsg){
					layer.alert('网络异常');
				}
			});
		}else{
			layer.alert('网络异常');
		}
	});

	
	//点击授权保存
	$('#save_permission').on('click',function(){
		var roleIds = [];
		var id = $('#userId').val();
		var roleTreeObj = $.fn.zTree.getZTreeObj("roleTree");
		var roleTreeNodes = roleTreeObj.getCheckedNodes();
		
		if(roleTreeNodes.length!=0 ){
			for (var i = 0; i < roleTreeNodes.length; i++) {
				roleIds.push(roleTreeNodes[i].roleId);
			}
			var param = {'roleIds':roleIds};
			$.ajax({
				url:ctxPath + '/system/user/permission/save/'+ id +'?_t='+new Date().getTime(),
				data:param,
				type:'POST',
				dataType:'json',
				success:function(data){
					if(data.statusCode==200){
						layer.alert(data.msg);
						if(permissionDigIndex){
							layer.close(permissionDigIndex);
						}
						listui.refresh();
					}else{
						layer.alert(data.msg);
					}
				},
				error:function(errMsg){
					layer.alert('网络异常');
				}
			});
		}else{
			layer.alert("请至少勾选一种角色");
			return;
		}
	});
	
	
	
	//zTree属性设置
	var setting = {
			callback: {
//				点击回调
//				onCheck: zTreeOnCheck
			},
			check: {
				enable: true
			},
	        view: {
	        	showIcon: true,
				showLine: true,
				selectedMulti: false
	        },
	        data: {
	        	key: {
	    			name: "roleName"
	    		},
	            simpleData: {
	                enable: true,
	                idKey: "roleId",
	    			pIdKey: "parentId",
	    			rootPId: 0
	            }
	        }
	    };

//	function zTreeOnCheck(event, treeId, treeNode) {
//		var treeObj = $.fn.zTree.getZTreeObj("roleTree");
//		var nodes = treeObj.getCheckedNodes();
//		console.log(nodes);
//	};
	
});