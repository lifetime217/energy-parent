function getCustomQueryData() {
	var projectId = $(this).find('input[name="projectId"]').val();
	return {
		"projectId" : projectId
	};
}

$(function() {
	var addDlgIndex;
	$('#add_electrictable_submit').click(function() {
		$('#add_electrictable_dlg_msg').text('');
		$.getJSON($("#add_electrictable_form").attr('action'), $("#add_electrictable_form").serialize()).done(function(data) {
			if (data.statusCode == 200) {
				alert(data.msg);
				if (addDlgIndex) {
					layer.close(addDlgIndex);
				}
				listui.refresh();
			} else {
				$('#add_electrictable_dlg_msg').text(data.msg);
			}
		}).fail(function(err) {
			alert(err);
		});
		return false;
	});

	$('#btnAdd').click(function() {
		addDlgIndex = layer.open({
			title : "添加项目电价分时价",
			type : 1,
			area : [ '300px', '350px' ], // 宽高
			content : $('#add_electrictable_dlg')
		});
	});
});
