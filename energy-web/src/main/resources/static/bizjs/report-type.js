var ctxPath = $('body').attr('ctx');
function getOpBtns() {
	return [ {
		css : "view",
		text : "编辑"
	}, {
		css : "delete",
		text : "删除"
	} ];
}

$(function() {
	var addDlgIndex;
	//点击添加
	$('#add').click(function() {
		$('#report_type_id').val('');
		$('#reportName').val('');
		$('#reportType').val('');
		$('#reportUrl').val('');
		$('#_description').val('');
		$('#add_report_type_dlg_msg').empty();
		$('#add_report_type_submit').show();
		$('#update_report_type').hide();
		addDlgIndex = layer.open({
			title : "新增报告类型",
			type : 1,
			area : [ '300px', '420px' ], // 宽高
			content : $('#add_report_type_dlg')
		});
	});
	
	//点击提交
	$('#add_report_type_submit').click(function(){
		$('#add_report_type_dlg_msg').text('');
		$.getJSON($("#add_report_type_form").attr('action'), $("#add_report_type_form").serialize()).done(function(data) {
			if(data.statusCode == 200){
				layer.alert(data.msg);
				if(addDlgIndex){
					layer.close(addDlgIndex);
				}
				listui.refresh();
			}else{
				$('#add_report_type_dlg_msg').text(data.msg);
			}
		}).fail(function(err){
			layer.alert("网络异常");
		});
		return false;
	});
	
	
	$('.list-content-div table>tbody').delegate('tr>td>.view', 'click', function() {
		var id = $(this).data("id");
		if (id) {
			$.ajax({
				url:ctxPath+"/system/report/view/"+id+"?_t="+new Date().getTime(),
				type:'POST',
				dataType:'json',
				success:function(data){
					if(data.statusCode==200){
						$('#report_type_id').val(data.data.id);
						$('#reportName').val(data.data.reportName);
						$('#reportType').val(data.data.reportType);
						$('#reportUrl').val(data.data.reportUrl);
						$('#_description').val(data.data.description);
						$('#update_report_type').show();
						$('#add_report_type_submit').hide();
						$('#add_role_dlg_msg').empty();
						addDlgIndex = layer.open({
							title : "修改报告类型",
							type : 1,
							area : [ '300px', '420px' ], // 宽高
							content : $('#add_report_type_dlg')
						});
						
					}else{
						layer.alert("网络异常");
					}
				},
				error:function(errMsg){
					layer.alert("网络异常");
				}
			});
		}
	});
	
	//点击修改保存按钮
	$('#update_report_type').on('click',function(){
		$.ajax({
			url:ctxPath+"/system/report/update?_t="+new Date().getTime(),
			type:'post',
			data:$("#add_report_type_form").serialize(),
			dataType:'json',
			success:function(data){
				if(data.statusCode==200){
					layer.alert(data.msg);
					if(addDlgIndex){
						layer.close(addDlgIndex);
					}
					listui.refresh();
				}else{
					layer.alert('网络异常');
				}
			},
			error:function(errMsg){
				layer.alert("网络异常");
			}
		});
	})
	
	//删除
	$('.list-content-div table>tbody').delegate('tr>td>.delete', 'click', function() {
		var id = $(this).data("id");
		if (id) {
			layer.confirm('确定删除吗？', {
				  btn: ['确认','取消'] // 按钮
				}, function(){
					$.ajax({
						url:ctxPath+"/system/report/del/"+id+"?_t="+new Date().getTime(),
						type:'post',
						dataType:'json',
						success:function(data){
							if(data.statusCode==200){
								layer.msg(data.msg);
								listui.refresh();
							}
						},
						error:function(errMsg){
							layer.alert("网络异常");
						}
					});
				})
		}else{
			layer.alert("网络异常,请联系管理员");
		}
	});
	
	
	
});