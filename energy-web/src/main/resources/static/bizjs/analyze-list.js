/**
 * 
 * @param columnName
 *            列名
 * @param cellVal
 *            单元格值
 * @param obj
 *            行对象
 * @returns
 */
function handleCellValue(columnName, cellVal, obj) {
	if(columnName == "name"){
		return "腾飞大厦";
	}
	return cellVal;
}

function getCustomQueryData() {
	return {};
}

function getOpBtns() {
	return [ {
		css : "view-report",
		text : "查看报告"
	}];
}


$(function(){
	
	$('.list-content-div table>tbody').delegate('tr>td>.view-report', 'click', function() {
		var id = $(this).data("id");
		if (id) {
			layer.open({
				title : "诊断报告",
				type : 2,
				area : [ '650px', '600px' ], // 宽高
				content : $('body').attr('ctx') + '/diagnostic/analyze/report/' + id
			});
		}
	});
});