/**
 * 
 * @param columnName
 *            列名
 * @param cellVal
 *            单元格值
 * @param obj
 *            行对象
 * @returns
 */
function handleCellValue(columnName, cellVal, obj) {
	if("name" == columnName){
		var url = $('body').attr('ctx') + "/system/project/view/" + obj.id;
		return "<a href='" + url + "'>" + cellVal + "</a>";
	}
	return cellVal;
}

function getOpBtns() {
	return [ {
		css : "edit",
		text : "编辑"
	}, {
		css : "del",
		text : "删除"
	}, {
		css : "djb",
		text : "电价表"
	}, {
		css : "sbgl",
		text : "设备管理"
	} ];
}

var addDlgIndex;
function editBill(id) {
	$.getJSON($('body').attr('ctx') + '/system/project/get/'+id).done(function(res) {
		if (res.statusCode == 200) {
			var obj = res.data;
			$('#add_project_form').find('input[name]').each(function(i, o) {
				if ($(o).attr('name')=='joinTime') {
					//$(o).val('2000');
				}
				$(o).val(obj[$(o).attr('name')]);
			});
			$('#add_project_form').attr('action',$('body').attr('ctx') + '/system/project/update/'+$('input[name=id]').val());
			$('#add_project_submit').hide();
			$('#update_project_save').show();
			addDlgIndex = layer.open({
				title : "编辑项目",
				type : 1,
				area : [ '450px', '600px' ], // 宽高
				content : $('#add_project_dlg')
			});
		}
	});
}

//点击修改保存
$('#update_project_save').click(function() {
	$('#add_project_dlg_msg').text('');
	$.getJSON($("#add_project_form").attr('action'), $("#add_project_form").serialize()).done(function(data) {
		if (data.statusCode == 200) {
			layer.alert(data.msg);
			if (addDlgIndex) {
				layer.close(addDlgIndex);
			}
			listui.refresh();
		} else {
			$('#add_project_dlg_msg').text(data.msg);
		}
	}).fail(function(err) {
		alert(err);
	});
	return false;
});


$(function() {
	$('.list-content-div table>tbody').delegate('tr>td>.djb', 'click', function() {
		var id = $(this).data("id");
		if (id) {
			layer.open({
				title : "项目电价表",
				type : 2,
				area : [ '750px', '600px' ], // 宽高
				content : $('body').attr('ctx') + '/system/electrictable/' + id
			});
		}
	});

	$('.list-content-div table>tbody').delegate('tr>td>.sbgl', 'click', function() {
		var id = $(this).data("id");
		if (id) {
			layer.open({
				title : "设备管理",
				type : 2,
				area : [ '450px', '500px' ], // 宽高
				content : $('body').attr('ctx') + '/system/project/device/' + id
			});
		}
	});

	//点击新增
	$('#add').click(function() {
		$('#add_project_dlg_msg').text('');//清除提示
		$('#add_project_form input[name]').val('');//清除输入
		$('#add_project_form').attr('action',$('body').attr('ctx') + '/system/project/add');
		$('#add_project_submit').show();
		$('#update_project_save').hide();
		addDlgIndex = layer.open({
			title : "添加项目",
			type : 1,
			area : [ '450px', '600px' ], // 宽高
			content : $('#add_project_dlg')
		});
	});

	//新增保存
	$('#add_project_submit').click(function() {
		$('#add_project_dlg_msg').text('');
		$.getJSON($("#add_project_form").attr('action'), $("#add_project_form").serialize()).done(function(data) {
			if (data.statusCode == 200) {
				layer.alert(data.msg);
				if (addDlgIndex) {
					layer.close(addDlgIndex);
				}
				listui.refresh();
			} else {
				$('#add_project_dlg_msg').text(data.msg);
			}
		}).fail(function(err) {
			alert(err);
		});
		return false;
	});

	var mapIndex;
	$('#chrose_map').click(function() {
		mapIndex = layer.open({
			title : "选择项目位置",
			type : 1,
			area : [ '450px', '300px' ], // 宽高
			content : $('#map_ok')
		});
	});

	$('#btn_map_ok').click(function() {
		var myGeo = new BMap.Geocoder();
		// 将地址解析结果显示在地图上,并调整地图视野
		var lgt = $('#lgt').val();
		if (lgt) {
			$('#projectAddress').val($("#suggestId").val());
			if (mapIndex) {
				layer.close(mapIndex);
			}
		}
	});

	var selectMap = function() {
		map.clearOverlays();
		$("#projectAddress").val('');
		$("#lgt").val('');
		$("#lat").val('');
		var myGeo = new BMap.Geocoder();
		var sads = $('#suggestId').val();
		myGeo.getPoint(sads, function(point) {
			if (point) {
				$("#lgt").val(point.lng);
				$("#lat").val(point.lat);
				$("#projectAddress").val(sads);
				map.centerAndZoom(point, 16);
				var marker = new BMap.Marker(point);
				map.addOverlay(marker);
				marker.setAnimation(BMAP_ANIMATION_BOUNCE);
				marker.addEventListener("dragend", function(e) {
					$("#lgt").val(e.point.lng);
					$("#lat").val(e.point.lat);
				})
			} else {
				alert("您选择地址没有解析到结果!");
			}
		}, "中国");
	}

	$('#suggestId').on('keypress', function(event) {
		if (event.keyCode == "13") {
			selectMap();
		}
	}).on('blur', function() {
		selectMap();
	});

	var map = new BMap.Map("l-map"); // 创建地图实例
	map.enableScrollWheelZoom(); // 启用滚轮放大缩小，默认禁用
	var point = new BMap.Point(116.404, 39.915); // 创建点坐标
	map.centerAndZoom(point, 16);
	var geolocation = new BMap.Geolocation();
	geolocation.getCurrentPosition(function(r) {
		if (this.getStatus() == BMAP_STATUS_SUCCESS) {
			var marker = new BMap.Marker(r.point);
			map.addOverlay(marker);
			marker.enableDragging(); // 可拖拽
			marker.setAnimation(BMAP_ANIMATION_BOUNCE); // 跳动的动画
			marker.addEventListener("dragend", function(e) { // 拖拽标注获取标注坐标
				$("#lgt").val(e.point.lng);
				$("#lat").val(e.point.lat);
			})
			map.panTo(r.point);
		} else {
			alert('failed' + this.getStatus());
		}
	}, {
		enableHighAccuracy : true
	})

});
