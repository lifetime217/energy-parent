
$(function(){
	var xdata = [],shiji = [],yuce = [];
	if($('#reportId').val() == '201806'){
		xdata = ['201804','201805','201806','201807','201808'];
		shiji = [100, 100, 99];
		yuce = [100,100,100,99,99];
	}else if($('#reportId').val() == '201808'){
		xdata = ['201804','201805','201806','201807','201808','201809','201810'];
		shiji = [100, 100, 99, 99, 99];
		yuce = [100,100,100,99,99,99,98];
	}else if($('#reportId').val() == '201810'){
		xdata = ['201804','201805','201806','201807','201808','201809','201810','201811','201812'];
		shiji = [100, 100, 99, 99, 99,99, 98];
		yuce = [100,100,100,99,99,99,98,98,98];
	}else if($('#reportId').val() == '201812'){
		xdata = ['201804','201805','201806','201807','201808','201809','201810','201811','201812','201901','201902'];
		shiji = [100, 100, 99, 99, 99,99, 98,98,98];
		yuce = [100,100,100,99,99,99,98,98,98,97,97];
	}
	
	var option = {
			title: {
				 x: "center",
				text: '储能系统总容量'
			},
			tooltip : {
				trigger: 'axis',
				axisPointer: {
					type: 'cross',
					label: {
						backgroundColor: '#6a7985'
					}
				}
			},
			legend: {
				 orient: "vertical",
			        x: "left",
				data:['容量变化','预测曲线']
			},
			toolbox: {
				feature: {
					saveAsImage: {}
				}
			},
			grid: {
				left: '3%',
				right: '4%',
				bottom: '3%',
				containLabel: true
			},
			xAxis : [
				{
					type : 'category',
					boundaryGap : false,
					data : xdata 
				}
				],
				yAxis : [
					{
						min: 0,
			            max: 110,
			            axisLabel: {
			                show: true,
			                formatter: "{value}%"
			            },
						type : 'value'
					}
					],
					series : [
						{
							name:'容量变化',
							type:'line',
							color:'#00c0ef',
							areaStyle: {},
							data:shiji
						},
						{
							name:'预测曲线',
							type:'line',
							color:'#ddd',
							areaStyle: {},
							data: yuce
						}
						]
	};
	var myChart = echarts.init(document.getElementById('cap_chart'),"macarons");
	myChart.setOption(option);
	
	
	option = {
		    title: {
		        text: "最高温度测点频次",
		        x: "center"
		    },
		    tooltip: {
		        trigger: "item",
		        formatter: "{a} <br/>{b} : {c} ({d}%)"
		    },
		    legend: {
		        orient: "vertical",
		        x: "left",
		        data: ["5箱14号", "7箱22号", "12箱5号", "7箱15号", "其他"]
		    },
		    toolbox: {
		        feature: {
		            dataView: {
		                readOnly: true
		            },
		            saveAsImage: {
		                show: true
		            }
		        },
		        show: true
		    },
		    calculable: true,
		    series: [
		        {
		            name: "访问来源",
		            type: "pie",
		            radius: "55%",
		            center: ["50%", "60%"],
		            itemStyle: {
		                normal: {
		                    label: {
		                        show: true,
		                        formatter: "{b}: {c} ({d}%)"
		                    }
		                }
		            },
		            data: [
		                {
		                    value: 31,
		                    name: "5箱14号"
		                },
		                {
		                    value: 53,
		                    name: "7箱22号"
		                },
		                {
		                    value: 7,
		                    name: "12箱5号"
		                },
		                {
		                    value: 5,
		                    name: "7箱15号"
		                },
		                {
		                    value: 4,
		                    name: "其他"
		                }
		            ]
		        }
		    ]
		};
	var myChart = echarts.init(document.getElementById('hig_temp_chart'),"macarons");
	myChart.setOption(option);
	
	option = {
		    title: {
		        text: "最高电压编号频次",
		        x: "center"
		    },
		    tooltip: {
		        trigger: "item",
		        formatter: "{a} <br/>{b} : {c} ({d}%)"
		    },
		    legend: {
		        orient: "vertical",
		        x: "left",
		        data: ["1箱4号", "12箱5号", "1箱15号", "10箱5号", "其他"]
		    },
		    toolbox: {
		        feature: {
		            dataView: {
		                readOnly: true
		            },
		            saveAsImage: {
		                show: true
		            }
		        },
		        show: true
		    },
		    calculable: true,
		    series: [
		        {
		            name: "占比",
		            type: "pie",
		            radius: "55%",
		            center: ["50%", "60%"],
		            itemStyle: {
		                normal: {
		                    label: {
		                        show: true,
		                        formatter: "{b}: {c} ({d}%)"
		                    }
		                }
		            },
		            data: [
		                {
		                    value: 35,
		                    name: "1箱4号"
		                },
		                {
		                    value: 42,
		                    name: "12箱5号"
		                },
		                {
		                    value: 8,
		                    name: "1箱15号"
		                },
		                {
		                    value: 10,
		                    name: "10箱5号"
		                },
		                {
		                    value: 5,
		                    name: "其他"
		                }
		            ]
		        }
		    ]
		};
	var myChart = echarts.init(document.getElementById('hig_v_chart'),"macarons");
	myChart.setOption(option);
	
	
	option = {
		    tooltip: {
		        trigger: "axis",
		        axisPointer: {
		            type: "shadow"
		        }
		    },
		    title: {
				 x: "center",
				text: '电芯容量直方分布图'
			},
		    legend: {
		        data: ["电芯容量"],
		        orient: "vertical",
		        x: "left",
		        itemWidth: 20
		    },
		    toolbox: {
		    	 feature: {
			            dataView: {
			                readOnly: true
			            },
			            magicType: {
			                type: ["line", "bar", "stack", "tiled"],
			                show: false
			            },
			            saveAsImage: {
			                show: true
			            }
			        },
			        show: true
		    },
		    calculable: true,
		    xAxis: [
		        {
		            type: "category",
		            data: ["175", "175.5", "176", "176.5", "177", "177.5", "178", "178.5", "179", "179.5", "180", "180.5", "181", "181.5", "182"],
		            axisLine: {
		                show: true
		            },
		            splitLine: {
		                show: false
		            }
		        }
		    ],
		    yAxis: [
		        {
		            type: "value",
		            splitLine: {
		                show: false
		            },
		            axisTick: {
		                show: true
		            }
		        }
		    ],
		    series: [
		        {
		            name: "电芯容量",
		            type: "bar",
		            data: [1, 2, 3, 4, 5, 16, 42, 84, 62, 10, 4, 3, 2, 1, 0.1],
		            barWidth: 11
		        }
		    ]
		};
	var myChart = echarts.init(document.getElementById('dx_cap_chart'),"macarons");
	myChart.setOption(option);
	
	option = {
		    tooltip: {
		        trigger: "axis",
		        axisPointer: {
		            type: "shadow"
		        }
		    },
		    title: {
				 x: "center",
				text: '电芯阻值直方分布图'
			},
		    legend: {
		        data: ["电芯阻值"],
		        orient: "vertical",
		        x: "left",
		        itemWidth: 21
		    },
		    toolbox: {
		        feature: {
		            dataView: {
		                readOnly: true
		            },
		            magicType: {
		                type: ["line", "bar", "stack", "tiled"],
		                show: false
		            },
		            saveAsImage: {
		                show: true
		            }
		        },
		        show: true
		    },
		    calculable: true,
		    xAxis: [
		        {
		            type: "category",
		            data: ["2.20", "2.21", "2.22", "2.23", "2.24", "2.25", "2.26", "2.27", "2.28", "2.29", "2.30", "2.31", "2.32", "2.33", "2.34", "2.35", "2.36", "2.37", "2.38", "2.39", "2.40", "2.41", "2.42", "2.43", "2.44", "2.45", "2.46", "2.47", "2.48", "2.49", "2.50", "2.51", "2.52", "2.53", "2.54", "2.55"],
		            axisLine: {
		                show: true
		            },
		            splitLine: {
		                show: false
		            }
		        }
		    ],
		    yAxis: [
		        {
		            type: "value",
		            splitLine: {
		                show: false
		            },
		            axisTick: {
		                show: true,
		                inside: false
		            }
		        }
		    ],
		    series: [
		        {
		            name: "电芯阻值",
		            type: "bar",
		            data: [2, 2, 2, 0, 0, 0, 0, 0, 8, 8, 7, 7, 9, 9, 22, 22, 22, 61, 62, 55, 55, 36, 36, 18, 18, 8, 8, 8, 8, 7, 7, 1, 1, 1, 1, 1],
		            barWidth: 10,
		            itemStyle: {
		                normal: {
		                    color: "rgb(44, 178, 64)",
		                    borderWidth: 0,
		                    borderRadius: 3
		                }
		            }
		        }
		    ]
		};
	var myChart = echarts.init(document.getElementById('dx_r_chart'),"macarons");
	myChart.setOption(option);
	
});
