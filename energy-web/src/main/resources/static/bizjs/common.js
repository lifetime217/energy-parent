$(function(){
	var refreshNotice = function(alertList){
		$('#system_notice_count').data('alertCount',alertList.length);//警告数量
		$('#system_notice_count>span').remove();
		$('#system_notice_list').empty();
		if(alertList.length > 0){
			$('#system_notice_count').append('<span class="label label-warning">' + alertList.length + '</span>');
			$('#system_notice_list').append('<li class="header">您有' + alertList.length + '条消息</li>');
			var list_li = $('<li/>');
			var list_ul = $('<ul/>').attr('class','menu');
			for(var i = 0; i<alertList.length; i++){
				var info = alertList[i];//每一条警告的内容
				if(info.alertLevel == 4){//警告等级
					list_ul.append($('<li/>').append('<a target="_blank" '+'href='+$("body").attr("ctx")+"/alert"+'><i class="fa fa-warning text-red"></i>' + info.alertContent + '</a>'));
				}else if(info.alertLevel == 3){
					list_ul.append($('<li/>').append('<a target="_blank" '+'href='+$("body").attr("ctx")+"/alert"+'><i class="fa fa-warning text-yellow"></i>' + info.alertContent + '</a>'));
				} else {
					list_ul.append($('<li/>').append('<a target="_blank" '+'href='+$("body").attr("ctx")+"/alert"+'><i class="fa fa-user text-green"></i>' + info.alertContent + '</a>'));
				}
			}
			list_ul.append('<li class="footer"><a href="'+$("body").attr("ctx")+"/alert"+'">查看全部</a></li>');
			list_li.append(list_ul);
			$('#system_notice_list').append(list_li);
		}
	}
	
	$(document).ready(function(){
		var sansuo = setInterval(function(){
			var count = $('#system_notice_count').data('alertCount');
			if(count > 0){
				$('#system_notice_count>span').toggle();
			}
		},600);
		
		var url = $('body').attr('ctx') + "/alert/all";
		var requestNotice = function(){
			$.post(url,function(res){//请求后台获取所有警告
				if(res.statusCode == 200){
					refreshNotice(res.data);//拿到警告列表.刷新右上角警告区域
				}
			},"json");
		}
		
		requestNotice();
		setInterval(function(){
			requestNotice();
		},50000000);
		
		
	});
	
	$('.fa-bell').parent().find('.pull-right-container').empty().append('<span class="label pull-right bg-red">4</span>');
	$('.fa-flash').parent().find('.pull-right-container').empty().append('<span class="label pull-right bg-green">1</span>');
});