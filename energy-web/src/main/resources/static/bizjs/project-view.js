$(function(){
	
	$('#btnPcsImp').on("click",function(){
		$('#pcsfileChonse').click();
	});

	$('#btnBmsImp').on("click",function(){
		$('#bmsfileChonse').click();
	});
	
	$('#bmsfileChonse,#pcsfileChonse').on('input propertychange', function() {
		if($(this).val()){
			var form = $(this).closest('form');
			var index = layer.load(1, {
				shade : [ 0.5, '#ccc' ]
			});
			if(!$(this)[0].files[0].name.toLowerCase().endsWith('.xlsx')){
				layer.close(index);
				layer.alert('您上传的文件格式不正确，请先下载模板后导入！');
				return;
			}
			if($(this)[0].files[0].size>10485760){
				layer.close(index);
				layer.alert('文件不能大于10M');
				return;
			}
			var formAjax = form.ajaxSubmit();
			console.log($(this)[0].files[0])
			var xhr = formAjax.data('jqxhr');
			xhr.done(function(res) {
				console.log(res);
				layer.close(index);
				$('#bmsfileChonse,#pcsfileChonse').val('');
				layer.alert(res.msg);
			}).fail(function(err) {
				console.error(err);
				layer.close(index);
				$('#bmsfileChonse,#pcsfileChonse').val('');
				layer.alert('文件不能大于10M')
			});
		}
	});
	
	$('.item-chart').sparkline([3,4,5,3,5,6,6,4,6,3,6,4,5,5,5,4,5,4,4,5,6,4,3,6,3,5,6,5],{type: 'bar', barColor: 'green'});
	
	$(".bms-chart").sparkline([[2,-2],[3,-4],[6,-7],[8,-6],[5,-4],[3,-4],[6,-7],[8,-6],[5,-4],[3,-4],[6,-7],[8,-6],[5,-4],[3,-4],[6,-7],[8,-6],[5,-4]], {
	    type: 'bar',
	    height: '34',
	    barWidth: 5,
	    barColor: '#00ffff',
	    negBarColor: '#7fff00',
	    zeroColor: '#ffff56'});
});