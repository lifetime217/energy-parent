
$(function(){
	//获取项目故障信息.
	$.ajax({
		url:window.location.pathname+'/bug?_t='+new Date().getTime(),
		type:'post',
		dataType:'json',
		success:function(data){
			var table = $('#bugTable');
			table.empty();
			if(data.statusCode==200){
				if(data.data!=null){
					$(data.data).each(function(i,e){
						console.log(e)
						var tr = $('<tr/>');
						var td1 = $('<td/>');
						var td2 = $('<td/>');
						var td3 = $('<td/>');
						var td4 = $('<td style="width:150px;"/>');
						tr.append(td1.append(e.alertDevice==10?'PCS':e.alertDevice==20?'BMS':e.alertDevice==30?'电池单体':'其他设备'))
						.append(td2.append(e.alertContent))
						.append(td3.append(e.alertLevel ==1 ?'<span class="label label-danger">1</span>':e.alertLevel ==2 ?'<span class="label label-warning">2</span>':e.alertLevel ==3 ?'<span class="label label-info">3</span>':e.alertLevel ==4?'<span class="label label-default">4</span>':'<span class="label label-success">可忽略</span>'))
						.append(td4.append(e.alertTime));
						table.append(tr);
					})
				}else{
					table.append('<text>暂无故障信息</text>');
				}
			}else{
				table.append('<text>网络异常</text>');
			}
		},
		error:function(data){
			table.append('<text>网络异常</text>');
		}
	})
	
	var option = {
		    title: {
		        text: "今日电费曲线"
		    },
		    tooltip: {
		        trigger: "axis"
		    },
		    legend: {
		        data: ["省电费", "耗电费"]
		    },
		    toolbox: {
		        show: true,
		        feature: {
		            dataView: {
		                readOnly: true
		            },
		            magicType: {
		                type: ["line", "bar"],
		                show: false
		            },
		            saveAsImage: {
		                show: true
		            }
		        }
		    },
		    calculable: true,
		    xAxis: [
		        {
		            type: "category",
		            data: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24"]
		        }
		    ],
		    yAxis: [
		        {
		            type: "value"
		        }
		    ],
		    series: [
		        {
		            name: "省电费",
		            type: "bar",
		            data: [2, 4.9, 7, 23.2, 25.6, 76.7, 135.6, 162.2, 32.6, 20, 6.4, 3.3, 25.6, 76.7, 6.4, 3.3, 25.6, 76.7, 6.4, 3.3, 25.6, 76.7, 25.6, 76.7]
		        },
		        {
		            name: "耗电费",
		            type: "bar",
		            data: [2.6, 5.9, 9, 26.4, 28.7, 70.7, 175.6, 182.2, 48.7, 18.8, 6, 2.3, 28.7, 70.7, 6, 2.3, 28.7, 70.7, 6, 2.3, 28.7, 70.7, 28.7, 70.7]
		        }
		    ]
		};
	var myChart = echarts.init(document.getElementById("dianfeiChart"), 'macarons');
	myChart.setOption(option);
	
	
	
	option = {
		    title: {
		        text: "今日充放电曲线"
		    },
		    tooltip: {
		        trigger: "axis"
		    },
		    legend: {
		        data: ["充电", "放电"]
		    },
		    toolbox: {
		        show: true,
		        feature: {
		            dataView: {
		                readOnly: true
		            },
		            magicType: {
		                type: ["line", "bar"],
		                show: false
		            },
		            saveAsImage: {
		                show: true
		            }
		        }
		    },
		    calculable: true,
		    xAxis: [
		        {
		            type: "category",
		            boundaryGap: false,
		            data: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24"]
		        }
		    ],
		    yAxis: [
		        {
		            type: "value",
		            name: "°C"
		        }
		    ],
		    series: [
		        {
		            name: "充电",
		            type: "line",
		            data: [11, 11, 15, 13, 12, 13, 10, 11, 11, 15, 13, 12, 13, 10, 11, 11, 15, 13, 12, 13, 10, 11, 11, 15]
		        },
		        {
		            name: "放电",
		            type: "line",
		            data: [1, -2, 2, 5, 3, 2, 0, 1, -2, 2, 5, 3, 2, 0, 1, -2, 2, 5, 3, 2, 0, 1, -2, 2]
		        }
		    ]
		};
	
	myChart = echarts.init(document.getElementById("chongfangChart"), 'macarons');
	myChart.setOption(option);
	
	
});