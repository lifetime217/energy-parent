/**
 * 
 * @param columnName
 *            列名
 * @param cellVal
 *            单元格值
 * @param obj
 *            行对象
 * @returns
 */
function handleCellValue(columnName, cellVal, obj) {
	if("alertDevice" == columnName){
		if(cellVal == 10){
			return "PCS";
		}else if(cellVal == 20){
			return "BMS";
		}else if(cellVal == 30){
			return "电池单体";
		}
	}else if("alertSourceType" == columnName){
		if(cellVal == 10){
			return "BMS";
		}else if(cellVal == 20){
			return "PCS";
		}else if(cellVal == 90){
			return "诊断";
		}
	}else if("alertLevel" == columnName){
		if(cellVal == 4){
			return "<span class='label label-danger'>4</span>";
		}else if(cellVal == 3){
			return "<span class='label label-warning'>3</span>";
		}else if(cellVal == 2){
			return "<span class='label label-success'>2</span>";
		}else if(cellVal == 1){
			return "<span class='label label-info'>1</span>";
		}
	}else if("alertSoeId" == columnName){
//			var url = $('body').attr('ctx') + "/event/trace/view/" +cellVal+"?_t="+new Date().getTime();
//			return "<a href='" + url + "'>" + cellVal + "</a>";
		return "<a href='#'>" + cellVal + "</a>";
	}else if("handleCount"==columnName){
		if(cellVal == 0 || cellVal==null){
			return "<text style='color:red'>报警尚未处理</text>";
		}else{
			return "报警已处理过"+cellVal+"次";
		}
	}
	return cellVal;
}

function getCustomQueryData() {
	return {};
}
//daterangepicker汉化
var locale = {
        "format": 'YYYY-MM-DD HH:mm:ss',
        "separator": "~",
        "applyLabel": "确定",
        "cancelLabel": "取消",
        "fromLabel": "起始时间",
        "toLabel": "结束时间'",
        "customRangeLabel": "自定义",
        "weekLabel": "W",
        "daysOfWeek": ["日", "一", "二", "三", "四", "五", "六"],
        "monthNames": ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
        "firstDay": 1
    };

$('.rang_time').daterangepicker(
	{	
        'timePicker24Hour' : true,//设置小时为24小时制 默认false
        'timePicker' : true,//可选中时分 默认false
		'locale': locale,
		//汉化按钮部分
        ranges: {
            '今日': [moment(), moment()],
            '昨日': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            '最近7日': [moment().subtract(6, 'days'), moment()],
            '最近30日': [moment().subtract(29, 'days'), moment()],
            '本月': [moment().startOf('month'), moment().endOf('month')],
            '上月': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
       },
       //默认报警区间是最近30天
        startDate: moment().subtract(29, 'days'),
        endDate: moment()
		}		
);

function getOpBtns() {
	return [ {
		css : "handle",
		text : "处理"
	}];
}

$(function(){
	
	//点击处理
	var addDlgIndex;
	$('.list-content-div table>tbody').delegate('tr>td>.handle', 'click', function() {
		var id =$( this).attr("data-id");
		$('#handleInfo').val('');
		$.ajax({
			url:$('body').attr('ctx')+"/alert/handle/"+id+"?_t="+new Date().getTime(),
			type:'post',
			dataType:'json',
			success:function(data){
				if(data.statusCode==200){
					$('#alertId').val(data.data.alertId);
					if(data.data.handleCount > 0){
						$('#beforeInfo').show();
						$('#handleInfos').val(data.data.detail);
						addDlgIndex = layer.open({
							title : "报警处理",
							type : 1,
							area : [ '450px', '400px' ], // 宽高
							content : $('#handle_dlg')
						});
					}else{
						$('#beforeInfo').hide();
						addDlgIndex = layer.open({
							title : "报警处理",
							type : 1,
							area : [ '450px', '400px' ], // 宽高
							content : $('#handle_dlg')
						});
					}
				}else{
					layer.alert(data.msg);
				}
			},
			error:function(errmsg){
				layer.alert('网络异常');
			}
		});
	});


	//点击保存
	$('#handle_submit').on('click',function(){
		var id = $('#alertId').val();
		var handleInfo = $('#handleInfo').val();
		if(! handleInfo){
			layer.msg('请填写相关处理信息');
			return;
		}else{
			$.ajax({
				url:$('body').attr('ctx')+"/alert/handle/save/"+id+"?_t="+new Date().getTime(),
				type:'post',
				data:{'handleInfo':handleInfo},
				dataType:'json',
				success:function(data){
					if(data.statusCode==200){
							layer.alert(data.msg);
							if(addDlgIndex){
								layer.close(addDlgIndex);
							}
							listui.refresh();
						}else{
							layer.alert(data.msg);
						}
				},
				error:function(errmsg){
					layer.alert('网络异常');
				}
			});
		}
		
	})
	
	
})

